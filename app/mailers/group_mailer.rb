# frozen_string_literal: true

class GroupMailer < ApplicationMailer
  def invitation(email:, token:, sender:)
    sender_name = sender.name

    @token = token
    @sender_name = sender_name

    mail(to: email, subject: default_i18n_subject(sender: sender_name))
  end
end
