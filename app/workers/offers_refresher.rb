# frozen_string_literal: true

require "sidekiq"

class OffersRefresher
  include Sidekiq::Worker

  def perform(klass, id, country)
    watchable = klass.constantize.find(id)
    MovieMates::Import::OffersRefresher.for(
      watchable:,
      country:
    ).refresh
  end
end
