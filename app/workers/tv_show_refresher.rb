# frozen_string_literal: true

require "sidekiq"

class TvShowRefresher
  include Sidekiq::Worker

  def perform(tv_show_id)
    tv_show = TvShow.find(tv_show_id)
    MovieMates::Import::TvShowRefresher.new(tv_show:).refresh
  end
end
