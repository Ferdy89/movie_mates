# frozen_string_literal: true

require "sidekiq"

class MoviesRefresher
  include Sidekiq::Worker

  def perform
    Movie.find_each do |movie|
      MovieRefresher.perform_async(movie.id)
      OffersAllCountriesRefresher.perform_async("Movie", movie.id)
    end
  end
end

Sidekiq::Cron::Job.create(
  name: "Refresh all movies every day",
  cron: "0 4 * * *",
  class: "MoviesRefresher"
)
