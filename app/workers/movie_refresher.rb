# frozen_string_literal: true

require "sidekiq"

class MovieRefresher
  include Sidekiq::Worker

  def perform(movie_id)
    movie = Movie.find(movie_id)
    MovieMates::Import::MovieRefresher.new(movie:).refresh
  end
end
