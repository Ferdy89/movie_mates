# frozen_string_literal: true

require "sidekiq"

class OffersAllCountriesRefresher
  include Sidekiq::Worker

  def perform(klass, id)
    AVAILABLE_COUNTRIES.each do |country|
      OffersRefresher.perform_async(klass, id, country.to_s)
    end
  end
end
