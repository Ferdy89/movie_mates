# frozen_string_literal: true

require "sidekiq"

class TvShowsRefresher
  WAIT_BETWEEN_JOBS = 1.minute

  include Sidekiq::Worker

  def perform
    TvShow.find_each.with_index do |tv_show, index|
      TvShowRefresher.perform_in(index * WAIT_BETWEEN_JOBS, tv_show.id)
      OffersAllCountriesRefresher.perform_in(
        index * WAIT_BETWEEN_JOBS,
        "TvShow",
        tv_show.id
      )
    end
  end
end

Sidekiq::Cron::Job.create(
  name: "Refresh all TV shows every day",
  cron: "0 6 * * *",
  class: "TvShowsRefresher"
)
