# frozen_string_literal: true

require "sidekiq"

# Worker meant to be run manually when movies added to the library aren't in
# TMDb anymore. This will get rid of them and all the associated data (comments,
# votes, etc).
class MoviePurger
  include Sidekiq::Worker

  def perform(movie_id)
    ActiveRecord::Base.transaction do
      movie = Movie.find(movie_id)

      movie.tmdb_profile.destroy
      Comment.where(nomination: movie.nominations).destroy_all
      Vote.where(nomination: movie.nominations).destroy_all
      movie.nominations.destroy_all
      movie.destroy
    end
  end
end
