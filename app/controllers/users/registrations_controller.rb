# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # This overrides a method on Devise to avoid calling update_with_password.
  # They do it this way for security reasons, but we don't have passwords so we
  # can't have it. Be mindful when allowing this controller to edit sensitive
  # information.
  def update_resource(resource, params)
    resource.update(params)
  end
end
