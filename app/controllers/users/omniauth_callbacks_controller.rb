# frozen_string_literal: true

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  include Devise::Controllers::Rememberable

  def google_callback
    user = MovieMates::Registration::UserCredentials.
      for(omniauth_hash: request.env["omniauth.auth"],
          invitation_token: cookies[:invitation_token]).
      register

    cookies.delete(:invitation_token)

    sign_in_and_remember(user)

    redirect_to home_path

    set_flash_message(:notice, :success, kind: "Google") if is_navigational_format?
  end
  alias google_oauth2 google_callback

  def failure
    if is_navigational_format?
      set_flash_message(:alert, :failure,
                        kind: "Google",
                        reason: request.env["omniauth.error.type"])
    end

    redirect_to root_path
  end

  private

  def sign_in_and_remember(user)
    sign_in(user)
    remember_me(user)
  end
end
