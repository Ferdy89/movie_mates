# frozen_string_literal: true

class StaticController < ApplicationController
  skip_before_action :authenticate_user!

  layout "static"

  def index
    redirect_to home_path if user_signed_in?
  end
end
