# frozen_string_literal: true

class VotesController < ApplicationController
  def update
    nomination = nomination_klass.
      for_group_and_slug(current_user.group, params[:slug])

    vote = Vote.find_or_initialize_by(
      voter: current_user, nomination:
    )

    vote.update!(opinion: params[:vote])
  end
end
