# frozen_string_literal: true

class Library::NominationsController < ApplicationController
  def create
    watchable = watchable_klass.find_by!(slug: params[:slug])
    watchable.nominations.create!(
      group: current_user.group, contributor: current_user
    )

    flash[:notice] = t(".added_notice", title: watchable.tmdb_profile.title)
    redirect_to watchable
  end
end
