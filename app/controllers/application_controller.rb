# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_locale
  before_action :authenticate_user!
  before_action :set_raven_context

  private

  # :nocov:
  helper_method def dogfooder?
    Rails.env.test? || DOGFOODERS.include?(current_user.email)
  end
  # :nocov:

  def set_locale
    I18n.locale = user_signed_in? && current_user.preferred_locale ||
                  http_accept_language.compatible_language_from(
                    AVAILABLE_COUNTRIES
                  ) ||
                  I18n.default_locale
  end

  # :nocov:
  def set_raven_context
    if defined?(Raven)
      Raven.user_context(id: current_user.id, email: current_user.email) if user_signed_in?
      Raven.extra_context(params: params.to_unsafe_h, url: request.url)
    end
  end
  # :nocov:

  def watchable_klass
    if params[:watchable_type] == "movie"
      Movie
    elsif params[:watchable_type] == "tv_show"
      TvShow
    else
      raise "Unknown watchable_type '#{params[:watchable_type]}'"
    end
  end

  def nomination_klass
    if params[:watchable_type] == "movie"
      MovieNomination
    elsif params[:watchable_type] == "tv_show"
      TvShowNomination
    else
      raise "Unknown watchable_type '#{params[:watchable_type]}'"
    end
  end

  def watch_session_klass
    if params[:watchable_type] == "movie"
      Picking::MovieSession
    elsif params[:watchable_type] == "tv_show"
      Picking::TvShowSession
    else
      raise "Unknown watchable_type '#{params[:watchable_type]}'"
    end
  end

  def search_klass
    if params[:watchable_type] == "movie"
      MovieMates::SearchResults
    elsif params[:watchable_type] == "tv_show"
      MovieMates::TvShowSearchResults
    else
      MovieMates::SearchResults
    end
  end
end
