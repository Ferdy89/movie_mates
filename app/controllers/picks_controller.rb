# frozen_string_literal: true

class PicksController < ApplicationController
  def new
    @data = {
      users: Picking::GroupUsersPresenter.as_json(group: current_user.group),
      providers: Picking::ProvidersPresenter.as_json(
        providers: Provider.for_country(I18n.locale)
      ),
    }
  end

  def create
    user_ids = (params[:present_movie_mates_ids] || {}).keys.map(&:to_i)
    provider_ids = (params[:providers] || []).map(&:to_i)

    redirect_to action: :show,
                present_movie_mates_ids: user_ids,
                provider_ids:,
                watchable_type: params[:watchable_type]
  end

  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/PerceivedComplexity
  def show
    user_ids = (params[:present_movie_mates_ids] || []).map(&:to_i)
    provider_ids = (params[:provider_ids] || []).map(&:to_i)
    provider_filter = if provider_ids.any?
      Picking::ProviderFilter.some(ids: provider_ids)
    else
      Picking::ProviderFilter.all
    end

    watch_session = watch_session_klass.new(
      user_ids:,
      current_user:,
      providers: provider_filter,
      country: I18n.locale
    )

    if watch_session.no_users?
      flash[:alert] = t(".no_users_alert")
      redirect_back(fallback_location: new_pick_path)
    else
      @data = {
        catalog: watch_session.as_json,
        providers: Picking::ProvidersPresenter.as_json(
          providers: Provider.for_country(I18n.locale)
        ),
        users: Picking::GroupUsersPresenter.as_json(
          group: current_user.group,
          selected_user_ids: user_ids
        ),
        selected_provider_ids: provider_ids,
        watchable_type: params[:watchable_type],
      }
    end
  end
  # rubocop:enable Metrics/PerceivedComplexity
  # rubocop:enable Metrics/AbcSize
end
