# frozen_string_literal: true

class GroupsController < ApplicationController
  def index
    @groups = Group.where.not(id: current_user.group.id)
  end
end
