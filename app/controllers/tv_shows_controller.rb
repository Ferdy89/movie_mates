# frozen_string_literal: true

class TvShowsController < ApplicationController
  def index
    @data = {
      catalog: MovieMates::TvShowLibrary.new(
        user: current_user,
        sort_by: params[:sort_by],
        page: params[:page]
      ).as_json,
      sort_by: params[:sort_by],
    }
  end

  def create
    tv_show = MovieMates::Import::Librarian.
      new(tmdb_id: params[:tmdb_id], user: current_user).
      nominate_tv_show.
      tv_show

    flash[:notice] =
      t(".import_notice", title: tv_show.tmdb_profile.title)

    redirect_to tv_show
  end

  def show
    @tv_show = Picking::WatchablePresenter::TvShow.new(
      slug: params[:slug],
      current_user:,
      country: I18n.locale
    ).as_json
  end

  def update
    Picking::UpdateLastWatched.update_last_watched(
      group: current_user.group,
      slug: params[:slug],
      last_season_watched: params[:season].to_i,
      last_episode_watched: params[:episode].to_i
    )

    redirect_to tv_show_path(params[:slug])
  end

  def destroy
    TvShowNomination.
      joins(:group, :tv_show).
      find_by!(group: current_user.group, tv_shows: {slug: params[:slug]}).
      destroy

    redirect_to home_path
  end
end
