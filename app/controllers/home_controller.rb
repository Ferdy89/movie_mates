# frozen_string_literal: true

class HomeController < ApplicationController
  # rubocop:disable Metrics/AbcSize
  def index
    movies_left_to_vote = MovieMates::MovieCatalog.new(
      current_user:,
      movie_ids: Movie.not_voted_by(current_user).pluck(:id)
    )
    tv_shows_left_to_vote = MovieMates::TvShowCatalog.new(
      current_user:,
      tv_show_ids: TvShow.not_voted_by(current_user).pluck(:id)
    )

    @data = {
      users: Picking::GroupUsersPresenter.as_json(group: current_user.group),
      movie_catalog: movies_left_to_vote.as_json,
      tv_show_catalog: tv_shows_left_to_vote.as_json,
      providers: Picking::ProvidersPresenter.as_json(
        providers: Provider.for_country(I18n.locale)
      ),
    }
  end
  # rubocop:enable Metrics/AbcSize
end
