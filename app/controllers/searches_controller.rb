# frozen_string_literal: true

class SearchesController < ApplicationController
  def show
    search_results = search_klass.
      from(params[:term], user: current_user, year: params[:year])

    @data = {
      term: params[:term],
      results: search_results.as_json,
      watchable_type: params.fetch(:watchable_type, "movie"),
    }
  end
end
