# frozen_string_literal: true

class InvitationsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:commit]
  skip_before_action :verify_authenticity_token, only: [:commit]

  def create
    send_invitations

    redirect_back(
      fallback_location: groups_path,
      notice: t(".sent_notice", emails: emails.join(", "))
    )
  end

  def commit
    token = params[:token]

    if user_signed_in?
      MovieMates::Registration::Invitation.
        from(token:, user: current_user).
        accept

      redirect_to home_path
    else
      cookies[:invitation_token] = token

      authenticate_user!
    end
  end

  private

  def send_invitations
    invitations = ActiveRecord::Base.transaction do
      emails.map do |email|
        current_user.group.group_invitations.create!(email:)
      end
    end

    invitations.each do |invitation|
      invitation.send_email(sender: current_user)
    end
  end

  def emails
    require "mail"

    stripped_emails.select do |email|
      Mail::Address.new(email)
    rescue Mail::Field::ParseError
      false
    end
  end

  def stripped_emails
    params.
      fetch(:emails, "").
      split(",").
      map(&:strip)
  end
end
