# frozen_string_literal: true

class MoviesController < ApplicationController
  def index
    @data = {
      catalog: MovieMates::Library.new(
        user: current_user,
        sort_by: params[:sort_by],
        page: params[:page]
      ).as_json,
      sort_by: params[:sort_by],
    }
  end

  def create
    movie = MovieMates::Import::Librarian.
      new(tmdb_id: params[:tmdb_id], user: current_user).
      nominate_movie.
      movie

    flash[:notice] = t(".import_notice", title: movie.tmdb_profile.title)
    redirect_to movie_path(movie)
  end

  def show
    @movie = Picking::WatchablePresenter::Movie.new(
      slug: params[:slug],
      current_user:,
      country: I18n.locale
    ).as_json
  end

  def update
    Picking::LibraryVotes.new(group: current_user.group).mark_watched(
      slug: params[:slug],
      watched_user_ids: (params[:watched] || {}).keys.map(&:to_i)
    )

    redirect_to movie_path(params[:slug])
  end

  def destroy
    MovieNomination.
      joins(:group, :movie).
      find_by!(group: current_user.group, movies: {slug: params[:slug]}).
      destroy

    redirect_to movies_path
  end
end
