# frozen_string_literal: true

class CommentsController < ApplicationController
  def create
    watchable = watchable_klass.find_by!(slug: params[:slug])

    nomination_klass.
      find_by(group: current_user.group, watchable:).
      comments.
      create!(author: current_user, body: params[:comment])

    redirect_back(fallback_location: watchable)
  end
end
