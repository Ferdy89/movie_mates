# frozen_string_literal: true

class MyGroupsController < ApplicationController
  ShowGroup = Struct.new(:id, :name, :one_member?)
  def show
    @group = ShowGroup.new(
      current_group.id, current_group.name, current_group.users.count == 1
    )
  end

  def update
    current_group.update!(name: params[:name])

    redirect_to my_group_path, notice: t(".success_notice")
  end

  def destroy
    if current_group.users.count == 1
      current_group.implode

      sign_out(current_user)

      redirect_to root_path
    else
      MovieMates::Registration::GroupSplit.
        split_into_new_group(user: current_user)

      redirect_to my_group_path, notice: t(".success_notice")
    end
  end

  private

  helper_method def current_group
    current_user.group
  end
end
