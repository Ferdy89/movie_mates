# frozen_string_literal: true

class LocalesController < ApplicationController
  def update
    I18n.locale = params[:locale]
    current_user.update!(preferred_locale: I18n.locale)

    redirect_back(fallback_location: root_path)
  end
end
