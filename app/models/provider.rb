# frozen_string_literal: true

class Provider < ApplicationRecord
  has_many :offers, dependent: :destroy

  def self.with_offers(watchable:, country:)
    joins(:offers).
      includes(:offers).
      where(offers: {watchable:, country:}).
      group("providers.id", "offers.id").
      order(:provider_id)
  end

  def self.for_country(country)
    joins(:offers).
      where(offers: {country:}).
      group("providers.id").
      order(:provider_id)
  end
end
