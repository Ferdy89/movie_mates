# frozen_string_literal: true

class Offer < ApplicationRecord
  belongs_to :watchable, polymorphic: true
  belongs_to :provider
  monetize :price_cents
end
