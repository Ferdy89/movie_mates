# frozen_string_literal: true

class User < ApplicationRecord
  devise :registerable, :rememberable, :trackable,
         :omniauthable, omniauth_providers: [:google_oauth2]

  belongs_to :group
  has_many :votes,
           foreign_key: :voter_id,
           inverse_of: :voter,
           dependent: :destroy
  has_many :contributions,
           class_name: "MovieNomination",
           foreign_key: :contributor_id,
           inverse_of: :contributor,
           dependent: :nullify
  has_many :comments,
           foreign_key: :author_id,
           inverse_of: :author,
           dependent: :destroy
end
