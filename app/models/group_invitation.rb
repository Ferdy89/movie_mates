# frozen_string_literal: true

require "securerandom"

class GroupInvitation < ApplicationRecord
  belongs_to :group

  validates :email, presence: true

  after_initialize :ensure_token

  def send_email(sender:)
    GroupMailer.
      invitation(email:, token:, sender:).
      deliver_now
  end

  private

  def ensure_token
    self.token ||= SecureRandom.uuid
  end
end
