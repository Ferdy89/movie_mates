# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  after_commit do
    if Thread.current[:jobs]
      Thread.current[:jobs].each do |job|
        job.klass.perform_async(*job.args)
      end
      Thread.current[:jobs] = nil
    end
  end
end
