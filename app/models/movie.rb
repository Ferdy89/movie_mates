# frozen_string_literal: true

class Movie < ApplicationRecord
  def self.nomination_klass_name
    "MovieNomination"
  end

  has_many :nominations,
           class_name: nomination_klass_name,
           dependent: :restrict_with_exception
  has_many :offers, as: :watchable, dependent: :destroy
  has_one  :tmdb_profile, dependent: :destroy

  def self.with_profile(ids:, group:)
    for_group(group).
      includes(
        :tmdb_profile,
        nominations: [:contributor, :comments, {votes: :voter}]
      ).where(id: ids)
  end

  def self.not_voted_by(user)
    movies_voted =
      joins(nominations: :votes).where(votes: {voter_id: user.id})

    for_group(user.group_id).
      where.not(id: movies_voted).
      distinct
  end

  def self.for_group(group)
    joins(:nominations).where(movie_nominations: {group_id: group})
  end

  def self.among_tmdb_ids(tmdb_ids)
    joins(:tmdb_profile).where(tmdb_profiles: {tmdb_id: tmdb_ids})
  end

  def self.for_tmdb_id(tmdb_id)
    joins(:tmdb_profile).find_by(tmdb_profiles: {tmdb_id:})
  end

  def self.find_with_profile_strict(slug:)
    strict_loading.
      includes(:tmdb_profile).
      find_by!(slug:)
  end

  def to_param
    slug
  end
end
