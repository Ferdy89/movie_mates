# frozen_string_literal: true

class Vote < ApplicationRecord
  UNKNOWN_OPINION = "unknown"

  belongs_to :voter,
             class_name: "User",
             inverse_of: :votes
  belongs_to :nomination, polymorphic: true
end
