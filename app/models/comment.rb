# frozen_string_literal: true

class Comment < ApplicationRecord
  belongs_to :author,
             class_name: "User",
             inverse_of: :comments
  belongs_to :nomination, polymorphic: true
end
