# frozen_string_literal: true

class TmdbTvShowProfile < ApplicationRecord
  belongs_to :tv_show
  has_many :seasons, dependent: :destroy
end
