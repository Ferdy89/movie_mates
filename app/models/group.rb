# frozen_string_literal: true

class Group < ApplicationRecord
  has_many :users, dependent: :restrict_with_exception
  has_many :group_invitations, dependent: :destroy
  has_many :movie_nominations, dependent: :destroy

  def implode
    transaction do
      movie_nominations.destroy_all
      users.destroy_all
      destroy!
    end
  end
end
