# frozen_string_literal: true

class TmdbProfile < ApplicationRecord
  belongs_to :movie
end
