# frozen_string_literal: true

class Season < ApplicationRecord
  belongs_to :tmdb_tv_show_profile
end
