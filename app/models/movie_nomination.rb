# frozen_string_literal: true

class MovieNomination < ApplicationRecord
  belongs_to :contributor,
             class_name: "User",
             inverse_of: :contributions
  belongs_to :group
  belongs_to :movie
  alias_attribute :watchable, :movie
  has_many :votes, as: :nomination, dependent: :destroy do
    def opinion_for(user:)
      user_vote = find { |vote| vote.voter_id == user.id }

      user_vote ? user_vote.opinion : Vote::UNKNOWN_OPINION
    end
  end
  has_many :comments, as: :nomination, dependent: :destroy

  delegate :name, to: :contributor, prefix: true

  def self.for_group_and_slug(group, slug)
    joins(:movie).find_by(group:, movies: {slug:})
  end

  def self.find_with_votes_comments_strict(watchable:, group:)
    strict_loading.
      includes([
        :contributor,
        {comments: :author},
        :votes,
      ]).
      find_by(watchable:, group: [group, nil])
  end
end
