# frozen_string_literal: true

module ApplicationHelper
  def self.detailed_votes_list(votes)
    votes.map do |vote|
      opinion_text = I18n.t("opinion.#{vote.fetch(:opinion)}")

      "#{vote.fetch(:user_name)} ➡ #{opinion_text}"
    end.join("<br>")
  end

  def active_when
    yield ? "active" : ""
  end

  def icon_for_opinion(opinion)
    @icon_for_opinion ||= {
      yes: glyphicon("play", classes: "text-success", "data-opinion": "yes"),
      no: glyphicon("stop", classes: "text-danger", "data-opinion": "no"),
      abstention: glyphicon("adjust", classes: "text-warning", "data-opinion": "abstention"),
      watched: glyphicon("eye-open", classes: "text-muted", "data-opinion": "watched"),
      unknown: glyphicon("question-sign", classes: "text-primary", "data-opinion": "unknown"),
    }.freeze
    @icon_for_opinion[opinion.to_sym] # rubocop:disable Rails/HelperInstanceVariable
  end

  def format_offers_cell(provider, category)
    safe_join(
      provider.fetch(:offers).fetch(category).map do |offer|
        link_to(yield(offer), offer.fetch(:url))
      end,
      raw("<br>") # rubocop:disable Rails/OutputSafety
    )
  end

  def glyphicon(name, classes: "", **extra_options)
    content_tag(:span, "",
                class: "glyphicon glyphicon-#{name} #{classes}",
                **extra_options)
  end
end
