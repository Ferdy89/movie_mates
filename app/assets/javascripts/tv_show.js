var MovieMates = MovieMates || {};
MovieMates.TvShow = MovieMates.TvShow || {};

MovieMates.TvShow.updateEpisodesForSeason = function() {
  let selectedSeason = $('#season-picker option:selected');
  let episodeCount = $(selectedSeason).attr('data-episode-count') - 0;
  let episodeOptions = [];

  for (let i = 0; i < episodeCount; i++) {
    episodeOptions.push(
      `<option value="${i + 1}">
         E${i + 1}
       </option>`
    );
  }

  $('#episode-picker').html(episodeOptions.join(''));
  $('#episode-picker').selectpicker('refresh');
  let lastEpisodeWatched = $("#last-episode-watched").text().trim();
  $('#episode-picker').selectpicker('val', lastEpisodeWatched);
}

$(document).on('turbolinks:load', function() {
  MovieMates.TvShow.updateEpisodesForSeason();

  $('#season-picker').change(function() {
    MovieMates.TvShow.updateEpisodesForSeason();
  });
})
