var MovieMates = MovieMates || {};
MovieMates.Catalog = MovieMates.Catalog || {};

MovieMates.Catalog.update_vote = function(target, watchable_type) {
  let vote = $(target).find('input[name=vote]').first().val();
  let slug = $(target).find('input[name=slug]').first().val();

  let buttons = $(target).parent().find('label');
  buttons.addClass('disabled');

  $.ajax(`/votes/${slug}?watchable_type=${watchable_type}`, {
    method: 'PUT',
    data: { vote: vote },
    complete: function() { buttons.removeClass('disabled'); }
  })
}
