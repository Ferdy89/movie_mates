$(document).on('turbolinks:load', function() {
  $('[data-toggle="tooltip"]').tooltip({ html: true })
  $('[data-toggle="popover"]').popover()
  $('select.selectpicker').selectpicker();
})

$(document).on("turbolinks:before-cache", function() {
  // From https://github.com/snapappointments/bootstrap-select/issues/1413#issuecomment-231936277
  $('.selectpicker').selectpicker('destroy').addClass('selectpicker');
});
