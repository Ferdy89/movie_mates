# frozen_string_literal: true

class Picking::TvShowSession # rubocop:disable Metrics/ClassLength
  def initialize(current_user:, user_ids:, providers:, country:)
    @current_user = current_user
    @user_ids     = user_ids
    @providers    = providers
    @country      = country
  end

  def no_users?
    user_ids.empty?
  end

  def as_json
    MovieMates::TvShowCatalog.new(
      current_user:,
      tv_show_ids: pick_tv_shows.map(&:id)
    ).as_json
  end

  private

  attr_reader :current_user, :user_ids, :providers, :country

  delegate :group, to: :current_user

  def pick_tv_shows
    # Order here matters, since one array is prioritized and it's sorted
    (prioritized_by_user_votes & voted_by_all_users) - cannot_watch
  end

  # rubocop:disable Metrics/AbcSize
  def prioritized_by_user_votes
    extra_joins = []
    extra_where = []
    extra_params = []

    unless providers.all?
      extra_joins.push(
        "JOIN offers ON offers.watchable_id = tv_shows.id
          AND offers.watchable_type = 'TvShow'"
      )
      extra_joins.push("JOIN providers ON providers.id = offers.provider_id")
      extra_where.push("AND providers.id = ANY(ARRAY%s)
                        AND offers.country = '%s'")
      extra_params.push(providers.ids, country)
    end

    query = <<-SQL
      SELECT tv_shows.*
      FROM tv_shows
      JOIN tv_show_nominations ON tv_shows.id = tv_show_nominations.tv_show_id
      LEFT JOIN votes_per_tv_show(ARRAY%s)
        AS user_tv_show_votes
        ON tv_shows.id = user_tv_show_votes.tv_show_id
      LEFT JOIN votes_per_tv_show(
          ARRAY(SELECT id FROM users WHERE group_id = %s AND NOT id = ANY(ARRAY%s)))
        AS other_user_tv_show_votes
        ON tv_shows.id = other_user_tv_show_votes.tv_show_id
      JOIN tmdb_tv_show_profiles ON tmdb_tv_show_profiles.tv_show_id = tv_shows.id
      #{extra_joins.join("\n")}
      WHERE tv_show_nominations.group_id = %s
      AND (
        tv_show_nominations.last_season_watched IS NULL
        OR tv_show_nominations.last_season_watched < (
          SELECT MAX(season_number)
          FROM seasons
          WHERE seasons.tmdb_tv_show_profile_id = tmdb_tv_show_profiles.id
        )
        OR tv_show_nominations.last_episode_watched IS NULL
        OR tv_show_nominations.last_episode_watched < (
          SELECT episode_count
          FROM seasons
          WHERE seasons.tmdb_tv_show_profile_id = tmdb_tv_show_profiles.id
          AND seasons.season_number = tv_show_nominations.last_season_watched
        )
      )
      #{extra_where.join("\n")}
      ORDER BY
        user_tv_show_votes.yes_votes DESC,
        other_user_tv_show_votes.no_or_watched_votes DESC,
        tmdb_tv_show_profiles.rating DESC
    SQL
    group_id = group.id
    TvShow.find_by_sql(
      ActiveRecord::Base.send(
        :sanitize_sql_array,
        [query, user_ids, group_id, user_ids, group_id] + extra_params
      )
    )
  end
  # rubocop:enable Metrics/AbcSize

  def voted_by_all_users
    TvShow.
      for_group(group).
      joins(nominations: :votes).
      group(:id).
      having("COUNT(*) = (SELECT COUNT(*) FROM users WHERE group_id = ?)",
             group.id)
  end

  def cannot_watch
    other_users_want_to_watch + users_do_not_want_to_watch
  end

  def other_users_want_to_watch
    TvShow.
      for_group(group).
      joins(nominations: {votes: :voter}).
      where.not(votes: {voter_id: user_ids}).
      where(votes: {opinion: "yes"}).
      distinct
  end

  def users_do_not_want_to_watch
    TvShow.
      for_group(group).
      joins(nominations: {votes: :voter}).
      where(votes: {voter_id: user_ids, opinion: ["no", "watched"]}).
      distinct
  end
end
