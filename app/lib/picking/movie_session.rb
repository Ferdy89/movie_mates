# frozen_string_literal: true

class Picking::MovieSession
  def initialize(current_user:, user_ids:, providers:, country:)
    @current_user = current_user
    @user_ids     = user_ids
    @providers    = providers
    @country      = country
  end

  def no_users?
    user_ids.empty?
  end

  def as_json
    MovieMates::MovieCatalog.new(
      current_user:,
      movie_ids: pick_movies.map(&:id)
    ).as_json
  end

  private

  attr_reader :current_user, :user_ids, :providers, :country

  delegate :group, to: :current_user

  def pick_movies
    # Order here matters, since one array is prioritized and it's sorted
    (prioritized_by_user_votes & voted_by_all_users) - cannot_watch
  end

  # rubocop:disable Metrics/AbcSize
  def prioritized_by_user_votes
    extra_joins = []
    extra_where = []
    extra_params = []

    unless providers.all?
      extra_joins.push(
        "JOIN offers ON offers.watchable_id = movies.id
          AND offers.watchable_type = 'Movie'"
      )
      extra_joins.push("JOIN providers ON providers.id = offers.provider_id")
      extra_where.push("AND providers.id = ANY(ARRAY%s)
                        AND offers.country = '%s'")
      extra_params.push(providers.ids, country)
    end

    query = <<-SQL
      SELECT movies.*
      FROM movies
      JOIN movie_nominations ON movies.id = movie_nominations.movie_id
      LEFT JOIN votes_per_movie(ARRAY%s)
        AS user_movie_votes
        ON movies.id = user_movie_votes.movie_id
      LEFT JOIN votes_per_movie(
          ARRAY(SELECT id FROM users WHERE group_id = %s AND NOT id = ANY(ARRAY%s)))
        AS other_user_movie_votes
        ON movies.id = other_user_movie_votes.movie_id
      JOIN tmdb_profiles ON tmdb_profiles.movie_id = movies.id
      #{extra_joins.join("\n")}
      WHERE movie_nominations.group_id = %s
      #{extra_where.join("\n")}
      ORDER BY
        user_movie_votes.yes_votes DESC,
        other_user_movie_votes.no_or_watched_votes DESC,
        tmdb_profiles.rating DESC
    SQL
    group_id = group.id
    Movie.find_by_sql(
      ActiveRecord::Base.send(
        :sanitize_sql_array,
        [query, user_ids, group_id, user_ids, group_id] + extra_params
      )
    )
  end
  # rubocop:enable Metrics/AbcSize

  def voted_by_all_users
    Movie.
      for_group(group).
      joins(nominations: :votes).
      group(:id).
      having("COUNT(*) = (SELECT COUNT(*) FROM users WHERE group_id = ?)",
             group.id)
  end

  def cannot_watch
    other_users_want_to_watch + users_do_not_want_to_watch
  end

  def other_users_want_to_watch
    Movie.
      for_group(group).
      joins(nominations: {votes: :voter}).
      where.not(votes: {voter_id: user_ids}).
      where(votes: {opinion: "yes"}).
      distinct
  end

  def users_do_not_want_to_watch
    Movie.
      for_group(group).
      joins(nominations: {votes: :voter}).
      where(votes: {voter_id: user_ids, opinion: ["no", "watched"]}).
      distinct
  end
end
