# frozen_string_literal: true

class Picking::LibraryVotes
  attr_reader :group

  def initialize(group:)
    @group = group
  end

  def mark_watched(slug:, watched_user_ids:)
    ActiveRecord::Base.transaction do
      nomination =
        MovieNomination.includes(:votes).for_group_and_slug(group, slug)

      NominationVotes.new(nomination, group.users).
        mark_watched(watched_user_ids)
    end
  end

  class NominationVotes
    attr_reader :nomination, :users

    def initialize(nomination, users)
      @nomination = nomination
      @users = users
    end

    def mark_watched(watched_user_ids)
      each_user_and_vote do |user, vote|
        if watched_user_ids.include?(user.id)
          vote.update!(nomination:, opinion: "watched")
        elsif vote.opinion == "watched"
          vote.delete
        end
      end
    end

    private

    def each_user_and_vote
      users.each do |user|
        user_vote =
          nomination.votes.detect { |vote| vote.voter_id == user.id } ||
          user.votes.build

        yield(user, user_vote)
      end
    end
  end
end
