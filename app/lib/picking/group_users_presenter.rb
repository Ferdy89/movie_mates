# frozen_string_literal: true

class Picking::GroupUsersPresenter
  def self.as_json(group:, selected_user_ids: [])
    group.users.map do |user|
      {
        id: user.id,
        name: user.name,
        profile_pic: user.google_pic,
        selected: selected_user_ids.include?(user.id),
      }
    end
  end
end
