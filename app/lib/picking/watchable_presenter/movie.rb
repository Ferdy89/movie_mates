# frozen_string_literal: true

# Compacts all the information about a movie and its nomination on a group
class Picking::WatchablePresenter::Movie < Picking::WatchablePresenter::Base
  private

  def record
    @record ||= Movie.find_with_profile_strict(slug:)
  end

  def watchable_presenter
    TmdbMoviePresenter.new(record.tmdb_profile)
  end

  def extra_nomination_info
    {}
  end

  class TmdbMoviePresenter < SimpleDelegator
    def as_json
      {
        title:,
        **year,
        url_imdb: "http://www.imdb.com/title/#{imdb_id}",
        url_tmdb:
          "http://www.themoviedb.org/movie/#{movie.tmdb_profile.tmdb_id}",
        plot:,
        trailer_youtube_id: movie.tmdb_profile.trailer_youtube_id,
        minutes:,
        rating:,
        poster: poster_url,
        genres:,
      }
    end

    private

    def year
      if release_date
        {year: release_date.year}
      else
        {}
      end
    end
  end
end
