# frozen_string_literal: true

# Compacts all the information about a watchable and its nomination on a group
class Picking::WatchablePresenter::Base
  attr_reader :slug, :current_user, :country

  delegate :group, to: :current_user

  def initialize(slug:, current_user:, country:)
    @slug = slug
    @current_user = current_user
    @country = country
  end

  def as_json
    {
      slug:,
      currently_in_library: in_library?,
      providers:,
      **watchable_presenter.as_json,
      **active_nomination_info,
    }
  end

  private

  def watchable_presenter
    raise NotImplementedMethod,
          "You must implement `watchable_presenter` on class #{self.class.name}"
  end

  def extra_nomination_info
    raise NotImplementedMethod,
          "You must implement `extra_nomination_info` on class #{self.class.name}"
  end

  def in_library?
    !nomination.nil?
  end

  # rubocop:disable Metrics/AbcSize
  def active_nomination_info
    if in_library?
      {
        contributor: nomination.contributor.name,
        votes: group.users.map do |user|
          {
            user_id: user.id,
            user_name: user.name,
            user_pic: user.google_pic,
            opinion: nomination.votes.opinion_for(user:),
          }
        end,
        current_user_vote: nomination.votes.opinion_for(user: current_user),
        comments: nomination.comments.map do |comment|
                    {
                      author_name: comment.author.name,
                      author_pic: comment.author.google_pic,
                      timestamp: comment.created_at,
                      body: comment.body,
                    }
                  end,
        **extra_nomination_info,
      }
    else
      {}
    end
  end
  # rubocop:enable Metrics/AbcSize

  def nomination
    if defined?(@nomination)
      @nomination
    else
      @nomination = record.class.
        nomination_klass_name.constantize.
        find_with_votes_comments_strict(group:, watchable: record)
    end
  end

  def providers
    Provider.with_offers(watchable: record, country:).map do |provider|
      {
        name: provider.clear_name,
        icon_url: "https://images.justwatch.com#{provider.icon_url}",
        offers: categorize_offers(provider.offers),
      }
    end
  end

  def categorize_offers(offers)
    offer_categories = {
      flatrate: [],
      rent: [],
      buy: [],
      ads: [],
      cinema: [],
      free: [],
    }

    offers.sort_by(&:presentation_type).
      each_with_object(offer_categories) do |offer, categories|
        categories[offer.monetization_type.to_sym] << {
          presentation_type: offer.presentation_type.upcase,
          price: offer.price,
          url: offer.standard_web_url,
        }
      end
  end
end
