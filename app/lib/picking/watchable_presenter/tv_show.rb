# frozen_string_literal: true

# Compacts all the information about a TV show and its nomination on a group
class Picking::WatchablePresenter::TvShow < Picking::WatchablePresenter::Base
  private

  def record
    @record ||= TvShow.find_with_profile_strict(slug:)
  end

  def watchable_presenter
    TmdbTvShowPresenter.new(record.tmdb_profile)
  end

  def extra_nomination_info
    {
      last_season_watched: nomination.last_season_watched,
      last_episode_watched: nomination.last_episode_watched,
    }
  end

  class TmdbTvShowPresenter < SimpleDelegator
    # rubocop:disable Metrics/AbcSize
    def as_json
      sorted_seasons =
        tv_show.tmdb_profile.seasons.order(:season_number)

      {
        title:,
        **year,
        url_tmdb:
          "http://www.themoviedb.org/tv/#{tv_show.tmdb_profile.tmdb_id}",
        plot:,
        minutes:,
        rating:,
        poster: poster_url,
        genres:,
        seasons: sorted_seasons.map do |season|
          {
            name: season.name,
            number: season.season_number,
            episode_count: season.episode_count,
            air_date: season.air_date,
            poster_url: season.poster_url,
          }
        end,
      }
    end
    # rubocop:enable Metrics/AbcSize

    private

    def year
      if release_date
        {year: release_date.year}
      else
        {}
      end
    end
  end
end
