# frozen_string_literal: true

class Picking::UpdateLastWatched
  def self.update_last_watched(
    group:, slug:, last_season_watched:, last_episode_watched:
  )
    nomination =
      TvShowNomination.for_group_and_slug(group, slug)
    nomination.update!(
      last_season_watched:,
      last_episode_watched:
    )
  end
end
