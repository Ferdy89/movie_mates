# frozen_string_literal: true

class Picking::ProvidersPresenter
  def self.as_json(providers:)
    providers.map do |provider|
      {
        id: provider.id,
        icon_url: provider.icon_url,
        name: provider.clear_name,
      }
    end
  end
end
