# frozen_string_literal: true

class Picking::ProviderFilter
  attr_reader :ids

  def self.all
    AllProviders.new
  end

  def self.some(ids:)
    new(ids)
  end

  def initialize(ids)
    @ids = ids
  end

  def all?
    false
  end

  class AllProviders
    def all?
      true
    end

    def ids
      raise "AllProviders can't be asked for the provider ids"
    end
  end
end
