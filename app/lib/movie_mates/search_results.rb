# frozen_string_literal: true

class MovieMates::SearchResults
  def self.from(term, user:, year: nil)
    if term.blank?
      EmptyResults.new(year)
    else
      new(term, user:, year:)
    end
  end

  def initialize(term, user:, year:)
    @term = term
    @user = user
    @year = year
  end

  def as_json(*)
    {term:, year:, library:, external: exclusively_external}
  end

  private

  attr_reader :term, :user, :year

  def exclusively_external
    external_watchables.reject do |external_watchable|
      in_library.include?(external_watchable.fetch(:tmdb_id))
    end
  end

  def library
    @library ||= begin
      external_tmdb_ids = external_watchables.map do |watchable|
        watchable.fetch(:tmdb_id)
      end

      matching_watchable_ids = Movie.
        for_group(user.group).
        among_tmdb_ids(external_tmdb_ids).
        pluck(:id)

      MovieMates::MovieCatalog.new(
        current_user: user,
        movie_ids: matching_watchable_ids
      ).as_json
    end
  end

  def external_watchables
    @external_watchables ||=
      Tmdb::Movie.find(term, year_filter).map do |tmdb_movie|
        MovieMates::Import::TmdbMovieWrapper.
          new(tmdb_movie:).
          hash_with(:tmdb_id, :title, :release_date, :poster_url, :plot,
                    :rating, :url, :original_language)
      end
  end

  def in_library
    @in_library ||= library.fetch(:watchables).map do |watchable|
      watchable.fetch(:tmdb_id)
    end.to_set
  end

  def year_filter
    year ? {year:} : {}
  end

  class EmptyResults
    def initialize(year)
      @year = year
    end

    def as_json(*)
      library = MovieMates::MovieCatalog.empty

      {term: nil, year:, library: library.as_json, external: []}
    end

    private

    attr_reader :year
  end
end
