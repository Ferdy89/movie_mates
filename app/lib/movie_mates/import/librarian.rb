# frozen_string_literal: true

# Assists a user with operations with movies from their group's Library
class MovieMates::Import::Librarian
  attr_reader :tmdb_id, :user

  def initialize(tmdb_id:, user:)
    @tmdb_id = tmdb_id
    @user = user
  end

  def nominate_movie
    ActiveRecord::Base.transaction do
      movie = MovieUpserter.new(tmdb_id:).current_movie

      nomination =
        movie.nominations.create!(group: user.group, contributor: user)
      nomination.votes.create!(voter: user, opinion: "yes")

      nomination
    end
  end

  def nominate_tv_show
    ActiveRecord::Base.transaction do
      tv_show = TvShowUpserter.new(tmdb_id:).current_tv_show

      nomination =
        tv_show.nominations.create!(group: user.group, contributor: user)
      nomination.votes.create!(voter: user, opinion: "yes")

      nomination
    end
  end

  module Slugger
    BAD_URL_CHARACTERS = ".#@&'%"

    def slug(tmdb_wrapper)
      parts = tmdb_wrapper.title.downcase.gsub(/[^a-z\d]/, " ").split(" ")
      parts << tmdb_wrapper.release_date.year if tmdb_wrapper.release_date
      parts.join("-")
    end
  end

  # Updates or creates a Movie with the current data from TMDb
  class MovieUpserter
    include Slugger

    attr_reader :tmdb_id

    def initialize(tmdb_id:)
      @tmdb_id = tmdb_id
    end

    def current_movie
      ActiveRecord::Base.transaction do
        movie = Movie.for_tmdb_id(tmdb_id)

        if movie
          MovieMates::Import::MovieRefresher.new(movie:).refresh
          movie
        else
          import_movie
        end
      end
    end

    private

    def import_movie
      movie = Movie.create!(slug: slug(tmdb_wrapper))
      movie.create_tmdb_profile!(current_tmdb_profile_attributes)
      MovieMates::DelayedJob.enqueue(
        klass: OffersAllCountriesRefresher,
        args: ["Movie", movie.id]
      )
      movie
    end

    def tmdb_wrapper
      @tmdb_wrapper ||= MovieMates::Import::TmdbMovieWrapper.by_id(tmdb_id)
    end

    def current_tmdb_profile_attributes
      tmdb_wrapper.hash_with(:tmdb_id, :title, :release_date, :poster_url,
                             :plot, :rating, :imdb_id, :minutes, :genres, :trailer_youtube_id)
    end
  end

  # Updates or creates a TV Show with the current data from TMDb
  class TvShowUpserter
    include Slugger

    attr_reader :tmdb_id

    def initialize(tmdb_id:)
      @tmdb_id = tmdb_id
    end

    def current_tv_show
      ActiveRecord::Base.transaction do
        tv_show = TvShow.for_tmdb_id(tmdb_id)

        if tv_show
          MovieMates::Import::TvShowRefresher.new(tv_show:).refresh
          tv_show
        else
          import_tv_show
        end
      end
    end

    private

    def import_tv_show
      tv_show = TvShow.create!(slug: slug(tmdb_wrapper))
      tmdb_profile =
        tv_show.create_tmdb_tv_show_profile!(current_tmdb_profile_attributes)
      import_seasons(tmdb_profile)

      MovieMates::DelayedJob.enqueue(
        klass: OffersAllCountriesRefresher,
        args: ["TvShow", tv_show.id]
      )
      tv_show
    end

    def import_seasons(tmdb_profile)
      tmdb_wrapper.seasons.each do |season|
        tmdb_profile.seasons.create!(
          name: season.name,
          air_date: season.air_date,
          episode_count: season.episode_count,
          season_number: season.season_number,
          poster_url: season.poster_url
        )
      end
    end

    def tmdb_wrapper
      @tmdb_wrapper ||= MovieMates::Import::TmdbTvShowWrapper.by_id(tmdb_id)
    end

    def current_tmdb_profile_attributes
      tmdb_wrapper.hash_with(:tmdb_id, :title, :release_date, :poster_url,
                             :plot, :rating, :minutes, :genres, :in_production, :next_episode_to_air)
    end
  end
end
