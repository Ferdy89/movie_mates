# frozen_string_literal: true

# Updates a Movie with the latest data from TMDb
class MovieMates::Import::MovieRefresher
  attr_reader :movie

  def initialize(movie:)
    @movie = movie
  end

  def refresh
    tmdb_profile.update!(current_tmdb_profile_attributes)
  end

  private

  def tmdb_profile
    movie.tmdb_profile
  end

  def current_tmdb_profile_attributes
    MovieMates::Import::TmdbMovieWrapper.
      by_id(tmdb_profile.tmdb_id).
      hash_with(:tmdb_id, :title, :release_date, :poster_url, :plot, :rating,
                :imdb_id, :minutes, :genres, :trailer_youtube_id)
  end
end
