# frozen_string_literal: true

require "just_watch"

# Updates a Movie's offers with the latest data from Just Watch
class MovieMates::Import::OffersRefresher
  def self.for(watchable:, country:)
    case watchable.class.name
    when "Movie" then Movie.new(watchable, country)
    when "TvShow" then TvShow.new(watchable, country)
    end
  end

  def initialize(watchable, country)
    @watchable = watchable
    @country = country
  end

  class Movie < MovieMates::Import::OffersRefresher
    private

    def just_watch_entry
      return @just_watch_entry if defined?(@just_watch_entry)

      candidates = client.search(tmdb_profile.title)

      @just_watch_entry = candidates.find do |candidate|
        candidate.tmdb_id == tmdb_profile.tmdb_id
      end
    end

    def tmdb_profile
      watchable.tmdb_profile
    end

    def cannot_find_watchable
      Rails.logger.info(
        "Couldn't find movie (ID: #{watchable.id}) on Just Watch"
      )
    end
  end

  class TvShow < MovieMates::Import::OffersRefresher
    private

    delegate :tmdb_profile, to: :watchable

    def just_watch_entry
      return @just_watch_entry if defined?(@just_watch_entry)

      candidates = client.search(tmdb_profile.title)

      @just_watch_entry = candidates.find do |candidate|
        candidate.tmdb_id == tmdb_profile.tmdb_id
      end
    end

    def cannot_find_watchable
      Rails.logger.info(
        "Couldn't find TV show (ID: #{watchable.id}) on Just Watch"
      )
    end
  end

  def refresh
    if just_watch_entry
      refresh_offers
    else
      cannot_find_watchable
    end
  end

  private

  attr_reader :watchable, :country

  def refresh_offers
    watchable.offers.where(country:).destroy_all

    just_watch_entry.offers.each do |offer|
      ensure_provider(offer)

      add_offer(offer)
    end
  end

  def ensure_provider(offer)
    unless local_provider_map.key?(offer.provider.id)
      provider = create_provider(offer.provider)
      local_provider_map[offer.provider.id] = provider
    end
  end

  def add_offer(offer)
    Offer.create!(
      watchable:,
      provider: local_provider_map[offer.provider.id],
      country:,
      monetization_type: offer.monetization_type,
      presentation_type: offer.presentation_type,
      price: offer.price,
      standard_web_url: offer.url
    )
  end

  def create_provider(provider)
    if provider
      Provider.create!(
        provider_id: provider.id,
        clear_name: provider.clear_name,
        icon_url: provider.icon_url.gsub("{profile}", "s100")
      )
    end
  end

  def local_provider_map
    @local_provider_map ||= Provider.where(provider_id: all_provider_ids).
      index_by(&:provider_id)
  end

  def all_provider_ids
    @all_provider_ids ||= just_watch_entry.offers.
      map { |offer| offer.provider.id }.uniq
  end

  def client
    @client ||= JustWatch::Client.new(locale: country.tr("-", "_"))
  end
end
