# frozen_string_literal: true

# Updates a TV show with the latest data from TMDb
class MovieMates::Import::TvShowRefresher
  def initialize(tv_show:)
    @tv_show = tv_show
  end

  def refresh
    ActiveRecord::Base.transaction do
      tmdb_profile.update!(current_tmdb_profile_attributes)
      tmdb_profile.seasons.destroy_all
      import_seasons
    end
  end

  private

  attr_reader :tv_show

  delegate :tmdb_profile, to: :tv_show

  def current_tmdb_profile_attributes
    tmdb_wrapper.
      hash_with(
        :tmdb_id,
        :title,
        :release_date,
        :poster_url,
        :plot,
        :rating,
        :minutes,
        :genres,
        :in_production,
        :next_episode_to_air
      )
  end

  def import_seasons
    tmdb_wrapper.seasons.each do |season|
      tmdb_profile.seasons.create!(
        name: season.name,
        air_date: season.air_date,
        episode_count: season.episode_count,
        season_number: season.season_number,
        poster_url: season.poster_url
      )
    end
  end

  def tmdb_wrapper
    @tmdb_wrapper ||= MovieMates::Import::TmdbTvShowWrapper.
      by_id(tmdb_profile.tmdb_id)
  end
end
