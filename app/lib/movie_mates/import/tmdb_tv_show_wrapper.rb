# frozen_string_literal: true

class MovieMates::Import::TmdbTvShowWrapper
  TvShowNotFound = Class.new(StandardError)

  def self.by_id(tmdb_id)
    from_hash(Tmdb::TV.detail(tmdb_id))
  end

  def self.from_hash(hash)
    tmdb_tv_show = Tmdb::TV.new(hash)

    raise TvShowNotFound if tmdb_tv_show.id.nil?

    new(tmdb_tv_show:)
  end

  def initialize(tmdb_tv_show:)
    @tmdb_tv_show = tmdb_tv_show
  end

  def hash_with(*fields)
    fields.index_with { |field| public_send(field) }
  end

  delegate :imdb_id, :original_language, :in_production, to: :tmdb_tv_show

  def tmdb_id
    tmdb_tv_show.id
  end

  def title
    tmdb_tv_show.original_name
  end

  def release_date
    Date.parse(tmdb_tv_show.first_air_date) if tmdb_tv_show.first_air_date.present?
  end

  def poster_url
    "https://image.tmdb.org/t/p/w500#{tmdb_tv_show.poster_path}"
  end

  def plot
    tmdb_tv_show.overview
  end

  def rating
    tmdb_tv_show.vote_average
  end

  def minutes
    tmdb_tv_show.episode_run_time.first
  end

  def genres
    Array(tmdb_tv_show.genres).map { |genre| genre["name"] }
  end

  def url
    "https://www.themoviedb.org/tv/#{tmdb_id}"
  end

  def next_episode_to_air
  end

  def seasons
    tmdb_tv_show.seasons.map do |season|
      Season.new(season)
    end
  end

  private

  attr_reader :tmdb_tv_show

  class Season
    attr_reader :name, :air_date, :episode_count, :season_number, :poster_url

    def initialize(season_hash)
      @name = season_hash.fetch("name")
      @air_date = Date.parse(season_hash.fetch("air_date")) if season_hash.fetch("air_date")
      @episode_count = season_hash.fetch("episode_count")
      @season_number = season_hash.fetch("season_number")
      @poster_url =
        "https://image.tmdb.org/t/p/w500#{season_hash.fetch("poster_path")}"
    end
  end
end
