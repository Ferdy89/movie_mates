# frozen_string_literal: true

class MovieMates::Import::TmdbMovieWrapper
  MovieNotFound = Class.new(StandardError)

  def self.by_id(tmdb_id)
    from_hash(Tmdb::Movie.detail(tmdb_id))
  end

  def self.from_hash(hash)
    tmdb_movie = Tmdb::Movie.new(hash)

    raise MovieNotFound if tmdb_movie.id.nil?

    new(tmdb_movie:)
  end

  attr_reader :tmdb_movie

  def initialize(tmdb_movie:)
    @tmdb_movie = tmdb_movie
  end

  def hash_with(*fields)
    fields.index_with { |field| public_send(field) }
  end

  delegate :imdb_id, :original_language, to: :tmdb_movie

  def tmdb_id
    tmdb_movie.id
  end

  def title
    tmdb_movie.original_title
  end

  def release_date
    Date.parse(tmdb_movie.release_date) if tmdb_movie.release_date.present?
  end

  def poster_url
    "https://image.tmdb.org/t/p/w500#{tmdb_movie.poster_path}"
  end

  def plot
    tmdb_movie.overview
  end

  def rating
    tmdb_movie.vote_average
  end

  def minutes
    tmdb_movie.runtime
  end

  def genres
    Array(tmdb_movie.genres).map { |genre| genre["name"] }
  end

  def url
    "https://www.themoviedb.org/movie/#{tmdb_id}"
  end

  def trailer_youtube_id
    Tmdb::Movie.trailers(tmdb_id).fetch("youtube").first&.fetch("source")
  end
end
