# frozen_string_literal: true

# Performs advanced queries to retrieve all the TV shows from a group with
# pagination and order
class MovieMates::TvShowLibrary
  ORDER_FIELDS = {
    "alphabetically_high" => "tmdb_tv_show_profiles.title ASC",
    "alphabetically_low" => "tmdb_tv_show_profiles.title DESC",
    "rating_high" => "tmdb_tv_show_profiles.rating DESC",
    "rating_low" => "tmdb_tv_show_profiles.rating ASC",
    "duration_high" => "tmdb_tv_show_profiles.minutes DESC",
    "duration_low" => "tmdb_tv_show_profiles.minutes ASC",
  }.freeze
  PAGE_SIZE = 12
  DEFAULT_SORTING = "tmdb_tv_show_profiles.title ASC"

  attr_reader :user, :sort_by, :page

  def initialize(user:, sort_by: nil, page: nil)
    @user = user
    @sort_by = ORDER_FIELDS.fetch(sort_by, DEFAULT_SORTING)
    @page = (1..total_pages).cover?(page.to_i) ? page.to_i : 1
  end

  def as_json(*)
    catalog = MovieMates::TvShowCatalog.new(
      current_user: user,
      tv_show_ids: tv_shows.pluck(:id)
    ).as_json

    catalog.merge(total_pages:, current_page: page)
  end

  private

  def tv_shows
    group_tv_shows.
      joins(:tmdb_tv_show_profile).
      order(sort_by).
      order(DEFAULT_SORTING).
      limit(PAGE_SIZE).
      offset((page - 1) * PAGE_SIZE)
  end

  def group_tv_shows
    TvShow.for_group(user.group)
  end

  def total_pages
    @total_pages ||= (group_tv_shows.count.to_f / PAGE_SIZE).ceil
  end
end
