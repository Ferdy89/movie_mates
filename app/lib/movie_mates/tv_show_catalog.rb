# frozen_string_literal: true

class MovieMates::TvShowCatalog
  def self.empty
    EmptyCatalog.new
  end

  def initialize(current_user:, tv_show_ids:)
    @current_user = current_user
    group = current_user.group
    unsorted_tv_shows = TvShow.with_profile(ids: tv_show_ids, group:)
    tv_shows_by_id = unsorted_tv_shows.index_by(&:id)
    # Re-sort by the original order passed in the IDs, order matters when
    # picking TV shows because the ones are the top are more "watchable".
    @tv_shows = tv_show_ids.map { |id| tv_shows_by_id[id] }
    @users = group.users
  end

  def as_json(*)
    {
      watchables: tv_shows.map do |tv_show|
        CatalogTvShow.as_json(tv_show, users, current_user)
      end,
    }
  end

  private

  attr_reader :current_user, :users, :tv_shows

  class CatalogTvShow < Struct.new(:tv_show, :users, :current_user)
    delegate :tmdb_profile, to: :tv_show
    delegate :tmdb_id, :title, :minutes, :poster_url, :rating, :release_date,
             to: :tmdb_profile

    def self.as_json(tv_show, users, current_user)
      new(tv_show, users, current_user).as_json
    end

    # rubocop:disable Metrics/AbcSize
    def as_json
      {
        slug: tv_show.slug,
        tmdb_id:,
        title:,
        year: release_date&.year,
        minutes:,
        rating:,
        poster: poster_url,
        comment_count: nomination.comments.count,
        contributor: nomination.contributor_name,
        current_user_vote: nomination.votes.opinion_for(user: current_user),
        votes: votes_from_users,
      }
    end
    # rubocop:enable Metrics/AbcSize

    private

    def nomination
      @nomination ||= tv_show.nominations.find do |tv_show_nomination|
        tv_show_nomination.group_id == current_user.group.id
      end
    end

    def votes_from_users
      users.map do |user|
        {
          user_name: user.name,
          opinion: nomination.votes.opinion_for(user:),
        }
      end
    end
  end

  class EmptyCatalog
    def as_json(*)
      {watchables: []}
    end
  end
end
