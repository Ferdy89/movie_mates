# frozen_string_literal: true

class MovieMates::MovieCatalog
  def self.empty
    EmptyCatalog.new
  end

  def initialize(current_user:, movie_ids:)
    @current_user = current_user
    group = current_user.group
    unsorted_movies = Movie.with_profile(ids: movie_ids, group:)
    movies_by_id = unsorted_movies.index_by(&:id)
    # Re-sort by the original order passed in the IDs, order matters when
    # picking movies because the ones are the top are more "watchable".
    @movies = movie_ids.map { |id| movies_by_id[id] }
    @users = group.users
  end

  def as_json(*)
    {
      watchables: movies.map do |movie|
        CatalogMovie.as_json(movie, users, current_user)
      end,
    }
  end

  private

  attr_reader :current_user, :users, :movies

  class CatalogMovie < Struct.new(:movie, :users, :current_user)
    delegate :tmdb_profile, to: :movie
    delegate :tmdb_id, :title, :minutes, :poster_url, :rating, :release_date,
             to: :tmdb_profile

    def self.as_json(movie, users, current_user)
      new(movie, users, current_user).as_json
    end

    # rubocop:disable Metrics/AbcSize
    def as_json
      {
        slug: movie.slug,
        tmdb_id:,
        title:,
        year: release_date&.year,
        minutes:,
        rating:,
        poster: poster_url,
        comment_count: nomination.comments.count,
        contributor: nomination.contributor_name,
        current_user_vote: nomination.votes.opinion_for(user: current_user),
        votes: votes_from_users,
      }
    end
    # rubocop:enable Metrics/AbcSize

    private

    def nomination
      @nomination ||= movie.nominations.find do |movie_nomination|
        movie_nomination.group_id == current_user.group.id
      end
    end

    def votes_from_users
      users.map do |user|
        {
          user_name: user.name,
          opinion: nomination.votes.opinion_for(user:),
        }
      end
    end
  end

  class EmptyCatalog
    def as_json(*)
      {watchables: []}
    end
  end
end
