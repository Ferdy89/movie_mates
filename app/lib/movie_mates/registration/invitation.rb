# frozen_string_literal: true

class MovieMates::Registration::Invitation
  def self.from(token:, user:)
    new(invitation: GroupInvitation.find_by!(token:), user:)
  end

  attr_reader :invitation, :user, :current_group

  def initialize(invitation:, user:)
    @invitation    = invitation
    @user          = user
    @current_group = user.group
  end

  def accept
    ActiveRecord::Base.transaction do
      move_nominations

      user.update!(group: new_group)

      current_group.destroy if current_group.users.count.zero?
      invitation.destroy
    end
  end

  private

  def move_nominations
    MovieNomination.where(group: user.group_id).find_each do |nomination|
      UserNomination.new(user, nomination).move_to(new_group)
    end
  end

  def new_group
    invitation.group
  end

  UserNomination = Struct.new(:user, :nomination) do
    def move_to(new_group)
      new_nomination = find_or_create_nomination(new_group)

      current_vote.update!(nomination: new_nomination)
    end

    private

    def find_or_create_nomination(new_group)
      MovieNomination.find_by(group: new_group, movie:) ||
        new_group.movie_nominations.create!(movie:, contributor: user)
    end

    def movie
      nomination.movie
    end

    def current_vote
      nomination.votes.find_by(voter: user) || NullVote.new
    end
  end

  NullVote = Class.new do
    def update(*)
    end
    alias_method :update!, :update
  end
end
