# frozen_string_literal: true

require "securerandom"

class MovieMates::Registration::NewGroup
  def self.create_with_random_name
    Group.create!(name: random_group_name)
  end

  def self.random_group_name
    I18n.t("movie_mates.registration.user_credentials.default_group_name",
           random_string: SecureRandom.hex(3))
  end
  private_class_method :random_group_name
end
