# frozen_string_literal: true

class MovieMates::Registration::GroupSplit
  def self.split_into_new_group(user:)
    new(user).split_into_new_group
  end

  def initialize(user)
    @user = user
  end

  def split_into_new_group
    ActiveRecord::Base.transaction do
      initial_group =
        Group.includes(movie_nominations: {votes: :voter}).find(user.group_id)
      new_group = MovieMates::Registration::NewGroup.create_with_random_name
      user.update!(group: new_group)
      duplicate_nominations(initial_group, new_group)
    end
  end

  private

  attr_reader :user

  def duplicate_nominations(initial_group, new_group)
    initial_group.movie_nominations.each do |nomination|
      new_nomination = new_group.movie_nominations.create!(
        contributor_id: nomination.contributor_id,
        movie_id: nomination.movie_id
      )

      user_vote = nomination.votes.find { |vote| vote.voter_id == user.id }
      user_vote.update!(nomination: new_nomination)
    end
  end
end
