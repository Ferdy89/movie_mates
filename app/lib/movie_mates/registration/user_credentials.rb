# frozen_string_literal: true

module MovieMates::Registration
  class UserCredentials
    def self.for(invitation_token:, omniauth_hash:)
      invitation = GroupInvitation.find_by(token: invitation_token)

      if invitation
        WithInvitation.
          new(omniauth_hash:, token: invitation_token)
      else
        new(omniauth_hash:)
      end
    end

    attr_reader :omniauth_hash

    def initialize(omniauth_hash:)
      @omniauth_hash = omniauth_hash
    end

    def register
      ActiveRecord::Base.transaction do
        update_user_information
        post_check_in_actions
      end

      user
    end

    private

    def update_user_information
      user.name = google_profile["first_name"] if user.name.blank?
      user.update!(
        email: google_profile["email"],
        google_pic: google_profile["image"],
        group:
      )
    end

    def user
      @user ||= User.find_or_initialize_by(
        provider: omniauth_hash["provider"],
        uid: omniauth_hash["uid"]
      )
    end

    def google_profile
      omniauth_hash["info"]
    end

    def group
      user.group || MovieMates::Registration::NewGroup.create_with_random_name
    end

    def post_check_in_actions
      nil
    end
  end

  class WithInvitation < UserCredentials
    attr_reader :omniauth_hash, :token

    def initialize(omniauth_hash:, token:)
      super(omniauth_hash:)

      @token = token
    end

    private

    def post_check_in_actions
      MovieMates::Registration::Invitation.from(token:, user:).accept
    end
  end
end
