# frozen_string_literal: true

# Performs advanced queries to retrieve all the movies from a group with
# pagination and order
class MovieMates::Library
  ORDER_FIELDS = {
    "alphabetically_high" => "tmdb_profiles.title ASC",
    "alphabetically_low" => "tmdb_profiles.title DESC",
    "rating_high" => "tmdb_profiles.rating DESC",
    "rating_low" => "tmdb_profiles.rating ASC",
    "duration_high" => "tmdb_profiles.minutes DESC",
    "duration_low" => "tmdb_profiles.minutes ASC",
  }.freeze
  PAGE_SIZE = 12
  DEFAULT_SORTING = "tmdb_profiles.title ASC"

  attr_reader :user, :sort_by, :page

  def initialize(user:, sort_by: nil, page: nil)
    @user = user
    @sort_by = ORDER_FIELDS.fetch(sort_by, DEFAULT_SORTING)
    @page = (1..total_pages).cover?(page.to_i) ? page.to_i : 1
  end

  def as_json(*)
    catalog = MovieMates::MovieCatalog.new(
      current_user: user,
      movie_ids: movies.pluck(:id)
    ).as_json

    catalog.merge(total_pages:, current_page: page)
  end

  private

  def movies
    group_movies.
      joins(:tmdb_profile).
      order(sort_by).
      order(DEFAULT_SORTING).
      limit(PAGE_SIZE).
      offset((page - 1) * PAGE_SIZE)
  end

  def group_movies
    Movie.for_group(user.group)
  end

  def total_pages
    @total_pages ||= (group_movies.count.to_f / PAGE_SIZE).ceil
  end
end
