# frozen_string_literal: true

class MovieMates::DelayedJob
  # Rely on the after_commit hook on the ApplicationRecord to enqueue the job
  # after the transaction is done.
  def self.enqueue(klass:, args: [])
    (Thread.current[:jobs] ||= []) << new(klass, args)
  end

  attr_reader :klass, :args

  def initialize(klass, args)
    @klass = klass
    @args = args
  end
end
