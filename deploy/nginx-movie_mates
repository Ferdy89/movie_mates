# Nginx configuration file for the MovieMates file, to be installed in
# /etc/nginx/sites-available/ and symlinked to /etc/nginx/sites-enabled/
#
# Listens for requests to moviemates.party on port 8080 (for HTTP) and 8081
# (for HTTPS) and proxies requests to a UNIX socket where Puma should be
# handling requests for the Rails app.
#
# Replace <DEPLOYMENT_PATH> with the path where MovieMates is deployed.

upstream puma {
    server unix:<DEPLOYMENT_PATH>/tmp/sockets/puma.sock fail_timeout=0;
}

server {
    listen [::]:8081 ssl ipv6only=on;
    listen 8081 ssl;
    server_name moviemates.party;

    # Any files found in the public directory are served directly without
    # reaching Puma, which is more efficient.
    root <DEPLOYMENT_PATH>/current/public;
    try_files $uri @app;

    location @app {
        proxy_pass http://puma;
        proxy_redirect off;

        # Set headers so Rails and some gems can properly interpret the original
        # requests to the servers.
        proxy_set_header Host               $host;
        proxy_set_header X-Real-IP          $remote_addr;
        proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Scheme $scheme;
        proxy_set_header X-Forwarded-Host   $host;

        # Ensure browsers send the Referrer header with requests towards from
        # this site to this same site. This is necessary for Rails' Forgery
        # Protection to work.
        add_header       Referrer-Policy    "same-origin";
    }

    error_page 404             /404.html;
    error_page 422             /422.html;
    error_page 500 502 503 504 /500.html;

    # Entries managed by LetsEncrypt to provide SSL encryption
    ssl_certificate /etc/letsencrypt/live/moviemates.party/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/moviemates.party/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}

server {
    listen 8080;
    listen [::]:8080;
    server_name moviemates.party;

    # Force HTTPS without having to reach Rails
    return 301 https://moviemates.party$request_uri;
}
