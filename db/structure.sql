SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: locale; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.locale AS ENUM (
    'en-US',
    'es-ES'
);


--
-- Name: opinion_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.opinion_type AS ENUM (
    'yes',
    'no',
    'abstention',
    'watched'
);


--
-- Name: votes_per_movie(integer[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.votes_per_movie(integer[]) RETURNS TABLE(movie_id integer, yes_votes bigint, no_or_watched_votes bigint)
    LANGUAGE sql
    AS $_$
        SELECT
          movies.id AS movie_id,
          COALESCE(SUM(CASE WHEN votes.opinion = 'yes'              THEN 1 ELSE 0 END), 0) AS yes_votes,
          COALESCE(SUM(CASE WHEN votes.opinion IN ('no', 'watched') THEN 1 ELSE 0 END), 0) AS no_or_watched_votes
        FROM movies
        JOIN movie_nominations ON movies.id = movie_nominations.movie_id
        JOIN votes ON movie_nominations.id = votes.nomination_id
          AND votes.nomination_type = 'MovieNomination'
        WHERE votes.voter_id = ANY($1)
        GROUP BY movies.id
      $_$;


--
-- Name: votes_per_tv_show(integer[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.votes_per_tv_show(integer[]) RETURNS TABLE(tv_show_id bigint, yes_votes bigint, no_or_watched_votes bigint)
    LANGUAGE sql
    AS $_$
        SELECT
          tv_shows.id AS tv_show_id,
          COALESCE(SUM(CASE WHEN votes.opinion = 'yes'              THEN 1 ELSE 0 END), 0) AS yes_votes,
          COALESCE(SUM(CASE WHEN votes.opinion IN ('no', 'watched') THEN 1 ELSE 0 END), 0) AS no_or_watched_votes
        FROM tv_shows
        JOIN tv_show_nominations ON tv_shows.id = tv_show_nominations.tv_show_id
        JOIN votes ON tv_show_nominations.id = votes.nomination_id
          AND votes.nomination_type = 'TvShowNomination'
        WHERE votes.voter_id = ANY($1)
        GROUP BY tv_shows.id
      $_$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: comments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.comments (
    id bigint NOT NULL,
    author_id integer NOT NULL,
    body text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    nomination_id bigint NOT NULL,
    nomination_type character varying NOT NULL
);


--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.comments_id_seq OWNED BY public.comments.id;


--
-- Name: group_invitations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.group_invitations (
    id bigint NOT NULL,
    group_id bigint NOT NULL,
    email character varying NOT NULL,
    token character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: group_invitations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.group_invitations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: group_invitations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.group_invitations_id_seq OWNED BY public.group_invitations.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.groups (
    id bigint NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.groups_id_seq OWNED BY public.groups.id;


--
-- Name: movie_nominations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.movie_nominations (
    id bigint NOT NULL,
    contributor_id integer NOT NULL,
    group_id bigint NOT NULL,
    movie_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: movie_nominations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.movie_nominations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: movie_nominations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.movie_nominations_id_seq OWNED BY public.movie_nominations.id;


--
-- Name: movies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.movies (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    slug character varying NOT NULL
);


--
-- Name: movies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.movies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: movies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.movies_id_seq OWNED BY public.movies.id;


--
-- Name: offers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.offers (
    id bigint NOT NULL,
    provider_id bigint NOT NULL,
    watchable_id bigint NOT NULL,
    monetization_type character varying NOT NULL,
    presentation_type character varying NOT NULL,
    price_cents integer DEFAULT 0 NOT NULL,
    price_currency character varying DEFAULT 'USD'::character varying NOT NULL,
    standard_web_url character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    country public.locale NOT NULL,
    watchable_type character varying NOT NULL
);


--
-- Name: offers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.offers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: offers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.offers_id_seq OWNED BY public.offers.id;


--
-- Name: providers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.providers (
    id bigint NOT NULL,
    provider_id integer NOT NULL,
    clear_name character varying NOT NULL,
    icon_url character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: providers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.providers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: providers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.providers_id_seq OWNED BY public.providers.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: seasons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.seasons (
    id bigint NOT NULL,
    tmdb_tv_show_profile_id bigint NOT NULL,
    tmdb_id bigint,
    name character varying,
    season_number integer,
    episode_count integer,
    air_date date,
    poster_url character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: seasons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.seasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: seasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.seasons_id_seq OWNED BY public.seasons.id;


--
-- Name: tmdb_profiles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tmdb_profiles (
    id bigint NOT NULL,
    movie_id bigint NOT NULL,
    tmdb_id integer NOT NULL,
    imdb_id character varying,
    title character varying NOT NULL,
    plot text,
    release_date date,
    poster_url character varying,
    rating numeric,
    genres character varying[],
    minutes integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    trailer_youtube_id character varying
);


--
-- Name: tmdb_profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tmdb_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tmdb_profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tmdb_profiles_id_seq OWNED BY public.tmdb_profiles.id;


--
-- Name: tmdb_tv_show_profiles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tmdb_tv_show_profiles (
    id bigint NOT NULL,
    tv_show_id bigint NOT NULL,
    tmdb_id integer NOT NULL,
    title character varying NOT NULL,
    plot text,
    release_date date,
    poster_url character varying,
    rating numeric,
    genres character varying[],
    minutes integer,
    in_production boolean NOT NULL,
    next_episode_to_air date,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: tmdb_tv_show_profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tmdb_tv_show_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tmdb_tv_show_profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tmdb_tv_show_profiles_id_seq OWNED BY public.tmdb_tv_show_profiles.id;


--
-- Name: tv_show_nominations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tv_show_nominations (
    id bigint NOT NULL,
    contributor_id integer NOT NULL,
    group_id bigint NOT NULL,
    tv_show_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    last_season_watched integer,
    last_episode_watched integer
);


--
-- Name: tv_show_nominations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tv_show_nominations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tv_show_nominations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tv_show_nominations_id_seq OWNED BY public.tv_show_nominations.id;


--
-- Name: tv_shows; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tv_shows (
    id bigint NOT NULL,
    title character varying,
    contributor_id integer,
    slug character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: tv_shows_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tv_shows_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tv_shows_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tv_shows_id_seq OWNED BY public.tv_shows.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying,
    last_sign_in_ip character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name character varying DEFAULT ''::character varying NOT NULL,
    provider character varying,
    uid character varying,
    group_id bigint,
    remember_token character varying,
    preferred_locale public.locale,
    google_pic character varying
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: votes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.votes (
    id integer NOT NULL,
    voter_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    nomination_id integer NOT NULL,
    opinion public.opinion_type,
    nomination_type character varying NOT NULL
);


--
-- Name: votes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.votes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: votes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.votes_id_seq OWNED BY public.votes.id;


--
-- Name: comments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comments ALTER COLUMN id SET DEFAULT nextval('public.comments_id_seq'::regclass);


--
-- Name: group_invitations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_invitations ALTER COLUMN id SET DEFAULT nextval('public.group_invitations_id_seq'::regclass);


--
-- Name: groups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.groups ALTER COLUMN id SET DEFAULT nextval('public.groups_id_seq'::regclass);


--
-- Name: movie_nominations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.movie_nominations ALTER COLUMN id SET DEFAULT nextval('public.movie_nominations_id_seq'::regclass);


--
-- Name: movies id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.movies ALTER COLUMN id SET DEFAULT nextval('public.movies_id_seq'::regclass);


--
-- Name: offers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.offers ALTER COLUMN id SET DEFAULT nextval('public.offers_id_seq'::regclass);


--
-- Name: providers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.providers ALTER COLUMN id SET DEFAULT nextval('public.providers_id_seq'::regclass);


--
-- Name: seasons id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seasons ALTER COLUMN id SET DEFAULT nextval('public.seasons_id_seq'::regclass);


--
-- Name: tmdb_profiles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tmdb_profiles ALTER COLUMN id SET DEFAULT nextval('public.tmdb_profiles_id_seq'::regclass);


--
-- Name: tmdb_tv_show_profiles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tmdb_tv_show_profiles ALTER COLUMN id SET DEFAULT nextval('public.tmdb_tv_show_profiles_id_seq'::regclass);


--
-- Name: tv_show_nominations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tv_show_nominations ALTER COLUMN id SET DEFAULT nextval('public.tv_show_nominations_id_seq'::regclass);


--
-- Name: tv_shows id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tv_shows ALTER COLUMN id SET DEFAULT nextval('public.tv_shows_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: votes id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.votes ALTER COLUMN id SET DEFAULT nextval('public.votes_id_seq'::regclass);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: comments comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: group_invitations group_invitations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_invitations
    ADD CONSTRAINT group_invitations_pkey PRIMARY KEY (id);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: movie_nominations movie_nominations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.movie_nominations
    ADD CONSTRAINT movie_nominations_pkey PRIMARY KEY (id);


--
-- Name: movies movies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.movies
    ADD CONSTRAINT movies_pkey PRIMARY KEY (id);


--
-- Name: offers offers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offers_pkey PRIMARY KEY (id);


--
-- Name: providers providers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.providers
    ADD CONSTRAINT providers_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: seasons seasons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seasons
    ADD CONSTRAINT seasons_pkey PRIMARY KEY (id);


--
-- Name: tmdb_profiles tmdb_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tmdb_profiles
    ADD CONSTRAINT tmdb_profiles_pkey PRIMARY KEY (id);


--
-- Name: tmdb_tv_show_profiles tmdb_tv_show_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tmdb_tv_show_profiles
    ADD CONSTRAINT tmdb_tv_show_profiles_pkey PRIMARY KEY (id);


--
-- Name: tv_show_nominations tv_show_nominations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tv_show_nominations
    ADD CONSTRAINT tv_show_nominations_pkey PRIMARY KEY (id);


--
-- Name: tv_shows tv_shows_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tv_shows
    ADD CONSTRAINT tv_shows_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: votes votes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.votes
    ADD CONSTRAINT votes_pkey PRIMARY KEY (id);


--
-- Name: index_comments_on_author_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_comments_on_author_id ON public.comments USING btree (author_id);


--
-- Name: index_comments_on_nomination_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_comments_on_nomination_id ON public.comments USING btree (nomination_id);


--
-- Name: index_group_invitations_on_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_group_invitations_on_group_id ON public.group_invitations USING btree (group_id);


--
-- Name: index_group_invitations_on_token; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_group_invitations_on_token ON public.group_invitations USING btree (token);


--
-- Name: index_groups_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_groups_on_name ON public.groups USING btree (name);


--
-- Name: index_movie_nominations_on_contributor_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movie_nominations_on_contributor_id ON public.movie_nominations USING btree (contributor_id);


--
-- Name: index_movie_nominations_on_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movie_nominations_on_group_id ON public.movie_nominations USING btree (group_id);


--
-- Name: index_movie_nominations_on_group_id_and_movie_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_movie_nominations_on_group_id_and_movie_id ON public.movie_nominations USING btree (group_id, movie_id);


--
-- Name: index_movie_nominations_on_movie_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movie_nominations_on_movie_id ON public.movie_nominations USING btree (movie_id);


--
-- Name: index_movies_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_movies_on_slug ON public.movies USING btree (slug);


--
-- Name: index_offers_on_provider_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_offers_on_provider_id ON public.offers USING btree (provider_id);


--
-- Name: index_offers_on_watchable_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_offers_on_watchable_id ON public.offers USING btree (watchable_id);


--
-- Name: index_providers_on_provider_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_providers_on_provider_id ON public.providers USING btree (provider_id);


--
-- Name: index_seasons_on_tmdb_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_seasons_on_tmdb_id ON public.seasons USING btree (tmdb_id);


--
-- Name: index_seasons_on_tmdb_tv_show_profile_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_seasons_on_tmdb_tv_show_profile_id ON public.seasons USING btree (tmdb_tv_show_profile_id);


--
-- Name: index_tmdb_profiles_on_imdb_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_tmdb_profiles_on_imdb_id ON public.tmdb_profiles USING btree (imdb_id);


--
-- Name: index_tmdb_profiles_on_movie_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_tmdb_profiles_on_movie_id ON public.tmdb_profiles USING btree (movie_id);


--
-- Name: index_tmdb_profiles_on_tmdb_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_tmdb_profiles_on_tmdb_id ON public.tmdb_profiles USING btree (tmdb_id);


--
-- Name: index_tmdb_tv_show_profiles_on_tmdb_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_tmdb_tv_show_profiles_on_tmdb_id ON public.tmdb_tv_show_profiles USING btree (tmdb_id);


--
-- Name: index_tmdb_tv_show_profiles_on_tv_show_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_tmdb_tv_show_profiles_on_tv_show_id ON public.tmdb_tv_show_profiles USING btree (tv_show_id);


--
-- Name: index_tv_show_nominations_on_contributor_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_tv_show_nominations_on_contributor_id ON public.tv_show_nominations USING btree (contributor_id);


--
-- Name: index_tv_show_nominations_on_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_tv_show_nominations_on_group_id ON public.tv_show_nominations USING btree (group_id);


--
-- Name: index_tv_show_nominations_on_group_id_and_tv_show_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_tv_show_nominations_on_group_id_and_tv_show_id ON public.tv_show_nominations USING btree (group_id, tv_show_id);


--
-- Name: index_tv_show_nominations_on_tv_show_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_tv_show_nominations_on_tv_show_id ON public.tv_show_nominations USING btree (tv_show_id);


--
-- Name: index_tv_shows_on_contributor_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_tv_shows_on_contributor_id ON public.tv_shows USING btree (contributor_id);


--
-- Name: index_tv_shows_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_tv_shows_on_slug ON public.tv_shows USING btree (slug);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_group_id ON public.users USING btree (group_id);


--
-- Name: index_users_on_provider_and_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_provider_and_uid ON public.users USING btree (provider, uid);


--
-- Name: index_votes_on_nomination_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_votes_on_nomination_id ON public.votes USING btree (nomination_id);


--
-- Name: index_votes_on_voter_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_votes_on_voter_id ON public.votes USING btree (voter_id);


--
-- Name: index_votes_on_voter_id_and_nomination_id_and_nomination_type; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_votes_on_voter_id_and_nomination_id_and_nomination_type ON public.votes USING btree (voter_id, nomination_id, nomination_type);


--
-- Name: movie_nominations fk_rails_1fd5956ec5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.movie_nominations
    ADD CONSTRAINT fk_rails_1fd5956ec5 FOREIGN KEY (contributor_id) REFERENCES public.users(id);


--
-- Name: movie_nominations fk_rails_29cebf1c3c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.movie_nominations
    ADD CONSTRAINT fk_rails_29cebf1c3c FOREIGN KEY (movie_id) REFERENCES public.movies(id);


--
-- Name: tv_show_nominations fk_rails_2d5a3fe5ce; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tv_show_nominations
    ADD CONSTRAINT fk_rails_2d5a3fe5ce FOREIGN KEY (group_id) REFERENCES public.groups(id);


--
-- Name: movie_nominations fk_rails_33bc938eeb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.movie_nominations
    ADD CONSTRAINT fk_rails_33bc938eeb FOREIGN KEY (group_id) REFERENCES public.groups(id);


--
-- Name: votes fk_rails_40393c889d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.votes
    ADD CONSTRAINT fk_rails_40393c889d FOREIGN KEY (voter_id) REFERENCES public.users(id);


--
-- Name: tv_shows fk_rails_5e0bbace03; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tv_shows
    ADD CONSTRAINT fk_rails_5e0bbace03 FOREIGN KEY (contributor_id) REFERENCES public.users(id);


--
-- Name: offers fk_rails_a32a29b86a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.offers
    ADD CONSTRAINT fk_rails_a32a29b86a FOREIGN KEY (provider_id) REFERENCES public.providers(id);


--
-- Name: tv_show_nominations fk_rails_ac516ff866; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tv_show_nominations
    ADD CONSTRAINT fk_rails_ac516ff866 FOREIGN KEY (tv_show_id) REFERENCES public.tv_shows(id);


--
-- Name: tv_show_nominations fk_rails_b2da5276ae; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tv_show_nominations
    ADD CONSTRAINT fk_rails_b2da5276ae FOREIGN KEY (contributor_id) REFERENCES public.users(id);


--
-- Name: group_invitations fk_rails_f27a784b15; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_invitations
    ADD CONSTRAINT fk_rails_f27a784b15 FOREIGN KEY (group_id) REFERENCES public.groups(id);


--
-- Name: users fk_rails_f40b3f4da6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_rails_f40b3f4da6 FOREIGN KEY (group_id) REFERENCES public.groups(id);


--
-- Name: comments fk_rails_f44b1e3c8a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT fk_rails_f44b1e3c8a FOREIGN KEY (author_id) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20241111201014'),
('20210216013825'),
('20210125023123'),
('20210108032337'),
('20210104055034'),
('20210103022611'),
('20210103004954'),
('20201228010634'),
('20201214030732'),
('20201208024051'),
('20201207045138'),
('20201207043826'),
('20201207034303'),
('20201207033750'),
('20201207032759'),
('20201207030920'),
('20201104015006'),
('20201103055701'),
('20201103012456'),
('20201103001449'),
('20201016034257'),
('20201016034247'),
('20190128011756'),
('20190124000226'),
('20190114162835'),
('20190109033530'),
('20190109032847'),
('20181231214104'),
('20181122191605'),
('20181112000515'),
('20181111055620'),
('20181111050327'),
('20181110183218'),
('20181106025305'),
('20181104082203'),
('20181022005544'),
('20180909175941'),
('20180611002316'),
('20180602233704'),
('20180602232727'),
('20180602190936'),
('20180529011803'),
('20180528211159'),
('20180527201220'),
('20180122015933'),
('20171231062011'),
('20171230194643'),
('20171129011220'),
('20171020022151'),
('20171015175123'),
('20171015063205'),
('20170813214711'),
('20170813205327'),
('20170813043046'),
('20170320002610'),
('20161205214957'),
('20160918005316'),
('20160917231053'),
('20160828041609'),
('20160827204147'),
('20160814000951'),
('20160814000244');

