# frozen_string_literal: true

class PreferredLocaleAsFullLocale < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL
      CREATE TYPE locale AS ENUM ('en-US', 'es-ES');
      ALTER TABLE users ALTER COLUMN preferred_locale TYPE locale USING preferred_locale::locale;
    SQL
  end

  def down
    change_column :users, :preferred_locale, :string

    execute <<-SQL
      DROP TYPE locale;
    SQL
  end
end
