# frozen_string_literal: true

class EnforceUserInGroup < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :group_id, :bigint
    add_index  :users, :group_id
    add_foreign_key :users, :groups

    change_column :groups, :name, :string, null: false # rubocop:disable Rails/ReversibleMigration
    add_index :groups, :name, unique: true
  end
end
