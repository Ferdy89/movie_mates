# frozen_string_literal: true

class DropUniqueUserNameMakeNominationsUnique < ActiveRecord::Migration[5.1]
  def up
    remove_index :users, :name
    add_index :movie_nominations, [:group_id, :movie_id], unique: true
  end

  def down
    remove_index :movie_nominations, [:group_id, :movie_id]
    add_index :users, :name, unique: true
  end
end
