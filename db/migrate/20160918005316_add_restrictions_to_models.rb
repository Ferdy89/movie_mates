# frozen_string_literal: true

class AddRestrictionsToModels < ActiveRecord::Migration[5.0]
  def change
    change_column_null :votes, :voter_id, false
    change_column_null :votes, :opinion_type_id, false
    change_column_null :votes, :movie_id, false

    change_column_null :opinion_types, :name, false

    change_column_null :movies, :title, false
    change_column_null :movies, :contributor_id, false
  end
end
