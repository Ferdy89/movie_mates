# frozen_string_literal: true

class DropProvidersDisplayPriorityColumn < ActiveRecord::Migration[7.1]
  def up
    remove_column :providers, :display_priority
  end

  def down
    add_column :providers, :display_priority, :integer
    add_index :providers, :display_priority
  end
end
