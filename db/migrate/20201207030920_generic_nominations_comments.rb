# frozen_string_literal: true

class GenericNominationsComments < ActiveRecord::Migration[6.0]
  def up
    rename_column :comments, :movie_nomination_id, :nomination_id
    add_column :comments, :nomination_type, :string
    execute "UPDATE comments SET nomination_type = 'MovieNomination'"
    change_column_null :comments, :nomination_type, false
  end

  def down
    remove_column :comments, :nomination_type
    rename_column :comments, :nomination_id, :movie_nomination_id
  end
end
