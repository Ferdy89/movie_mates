# frozen_string_literal: true

class MoveLastSeasonAndEpisodeWatchedToTvNomination < ActiveRecord::Migration[6.0]
  def change
    add_column :tv_show_nominations, :last_season_watched, :integer # rubocop:disable Rails/BulkChangeTable
    add_column :tv_show_nominations, :last_episode_watched, :integer
    remove_column :tmdb_tv_show_profiles, :last_season_watched, :integer # rubocop:disable Rails/BulkChangeTable
    remove_column :tmdb_tv_show_profiles, :last_episode_watched, :integer
  end
end
