# frozen_string_literal: true

class AddLocaleToUser < ActiveRecord::Migration[5.1]
  def up
    execute <<-SQL
      CREATE TYPE locale AS ENUM ('en', 'es');
    SQL

    add_column :users, :preferred_locale, :locale
  end

  def down
    remove_column :users, :preferred_locale

    execute <<-SQL
      DROP TYPE locale;
    SQL
  end
end
