# frozen_string_literal: true

class AddConstraintOneVotePerUserAndNomination < ActiveRecord::Migration[5.1]
  def change
    add_index :votes, [:voter_id, :movie_nomination_id], unique: true
  end
end
