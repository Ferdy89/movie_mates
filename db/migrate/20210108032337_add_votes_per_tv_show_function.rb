# frozen_string_literal: true

class AddVotesPerTvShowFunction < ActiveRecord::Migration[6.0]
  def up
    # execute "DROP FUNCTION votes_per_tv_show(integer[])"
    execute <<-SQL
      CREATE FUNCTION votes_per_tv_show(integer[])
        RETURNS TABLE (tv_show_id bigint, yes_votes bigint, no_or_watched_votes bigint)
      AS
      $body$
        SELECT
          tv_shows.id AS tv_show_id,
          COALESCE(SUM(CASE WHEN votes.opinion = 'yes'              THEN 1 ELSE 0 END), 0) AS yes_votes,
          COALESCE(SUM(CASE WHEN votes.opinion IN ('no', 'watched') THEN 1 ELSE 0 END), 0) AS no_or_watched_votes
        FROM tv_shows
        JOIN tv_show_nominations ON tv_shows.id = tv_show_nominations.tv_show_id
        JOIN votes ON tv_show_nominations.id = votes.nomination_id
          AND votes.nomination_type = 'TvShowNomination'
        WHERE votes.voter_id = ANY($1)
        GROUP BY tv_shows.id
      $body$
      LANGUAGE sql;
    SQL
  end

  def down
  end
end
