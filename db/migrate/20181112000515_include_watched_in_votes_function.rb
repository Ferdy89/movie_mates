# frozen_string_literal: true

class IncludeWatchedInVotesFunction < ActiveRecord::Migration[5.1]
  def up
    execute "DROP FUNCTION votes_per_movie(integer[])"
    execute <<-SQL
      CREATE FUNCTION votes_per_movie(integer[])
        RETURNS TABLE (movie_id integer, yes_votes bigint, no_or_watched_votes bigint)
      AS
      $body$
        SELECT
          movies.id AS movie_id,
          COALESCE(SUM(CASE WHEN votes.opinion = 'yes'              THEN 1 ELSE 0 END), 0) AS yes_votes,
          COALESCE(SUM(CASE WHEN votes.opinion IN ('no', 'watched') THEN 1 ELSE 0 END), 0) AS no_or_watched_votes
        FROM movies
        JOIN movie_nominations ON movies.id = movie_nominations.movie_id
        JOIN votes ON movie_nominations.id = votes.movie_nomination_id
        WHERE votes.voter_id = ANY($1)
        GROUP BY movies.id
      $body$
      LANGUAGE sql;
    SQL
  end

  def down
  end
end
