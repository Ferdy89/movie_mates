# frozen_string_literal: true

class RemoveUniqueGroupName < ActiveRecord::Migration[6.0]
  def up
    remove_index :groups, :name
    add_index :groups, :name, unique: false
  end

  def down
    remove_index :groups, :name
    add_index :groups, :name, unique: true
  end
end
