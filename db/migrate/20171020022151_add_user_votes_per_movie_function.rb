# frozen_string_literal: true

class AddUserVotesPerMovieFunction < ActiveRecord::Migration[5.1]
  def up
    execute <<-SQL
      CREATE OR REPLACE FUNCTION votes_per_movie(integer[])
        RETURNS TABLE (movie_id integer, yes_votes bigint, abstention_votes bigint, no_votes bigint)
      AS
      $body$
        SELECT
          movies.id AS movie_id,
          COALESCE(SUM(CASE WHEN opinion_types.name = 'yes'        THEN 1 ELSE 0 END), 0) AS yes_votes,
          COALESCE(SUM(CASE WHEN opinion_types.name = 'abstention' THEN 1 ELSE 0 END), 0) AS abstention_votes,
          COALESCE(SUM(CASE WHEN opinion_types.name = 'no'         THEN 1 ELSE 0 END), 0) AS no_votes
        FROM movies
        JOIN votes ON movies.id = votes.movie_id
        JOIN opinion_types ON votes.opinion_type_id = opinion_types.id
        WHERE votes.voter_id = ANY($1)
        GROUP BY movies.id
      $body$
      LANGUAGE sql;
    SQL
  end

  def down
    execute "DROP FUNCTION IF EXISTS votes_per_movie(integer[])"
  end
end
