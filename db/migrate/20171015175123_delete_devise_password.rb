# frozen_string_literal: true

class DeleteDevisePassword < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :encrypted_password # rubocop:disable Rails/ReversibleMigration
    add_index :users, [:provider, :uid], unique: true
  end
end
