# frozen_string_literal: true

class MigratePolymorphicIndexes < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key :votes, :nominations, to_table: :movie_nominations
    remove_foreign_key :comments, :nominations, to_table: :movie_nominations
  end
end
