# frozen_string_literal: true

class AddTvShowTable < ActiveRecord::Migration[6.0]
  def change
    create_table :tv_shows do |t|
      t.string :title
      t.integer :contributor_id, index: true
      t.string :slug, index: true, unique: true, null: false

      t.timestamps
    end

    add_foreign_key :tv_shows, :users, column: :contributor_id
  end
end
