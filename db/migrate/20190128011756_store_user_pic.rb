# frozen_string_literal: true

class StoreUserPic < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :google_pic, :string
  end
end
