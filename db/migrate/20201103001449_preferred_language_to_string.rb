# frozen_string_literal: true

class PreferredLanguageToString < ActiveRecord::Migration[5.2]
  def up
    change_column :users, :preferred_locale, :string

    execute <<-SQL
      DROP TYPE locale;
    SQL
  end

  def down
    execute <<-SQL
      CREATE TYPE locale AS ENUM ('en', 'es');
      ALTER TABLE users ALTER COLUMN preferred_locale TYPE locale USING preferred_locale::locale;
    SQL
  end
end
