# frozen_string_literal: true

class CreateTmdbProfile < ActiveRecord::Migration[5.1]
  def change
    create_table :tmdb_profiles do |t| # rubocop:disable Rails/CreateTableWithTimestamps
      t.references :movie,   null: false, index: false
      t.integer    :tmdb_id, null: false
      t.string     :imdb_id, null: false
      t.string     :title,   null: false
      t.text       :plot
      t.date       :release_date
      t.string     :poster_url
      t.decimal    :rating
      t.string     :genres, array: true
      t.integer    :minutes

      t.index :movie_id, unique: true
      t.index :tmdb_id,  unique: true
      t.index :imdb_id,  unique: true
    end
  end
end
