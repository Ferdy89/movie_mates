# frozen_string_literal: true

class OffersCountryNotNullable < ActiveRecord::Migration[5.2]
  def change
    change_column_null :offers, :country, false
  end
end
