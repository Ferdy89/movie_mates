# frozen_string_literal: true

class DropImdbProfiles < ActiveRecord::Migration[5.1]
  def change
    drop_table :imdb_profiles # rubocop:disable Rails/ReversibleMigration
  end
end
