# frozen_string_literal: true

class VotesToNominations < ActiveRecord::Migration[5.1]
  def change
    add_column :votes, :movie_nomination_id, :integer
    add_index  :votes, :movie_nomination_id
    add_foreign_key :votes, :movie_nominations, column: :movie_nomination_id
  end
end
