# frozen_string_literal: true

class CreateMovieNominations < ActiveRecord::Migration[5.1]
  def change
    create_table :movie_nominations do |t| # rubocop:disable Rails/CreateTableWithTimestamps
      t.integer    :contributor_id, null: false, index: true
      t.references :group,          null: false, index: true, foreign_key: true
      t.references :movie,          null: false, index: true, foreign_key: true
    end
    add_foreign_key :movie_nominations, :users, column: :contributor_id
  end
end
