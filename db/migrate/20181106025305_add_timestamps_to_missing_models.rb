# frozen_string_literal: true

class AddTimestampsToMissingModels < ActiveRecord::Migration[5.1]
  def change
    tables_to_update = [:movie_nominations, :tmdb_profiles]
    tables_to_update.each do |table|
      add_timestamps(table, null: true)
    end
  end
end
