# frozen_string_literal: true

class RemoveMinutesNullConstraint < ActiveRecord::Migration[5.1]
  def change
    change_column_null :imdb_profiles, :minutes, true
  end
end
