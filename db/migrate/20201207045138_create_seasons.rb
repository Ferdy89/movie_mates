# frozen_string_literal: true

class CreateSeasons < ActiveRecord::Migration[6.0]
  def change
    create_table :seasons do |t|
      t.references :tmdb_tv_show_profile, null: false, index: false
      t.bigint     :tmdb_id
      t.string     :name
      t.integer    :season_number
      t.integer    :episode_count
      t.date       :air_date
      t.string     :poster_url

      t.index :tmdb_tv_show_profile_id, unique: true
      t.index :tmdb_id,                 unique: true

      t.timestamps
    end
  end
end
