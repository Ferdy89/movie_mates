# frozen_string_literal: true

class DropOpinionTypesTable < ActiveRecord::Migration[5.1]
  def change
    change_column_null :votes, :opinion, null: false

    remove_foreign_key :votes, :opinion_types
    remove_column :votes, :opinion_type_id # rubocop:disable Rails/ReversibleMigration
    drop_table :opinion_types # rubocop:disable Rails/ReversibleMigration

    execute <<-SQL # rubocop:disable Rails/ReversibleMigration
      CREATE OR REPLACE FUNCTION votes_per_movie(integer[])
        RETURNS TABLE (movie_id integer, yes_votes bigint, abstention_votes bigint, no_votes bigint)
      AS
      $body$
        SELECT
          movies.id AS movie_id,
          COALESCE(SUM(CASE WHEN votes.opinion = 'yes'        THEN 1 ELSE 0 END), 0) AS yes_votes,
          COALESCE(SUM(CASE WHEN votes.opinion = 'abstention' THEN 1 ELSE 0 END), 0) AS abstention_votes,
          COALESCE(SUM(CASE WHEN votes.opinion = 'no'         THEN 1 ELSE 0 END), 0) AS no_votes
        FROM movies
        JOIN movie_nominations ON movies.id = movie_nominations.movie_id
        JOIN votes ON movie_nominations.id = votes.movie_nomination_id
        WHERE votes.voter_id = ANY($1)
        GROUP BY movies.id
      $body$
      LANGUAGE sql;
    SQL
  end
end
