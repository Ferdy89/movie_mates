# frozen_string_literal: true

class ChangeOpinionTypesToEnums < ActiveRecord::Migration[5.1]
  def up
    execute <<-SQL
      CREATE TYPE opinion_type AS ENUM ('yes', 'no', 'abstention', 'watched');
    SQL

    add_column :votes, :opinion, :opinion_type
  end

  def down
    remove_column :votes, :opinion

    execute <<-SQL
      DROP TYPE opinion_type;
    SQL
  end
end
