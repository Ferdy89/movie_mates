# frozen_string_literal: true

class CreateImdbProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :imdb_profiles do |t|
      t.references :movie,    null: false
      t.integer :imdb_id,     null: false
      t.string  :url,         null: false
      t.string  :title,       null: false
      t.integer :year,        null: false
      t.date    :release_date
      t.text    :plot
      t.integer :minutes, null: false

      t.decimal :rating
      t.integer :votes

      t.string  :poster
      t.string  :trailer_url
      t.string  :tagline
      t.string  :mpaa_rating

      t.string  :director,        array: true
      t.string  :cast_members,    array: true
      t.string  :cast_member_ids, array: true
      t.string  :cast_characters, array: true

      t.string  :genres,          array: true
      t.string  :languages,       array: true
      t.string  :countries,       array: true
      t.string  :company

      t.timestamps
    end

    add_index :imdb_profiles, :imdb_id, unique: true
    add_foreign_key :imdb_profiles, :movies
  end
end
