# frozen_string_literal: true

class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.integer    :author_id, null: false, index: true
      t.text       :body, null: false
      t.timestamps
      t.references :movie_nomination,
                   null: false, index: true, foreign_key: true
    end

    add_foreign_key :comments, :users, column: :author_id
  end
end
