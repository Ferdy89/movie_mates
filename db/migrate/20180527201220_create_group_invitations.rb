# frozen_string_literal: true

class CreateGroupInvitations < ActiveRecord::Migration[5.1]
  def change
    create_table :group_invitations do |t|
      t.references :group, null: false, foreign_key: true
      t.string :email, null: false
      t.string :token, null: false, index: true

      t.timestamps
    end
  end
end
