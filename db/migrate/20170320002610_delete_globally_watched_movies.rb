# frozen_string_literal: true

class DeleteGloballyWatchedMovies < ActiveRecord::Migration[5.0]
  def up
    watched = OpinionType.find_or_create_by!(name: "watched")

    votes = Vote.
      joins(:movie, :opinion_type).
      includes(:opinion_type).
      where(movies: {watched: true}).
      where(opinion_types: {name: ["yes", "abstention"]})

    votes.each do |vote|
      vote.opinion_type = watched
      vote.save!
    end

    remove_column :movies, :watched
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
