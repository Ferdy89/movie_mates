# frozen_string_literal: true

class PolymorphicOffers < ActiveRecord::Migration[6.0]
  def up
    rename_column :offers, :movie_id, :watchable_id
    add_column :offers, :watchable_type, :string
    execute "UPDATE offers SET watchable_type = 'Movie'"
    change_column_null :offers, :watchable_type, false
  end

  def down
    remove_column :offers, :watchable_type
    rename_column :offers, :watchable_id, :movie_id
  end
end
