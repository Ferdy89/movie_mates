# frozen_string_literal: true

class AddTrailerYoutubeIdField < ActiveRecord::Migration[5.1]
  def change
    add_column :tmdb_profiles, :trailer_youtube_id, :string
  end
end
