# frozen_string_literal: true

class SlugsNotNull < ActiveRecord::Migration[5.1]
  def change
    change_column_null :movies, :slug, false
  end
end
