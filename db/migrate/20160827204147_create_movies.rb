# frozen_string_literal: true

class CreateMovies < ActiveRecord::Migration[5.0]
  def change
    create_table :movies do |t|
      t.string :title
      t.integer :contributor_id, index: true

      t.timestamps
    end

    add_foreign_key :movies, :users, column: :contributor_id
  end
end
