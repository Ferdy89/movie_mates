# frozen_string_literal: true

class RemoveStuffFromMovieThatBelongsToNominations < ActiveRecord::Migration[5.1]
  def change
    remove_column :movies, :contributor_id, :integer, null: false
    remove_column :votes,  :movie_id,       :integer, null: false
  end
end
