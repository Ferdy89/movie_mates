# frozen_string_literal: true

class RemoveOffersForeignKey < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key :offers, :movies
  end
end
