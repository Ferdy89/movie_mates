# frozen_string_literal: true

class RequireNominationOnVote < ActiveRecord::Migration[5.1]
  def change
    change_column :votes, :movie_nomination_id, :integer, null: false # rubocop:disable Rails/ReversibleMigration
  end
end
