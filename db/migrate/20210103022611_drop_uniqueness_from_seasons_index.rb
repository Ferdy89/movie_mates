# frozen_string_literal: true

class DropUniquenessFromSeasonsIndex < ActiveRecord::Migration[6.0]
  def change
    remove_index :seasons, :tmdb_tv_show_profile_id
    add_index :seasons, :tmdb_tv_show_profile_id
  end
end
