# frozen_string_literal: true

class AddTvShowNominationsTable < ActiveRecord::Migration[6.0]
  def change
    create_table :tv_show_nominations do |t|
      t.integer    :contributor_id, null: false, index: true
      t.references :group,          null: false, index: true, foreign_key: true
      t.references :tv_show,        null: false, index: true, foreign_key: true

      t.timestamps
    end

    add_index :tv_show_nominations, [:group_id, :tv_show_id], unique: true

    add_foreign_key :tv_show_nominations, :users, column: :contributor_id
  end
end
