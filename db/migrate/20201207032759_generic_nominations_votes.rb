# frozen_string_literal: true

class GenericNominationsVotes < ActiveRecord::Migration[6.0]
  def up
    rename_column :votes, :movie_nomination_id, :nomination_id
    add_column :votes, :nomination_type, :string
    execute "UPDATE votes SET nomination_type = 'MovieNomination'"
    change_column_null :votes, :nomination_type, false

    execute "DROP FUNCTION votes_per_movie(integer[])"
    execute <<-SQL
      CREATE FUNCTION votes_per_movie(integer[])
        RETURNS TABLE (movie_id integer, yes_votes bigint, no_or_watched_votes bigint)
      AS
      $body$
        SELECT
          movies.id AS movie_id,
          COALESCE(SUM(CASE WHEN votes.opinion = 'yes'              THEN 1 ELSE 0 END), 0) AS yes_votes,
          COALESCE(SUM(CASE WHEN votes.opinion IN ('no', 'watched') THEN 1 ELSE 0 END), 0) AS no_or_watched_votes
        FROM movies
        JOIN movie_nominations ON movies.id = movie_nominations.movie_id
        JOIN votes ON movie_nominations.id = votes.nomination_id
          AND votes.nomination_type = 'MovieNomination'
        WHERE votes.voter_id = ANY($1)
        GROUP BY movies.id
      $body$
      LANGUAGE sql;
    SQL
  end

  def down
    remove_column :votes, :nomination_type
    rename_column :votes, :nomination_id, :movie_nomination_id

    # Last version in the schema, not ideal but better than irreversible
    execute "DROP FUNCTION IF EXISTS votes_per_movie(integer[])"
    execute <<-SQL
      CREATE FUNCTION votes_per_movie(integer[])
        RETURNS TABLE (movie_id integer, yes_votes bigint, no_or_watched_votes bigint)
      AS
      $body$
        SELECT
          movies.id AS movie_id,
          COALESCE(SUM(CASE WHEN votes.opinion = 'yes'              THEN 1 ELSE 0 END), 0) AS yes_votes,
          COALESCE(SUM(CASE WHEN votes.opinion IN ('no', 'watched') THEN 1 ELSE 0 END), 0) AS no_or_watched_votes
        FROM movies
        JOIN movie_nominations ON movies.id = movie_nominations.movie_id
        JOIN votes ON movie_nominations.id = votes.movie_nomination_id
        WHERE votes.voter_id = ANY($1)
        GROUP BY movies.id
      $body$
      LANGUAGE sql;
    SQL
  end
end
