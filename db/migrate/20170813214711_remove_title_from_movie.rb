# frozen_string_literal: true

class RemoveTitleFromMovie < ActiveRecord::Migration[5.1]
  def change
    remove_column :movies, :title # rubocop:disable Rails/ReversibleMigration
  end
end
