# frozen_string_literal: true

class CreateProviders < ActiveRecord::Migration[5.2]
  def change
    create_table :providers do |t|
      t.integer :provider_id, null: false, unique: true, index: true
      t.string :clear_name, null: false
      t.integer :display_priority, index: true
      t.string :icon_url

      t.timestamps
    end
  end
end
