# frozen_string_literal: true

class SpecifyUniquenessIndexOnNominationVotes < ActiveRecord::Migration[6.0]
  def change
    remove_index :votes, [:voter_id, :nomination_id]
    add_index :votes,
              [:voter_id, :nomination_id, :nomination_type], unique: true
  end
end
