# frozen_string_literal: true

class MakeAllTimestampsNotNull < ActiveRecord::Migration[5.1]
  def change
    change_column_null :movie_nominations, :created_at, false
    change_column_null :movie_nominations, :updated_at, false
    change_column_null :tmdb_profiles, :created_at, false
    change_column_null :tmdb_profiles, :updated_at, false
  end
end
