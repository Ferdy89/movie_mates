# frozen_string_literal: true

class MakeSlugsUnique < ActiveRecord::Migration[5.1]
  def change
    remove_index :movies, :slug
    add_index :movies, :slug, unique: true
  end
end
