# frozen_string_literal: true

class CreateVotesAndOpinions < ActiveRecord::Migration[5.0]
  def change
    create_table :opinion_types do |t|
      t.string :name, limit: 10

      t.timestamps
    end

    create_table :votes do |t|
      t.integer :voter_id, index: true
      t.references :opinion_type, index: true, foreign_key: true
      t.references :movie, index: true, foreign_key: true

      t.timestamps
    end
    add_foreign_key :votes, :users, column: :voter_id
  end
end
