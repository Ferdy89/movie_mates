# frozen_string_literal: true

class AddOmniauthToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :provider, :string # rubocop:disable Rails/BulkChangeTable
    add_column :users, :uid, :string
  end
end
