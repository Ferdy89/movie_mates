# frozen_string_literal: true

class CreateTmdbTvShowProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :tmdb_tv_show_profiles do |t|
      t.references :tv_show, null: false, index: false
      t.integer    :tmdb_id, null: false
      t.string     :title,   null: false
      t.text       :plot
      t.date       :release_date
      t.string     :poster_url
      t.decimal    :rating
      t.string     :genres, array: true
      t.integer    :minutes
      t.boolean    :in_production, null: false
      t.date       :next_episode_to_air
      t.integer    :last_season_watched
      t.integer    :last_episode_watched

      t.index :tv_show_id, unique: true
      t.index :tmdb_id,    unique: true

      t.timestamps
    end
  end
end
