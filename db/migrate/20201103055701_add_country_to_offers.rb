# frozen_string_literal: true

class AddCountryToOffers < ActiveRecord::Migration[5.2]
  def change
    add_column :offers, :country, :locale
  end
end
