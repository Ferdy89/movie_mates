# frozen_string_literal: true

class MakeImdbIdNullable < ActiveRecord::Migration[6.0]
  def change
    change_column_null :tmdb_profiles, :imdb_id, true
  end
end
