# frozen_string_literal: true

class AddWatchedFlagToMovies < ActiveRecord::Migration[5.0]
  def change
    add_column :movies, :watched, :boolean, null: false, default: false
  end
end
