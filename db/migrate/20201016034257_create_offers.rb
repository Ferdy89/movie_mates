# frozen_string_literal: true

class CreateOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :offers do |t|
      t.belongs_to :provider, null: false, foreign_key: true, index: true
      t.belongs_to :movie, null: false, foreign_key: true, index: true
      t.string :monetization_type, null: false
      t.string :presentation_type, null: false
      t.monetize :price, null: false
      t.string :standard_web_url

      t.timestamps
    end
  end
end
