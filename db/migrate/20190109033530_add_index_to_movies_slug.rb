# frozen_string_literal: true

class AddIndexToMoviesSlug < ActiveRecord::Migration[5.1]
  def change
    add_index :movies, :slug
  end
end
