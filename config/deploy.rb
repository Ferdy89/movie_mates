# frozen_string_literal: true

require "mina/rails"
require "mina/git"

set :application_name, "movie_mates"

# The hostname to SSH to.
set :domain, ENV.fetch("MINA_DOMAIN")

# Username in the server to SSH to.
set :user, ENV.fetch("MINA_USER")

# SSH port number.
set :port, ENV.fetch("MINA_PORT")

# Path to deploy into.
set :deploy_to, ENV.fetch("MINA_DEPLOYMENT_PATH")

# Git repo to clone from. (needed by mina/git)
set :repository, "https://gitlab.com/ferdynton/movie_mates.git"

# Branch name to deploy. (needed by mina/git)
set :branch, "master"

desc "Deploys the current version to the server."
task :deploy do
  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'
    invoke :'deploy:cleanup'

    on :launch do
      command "sudo systemctl restart puma.service"
      command "sudo systemctl restart sidekiq.service"
    end
  end
end
