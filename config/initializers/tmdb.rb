# frozen_string_literal: true

require "themoviedb"

Tmdb::Api.key(ENV["THE_MOVIE_DATABASE_API_KEY"])
