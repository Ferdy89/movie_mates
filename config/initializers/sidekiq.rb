# frozen_string_literal: true

require "sidekiq/web"
Sidekiq::Web.use(Rack::Auth::Basic) do |_, password|
  # - Use & (do not use &&) so that it doesn't short circuit.
  # Protect against timing attacks:
  # - See https://codahale.com/a-lesson-in-timing-attacks/
  # - See https://thisdata.com/blog/timing-attacks-against-string-comparison/
  # - Use digests to stop length information leaking
  Rack::Utils.secure_compare(
    ::Digest::SHA256.hexdigest(password),
    ::Digest::SHA256.hexdigest(ENV["SIDEKIQ_PASSWORD"])
  )
end
