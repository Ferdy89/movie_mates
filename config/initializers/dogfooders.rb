# frozen_string_literal: true

DOGFOODERS = ENV.fetch("DOGFOODER_EMAILS", "").split(",").to_set
