# frozen_string_literal: true

require "i18n/backend/fallbacks"
I18n::Backend::Simple.include I18n::Backend::Fallbacks

AVAILABLE_COUNTRIES = [:"en-US", :"es-ES"].freeze

# Whitelist locales available for the application
I18n.available_locales = [:en, :es] + AVAILABLE_COUNTRIES
I18n.default_locale = :"en-US"
