# frozen_string_literal: true

require_relative "boot"

require "active_record/railtie"
require "action_controller/railtie"
require "action_view/railtie"
require "action_mailer/railtie"
require "rails/test_unit/railtie"
require "sprockets/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
Dotenv.load(".env.secrets") if defined?(Dotenv) && File.exist?(".env.secrets")

module MovieMatesRails # rubocop:disable Style/ClassAndModuleChildren
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified
    # here.  Application configuration should go into files in
    # config/initializers -- all .rb files in that directory are automatically
    # loaded.

    config.load_defaults 7.1

    config.active_record.schema_format = :sql
    config.action_mailer.smtp_settings = {
      address: ENV["MAIL_ADDRESS"],
      port: ENV["MAIL_PORT"],
      user_name: ENV["MAIL_USER"],
      password: ENV["MAIL_PASSWORD"],
      domain: "moviemates.party",
      authentication: :plain,
    }
  end
end
