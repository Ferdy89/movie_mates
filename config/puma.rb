# frozen_string_literal: true

# Puma can serve each request in a thread from an internal thread pool.
# The `threads` method setting takes two numbers a minimum and maximum.
# Any libraries that use thread pools should be configured to match
# the maximum value specified for Puma. Default is set to 5 threads for minimum
# and maximum, this matches the default thread size of Active Record.
#
threads_count = ENV.fetch("RAILS_MAX_THREADS", 5).to_i
threads threads_count, threads_count

rackup DefaultRackup if defined?(DefaultRackup)

# Specifies the `environment` that Puma will run in.
environment ENV.fetch("RACK_ENV", "development")

# There are 2 different ways of running MovieMates:
# 1. When DEPLOYMENT_APPLICATION_PATH is provided, it'll serve to a socket. This
#    is useful when there's an Nginx instance to listen from there. It also uses
#    this directory to place pid files, state files, and log files.
# 2. Otherwise, it serves to a regular port, either provided by the PORT
#    variable or defaults to port 3000.
if (app_path = ENV["DEPLOYMENT_APPLICATION_PATH"])
  directory "#{app_path}/current"

  # Set up socket location
  bind "unix://#{app_path}/tmp/sockets/puma.sock"

  # Logging
  stdout_redirect "#{app_path}/tmp/log/puma.stdout.log",
                  "#{app_path}/tmp/log/puma.stderr.log", true

  # Set master PID and state locations
  pidfile "#{app_path}/tmp/pids/puma.pid"
  state_path "#{app_path}/tmp/pids/puma.state"
else
  # Specifies where will Puma listen for requests.
  port ENV.fetch("PORT", 3000)
end

# Specifies the number of `workers` to boot in clustered mode.
# Workers are forked webserver processes. If using threads and workers together
# the concurrency of the application would be max `threads` * `workers`.
# Workers do not work on JRuby or Windows (both of which do not support
# processes).

workers ENV.fetch("WEB_CONCURRENCY", 2)

# Use the `preload_app!` method when specifying a `workers` number.
# This directive tells Puma to first boot the application and load code
# before forking the application. This takes advantage of Copy On Write
# process behavior so workers use less memory. If you use this option
# you need to make sure to reconnect any threads in the `on_worker_boot`
# block.

preload_app!

# The code in the `on_worker_boot` will be called if you are using
# clustered mode by specifying a number of `workers`. After each worker
# process is booted this block will be run, if you are using `preload_app!`
# option you will want to use this block to reconnect to any threads
# or connections that may have been created at application boot, Ruby
# cannot share connections between processes.

on_worker_boot do
  ActiveRecord::Base.establish_connection if defined?(ActiveRecord)
end
