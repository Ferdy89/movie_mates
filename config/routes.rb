# frozen_string_literal: true

require "sidekiq/web"
require "sidekiq/cron/web"

Rails.application.routes.draw do
  devise_for :users,
             skip: :registrations,
             controllers: {omniauth_callbacks: "users/omniauth_callbacks"}

  devise_scope :user do
    resource :registration,
             only: [:edit, :update],
             controller: "users/registrations",
             as: :user_registration

    delete "sign_out", to: "devise/sessions#destroy", as: :destroy_user_session
  end

  authenticate :user do
    mount Sidekiq::Web => "/sidekiq"
  end

  resource :my_group, except: [:new, :edit, :create] do
    resources :invitations, only: [:create] do
      collection do
        get :commit
      end
    end
  end
  resources :groups, only: [:index]

  namespace :library do
    resources :nominations, only: [:create], param: :slug
  end

  resources :movies, except: [:new, :edit], param: :slug
  resources :tv_shows, except: [:new, :edit], param: :slug

  resources :votes, only: :update, param: :slug
  resources :comments, only: :create, param: :slug

  resource :pick, only: [:new, :create, :show]

  resource :search, only: :show
  resource :faq, only: :show
  resource :locale, only: :update

  get "/home", to: "home#index", as: :home

  root to: "static#index"
end
