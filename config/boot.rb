# frozen_string_literal: true

ENV["BUNDLE_GEMFILE"] ||= File.expand_path("../Gemfile", __dir__)

require "bundler/setup" # Set up gems listed in the Gemfile.

# Might be able to remove this flag while on Ruby 2.6
# https://gitlab.com/ferdynton/movie_mates/issues/168
require "bootsnap/setup" unless ENV["DISABLE_BOOTSNAP"]
