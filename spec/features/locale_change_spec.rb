require "rails_helper"

RSpec.describe "Changing the locale" do
  scenario "on any page, changing the locale switches the language and stores the preference" do
    table = create_group_table(
      "      | Fer |",
      " Heat | y   | Fer "
    )
    user = table[:users][:fer]

    sign_in(user)
    visit movie_path(table[:movies][:heat])

    expect { click_button "España" }.
      to change { user.reload.preferred_locale }.to("es-ES").
      and change { I18n.locale }.to(:"es-ES")
    expect(current_path).to eql(movie_path(table[:movies][:heat]))
  end
end
