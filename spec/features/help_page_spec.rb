require "rails_helper"

RSpec.feature "FAQ page" do
  let(:user) { FactoryGirl.create(:user, group: FactoryGirl.create(:group)) }

  scenario "a user goes to the FAQ page to understand the MovieMates features" do
    sign_in user
    visit home_path

    within("nav") do
      click_link "Help"
    end

    expect(page).to have_text("Frequently Asked Questions")
  end
end
