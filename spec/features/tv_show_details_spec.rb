require "rails_helper"
require_relative "watchable_table_helpers"

RSpec.feature "TV show details page" do
  include WatchableTableHelpers

  scenario "voting the TV show", js: true do
    table = create_tv_show_group_table(
      "          | Fer |",
      " The Wire | u   | Fer "
    )
    sign_in(table[:users][:fer])
    visit tv_show_path(table[:tv_shows][:the_wire])

    current_user_vote(:no).click
    wait_for_ajax
    visit tv_show_path(table[:tv_shows][:the_wire])

    expect(page).to have_current_user_vote(:no)
  end

  # Required to be in JS mode to exercise the collapsible functionality
  scenario "looking up providers for a TV show", js: true do
    table = create_tv_show_group_table(
      "          | Fer |",
      " The Wire | u   | Fer "
    )
    netflix = FactoryGirl.create(:provider, :netflix, clear_name: "Netflix")
    FactoryGirl.create(
      :offer,
      watchable: table[:tv_shows][:the_wire],
      provider: netflix,
      monetization_type: "flatrate",
      presentation_type: "HD",
      country: I18n.locale
    )
    sign_in(table[:users][:fer])
    visit tv_show_path(table[:tv_shows][:the_wire])

    click_button("Where to watch this TV show?")

    within(page.find("#providers")) do
      rows = page.find_all("tr")
      headers = rows.first.find_all("th").map(&:text)
      subscription_index = headers.index("Subscription")

      offers = rows.drop(1)
      expect(offers.count).to be(1)
      expect(offers.first.find_all("td")[0].text).to eql("Netflix")
      netflix_offer = offers.first.find_all("td")[subscription_index]
      expect(netflix_offer.text).to eql("HD")
    end

    amazon_prime = FactoryGirl.create(:provider, :amazon_prime, clear_name: "Amazon Prime")
    FactoryGirl.create(
      :offer,
      watchable: table[:tv_shows][:the_wire],
      provider: amazon_prime,
      country: "es-ES",
      monetization_type: "flatrate",
      presentation_type: "4K"
    )

    click_link "United States" # Open country picker
    click_button "España"

    click_button("¿Dónde ver esta serie?")

    within(page.find("#providers")) do
      rows = page.find_all("tr")
      headers = rows.first.find_all("th").map(&:text)
      subscription_index = headers.index("Subscripción")

      offers = rows.drop(1)
      expect(offers.count).to be(1)
      expect(offers.first.find_all("td")[0].text).to eql("Amazon Prime")
      amazon_offer = offers.first.find_all("td")[subscription_index]
      expect(amazon_offer.text).to eql("4K")
    end
  end

  scenario "marking the last watched season and episode", js: true do
    table = create_tv_show_group_table(
      "          | Fer |",
      " The Wire | u   | Fer "
    )
    fer = table[:users][:fer]
    the_wire = table[:tv_shows][:the_wire]
    FactoryGirl.create(
      :season,
      tmdb_tv_show_profile: the_wire.tmdb_profile,
      name: "Season 2",
      season_number: 2,
      episode_count: 13
    )
    FactoryGirl.create(
      :season,
      tmdb_tv_show_profile: the_wire.tmdb_profile,
      name: "Season 1",
      season_number: 1,
      episode_count: 10
    )

    sign_in(fer)

    visit tv_show_path(table[:tv_shows][:the_wire])

    season_picker = find_all(".bootstrap-select").find { |picker| picker.find_css("#season-picker").any? }
    episode_picker = find_all(".bootstrap-select").find { |picker| picker.find_css("#episode-picker").any? }
    season_picker.click
    seasons = season_picker.find_css("li > a[role='option']").map(&:visible_text)
    expect(seasons).to eql(["Season 1", "Season 2"])

    season_picker.find_css("li > a[role='option']").find { |option| option.visible_text == "Season 1" }.click

    episode_picker.click
    episodes = episode_picker.find_css("li > a[role='option']").map(&:visible_text)
    expect(episodes.count).to be(10)
    episode_picker.click

    season_picker.click
    season_picker.find_css("li > a[role='option']").find { |option| option.visible_text == "Season 2" }.click

    # Wait until the episode picker is properly populated before opening it,
    # which seems to be an issue on GitLab CI.
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop until episode_picker.find_css("li > a[role='option']").last.all_text == "E13"
    end
    episode_picker.click

    episode_picker.find_css("li > a[role='option']").find { |option| option.visible_text == "E8" }.click

    click_button "Update last season and episode watched"

    season_picker = find_all(".bootstrap-select").find { |picker| picker.find_css("#season-picker").any? }
    episode_picker = find_all(".bootstrap-select").find { |picker| picker.find_css("#episode-picker").any? }

    expect(season_picker.text).to eql("Season 2")
    episode_picker.click
    expect(episode_picker.find_css("li.selected").first.visible_text).to eql("E8")

    nomination = the_wire.nominations.first
    expect(nomination.last_season_watched).to be(2)
    expect(nomination.last_episode_watched).to be(8)
  end

  scenario "deleting a TV show" do
    table = create_tv_show_group_table(
      "          | Fer |",
      " The Wire | u   | Fer "
    )
    sign_in(table[:users][:fer])

    visit tv_show_path(table[:tv_shows][:the_wire])

    expect { click_link "Remove from your library" }.to change { TvShowNomination.count }.by(-1)
  end
end
