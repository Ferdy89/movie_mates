require "rails_helper"
require_relative "watchable_table_helpers"

RSpec.feature "Movie selection" do
  include WatchableTableHelpers

  before { sign_in(table[:users][:fer]) }

  let(:table) do
    create_group_table(
      "           | Fer | Ainho | Fido |",
      " Heat      | y   | a     | a    | Fer ",
      " Snatch    | y   | y     | w    | Fer ",
      " Up        | y   | y     | u    | Fer ",
      " Gladiator | y   | y     | y    | Fer ",
      " Amelie    | y   | w     | n    | Fer ",
      " Alien     | u   | y     | n    | Fer "
    )
  end

  scenario "user selects a few of her friends and the system returns the movies they can watch" do
    visit home_path

    within("#pick-a-movie") do
      check        "Fer"
      check        "Ainho"
      click_button "Pick a movie"
    end

    within("#catalog") do
      all_titles = find_all("[data-watchable-title]").map(&:text)
      expect(all_titles).to match(["Snatch (2000)", "Heat (1995)"])

      within_watchable(title: "Snatch (2000)") do
        expect(page).to have_current_user_vote(:yes)
      end

      within_watchable(title: "Heat (1995)") do
        expect(page).to have_current_user_vote(:yes)
      end
    end
  end

  scenario "user can filter the results by provider", js: true do
    netflix = FactoryGirl.create(:provider, :netflix)
    amazon_prime = FactoryGirl.create(:provider, :amazon_prime)
    FactoryGirl.create(:offer, watchable: table[:movies][:heat], provider: netflix, country: "en-US")
    FactoryGirl.create(:offer, watchable: table[:movies][:snatch], provider: amazon_prime, country: "en-US")

    visit home_path

    find("#pick-a-movie-heading > h4").click
    # TODO(https://gitlab.com/ferdynton/movie_mates/-/issues/220) Investigate why
    # clicking on Fer first causes the test to be flaky (that's why we're
    # clicking on Ainho first here)
    find("label", text: "Ainho").click
    find("label", text: "Fer").click
    find(".bootstrap-select > button").click
    # All providers are checked by default, so I'm de-selecting Amazon Prime, leaving Netflix
    find("li > a[role='option']", text: "Amazon Prime").click
    click_button "Pick a movie"

    find(".bootstrap-select").click
    selected_providers = find(".dropdown-menu.open").find_all("li.selected").map(&:text)
    expect(selected_providers).to eql(["Netflix"])

    within("#catalog") do
      all_titles = find_all("[data-watchable-title]").map(&:text)

      expect(all_titles).to eql(["Heat (1995)"])
    end
  end

  scenario "user does not select any friends and the system shows an error message" do
    visit home_path
    click_button "Pick a movie"

    expect(current_path).to eq(home_path)
    expect(page).to have_content("Please select one or more users in order to pick something to watch")
  end

  scenario "picking a movie from the explicit page for it" do
    visit home_path

    click_link "Pick a Movie"

    check        "Fer"
    check        "Ainho"
    click_button "Pick a movie"

    within("#catalog") do
      all_titles = find_all("[data-watchable-title]").map(&:text)

      expect(all_titles).to_not be_empty
    end
  end
end
