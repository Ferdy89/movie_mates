require "rails_helper"

RSpec.feature "Search and add TV shows" do
  scenario "searching shows results from the local library and from TMDb", vcr: {cassette_name: "search/the_wire"} do
    table = create_tv_show_group_table(
      "          | Fer |",
      " The Wire | y   | Fer "
    )
    sign_in(table[:users][:fer])
    visit home_path

    within("nav") do
      fill_in "Search", with: "the wire"
      click_button "Search"
    end

    click_link "TV shows"

    within("section#library-results") do
      expect(titles_in_local_library).to eql(["The Wire (2002)"])
    end

    within("section#external-results") do
      expect(titles_to_be_added).to include("The War Behind The Wire (2000)")
    end

    click_link "The Wire (2002)"

    expect(page.current_path).to eql(tv_show_path(table[:tv_shows][:the_wire]))
  end

  scenario "external search results can be added to the library a TV show", vcr: {cassette_name: "features/tv_show_lifecycle/add_tv_show"} do
    group = FactoryGirl.create(:group)
    fer = FactoryGirl.create(:user, :fer, group:)
    sign_in(fer)
    visit home_path

    within("section#search") do
      fill_in "Search", with: "the wire"
      click_button "Search"
    end

    click_link "TV shows"

    within("section#external-results") do
      the_original = find_all("[data-watchable-result]").find do |result|
        result.text =~ /The Wire \(2002\)/
      end

      expect { the_original.find("[data-watchable-button]").click }.to change { TvShow.count }.by(1)
    end
    expect(page.text).to include("The Wire was added to your group's library")
  end

  scenario "start off in the search page", vcr: {cassette_name: "search/the_wire"} do
    group = FactoryGirl.create(:group)
    fer = FactoryGirl.create(:user, :fer, group:)
    sign_in(fer)
    visit search_path
    click_link "TV shows"

    within("section#search-params") do
      fill_in "Search query", with: "the wire"

      # Unfortunately, TMDb's API feature to search by year doesn't work
      expect(page.text).to_not include("Release year")

      click_button "Search"
    end

    within("section#external-results") do
      expect(titles_to_be_added).to include("The Wire (2002)")
    end
  end

  private

  def titles_in_local_library
    page.find_all("[data-watchable-title]").map(&:text)
  end

  def titles_to_be_added
    page.find_all("[data-watchable-result] .panel-heading").map(&:text)
  end
end
