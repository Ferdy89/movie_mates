require "rails_helper"

RSpec.feature "User management" do
  let(:fer) { FactoryGirl.create(:user, :fer, group: FactoryGirl.create(:group)) }

  scenario "signing in through Google" do
    visit root_path

    find("[data-google-signin]").click

    expect(page).to have_text("Successfully authenticated from Google account")
  end

  scenario "failure during Google sign in" do
    OmniAuth.config.mock_auth[:google_oauth2] = :invalid_credentials

    visit root_path

    silence_omniauth do # Failed sign in output to stderr
      find("[data-google-signin]").click
    end

    expect(page).to have_text("Could not authenticate you from Google")
  end

  scenario "editing your name" do
    sign_in fer

    visit home_path
    click_link "My profile"

    fill_in "Name", with: "El Venazos"
    click_button "Update"

    expect(page).to have_text("Your account has been updated successfully.")
  end

  private

  def silence_omniauth
    previous_logger = OmniAuth.config.logger
    OmniAuth.config.logger = Logger.new("/dev/null")
    yield
  ensure
    OmniAuth.config.logger = previous_logger
  end
end
