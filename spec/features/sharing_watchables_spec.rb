require "rails_helper"

RSpec.feature "Sharing watchables" do
  scenario "a user from one group shares a link to a movie to a user from another group" do
    group = create_group_table(
      "           | Fer |",
      " Heat      | y   | Fer "
    )
    other_group = FactoryGirl.create(:group)
    other_user = FactoryGirl.create(:user, :ainho, group: other_group)

    sign_in(other_user)

    visit movie_path(group[:movies][:heat])

    expect(page).to have_button("Add to library")
    expect(page).not_to have_link("Remove from your library")

    click_button "Add to library"

    expect(page).to have_text("Heat was added to your group's library")
    expect(page).to have_link("Remove from your library")
    expect(page).not_to have_button("Add to library")
  end

  scenario "a user from one group shares a link to a TV show to a user from another group" do
    group = create_tv_show_group_table(
      "          | Fer |",
      " Sherlock | y   | Fer "
    )
    other_group = FactoryGirl.create(:group)
    other_user = FactoryGirl.create(:user, :ainho, group: other_group)

    sign_in(other_user)

    visit tv_show_path(group[:tv_shows][:sherlock])

    expect(page).to have_button("Add to library")
    expect(page).not_to have_link("Remove from your library")

    click_button "Add to library"

    expect(page).to have_text("Sherlock was added to your group's library")
    expect(page).to have_link("Remove from your library")
    expect(page).not_to have_button("Add to library")
  end
end
