require "rails_helper"
require_relative "watchable_table_helpers"

RSpec.feature "Movie details page" do
  include WatchableTableHelpers

  scenario "voting the movie", js: true do
    table = create_group_table(
      "    | Fer |",
      " Up | u   | Fer "
    )
    sign_in(table[:users][:fer])
    visit movie_path(table[:movies][:up])

    current_user_vote(:no).click
    wait_for_ajax
    visit movie_path(table[:movies][:up])

    expect(page).to have_current_user_vote(:no)
  end

  # Required to be in JS mode to exercise the collapsible functionality
  scenario "looking up providers for a movie", js: true do
    table = create_group_table(
      "    | Fer |",
      " Up | u   | Fer "
    )
    netflix = FactoryGirl.create(:provider, :netflix, clear_name: "Netflix")
    FactoryGirl.create(
      :offer,
      watchable: table[:movies][:up],
      provider: netflix,
      monetization_type: "flatrate",
      presentation_type: "HD",
      country: I18n.locale
    )
    sign_in(table[:users][:fer])
    visit movie_path(table[:movies][:up])

    click_button("Where to watch this movie?")

    within(page.find("#providers")) do
      rows = page.find_all("tr")
      headers = rows.first.find_all("th").map(&:text)
      subscription_index = headers.index("Subscription")

      offers = rows.drop(1)
      expect(offers.count).to be(1)
      expect(offers.first.find_all("td")[0].text).to eql("Netflix")
      netflix_offer = offers.first.find_all("td")[subscription_index]
      expect(netflix_offer.text).to eql("HD")
    end

    amazon_prime = FactoryGirl.create(:provider, :amazon_prime, clear_name: "Amazon Prime")
    FactoryGirl.create(
      :offer,
      watchable: table[:movies][:up],
      provider: amazon_prime,
      country: "es-ES",
      monetization_type: "flatrate",
      presentation_type: "4K"
    )

    click_link "United States" # Open country picker
    click_button "España"

    click_button("¿Dónde ver esta película?")

    within(page.find("#providers")) do
      rows = page.find_all("tr")
      headers = rows.first.find_all("th").map(&:text)
      subscription_index = headers.index("Subscripción")

      offers = rows.drop(1)
      expect(offers.count).to be(1)
      expect(offers.first.find_all("td")[0].text).to eql("Amazon Prime")
      amazon_offer = offers.first.find_all("td")[subscription_index]
      expect(amazon_offer.text).to eql("4K")
    end
  end

  scenario "marking movies as watched" do
    table = create_group_table(
      "    | Fer | Fido |",
      " Up | u   | y    | Fer "
    )
    fer = table[:users][:fer]
    fido = table[:users][:fido]
    sign_in(fer)

    visit movie_path(table[:movies][:up])

    expect(watched_checkbox_for(fer)).to_not be_checked
    expect(watched_checkbox_for(fido)).to_not be_checked

    check "Fer watched it!"
    check "Fido watched it!"
    click_button "Mark watched"

    expect(watched_checkbox_for(fer)).to be_checked
    expect(watched_checkbox_for(fido)).to be_checked
  end

  scenario "deleting a movie" do
    table = create_group_table(
      "    | Fer |",
      " Up | u   | Fer "
    )
    sign_in(table[:users][:fer])

    visit movie_path(table[:movies][:up])

    expect { click_link "Remove from your library" }.to change { MovieNomination.count }.by(-1)
  end
end
