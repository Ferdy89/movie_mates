require "rails_helper"
require_relative "watchable_table_helpers"

RSpec.feature "Home page" do
  include WatchableTableHelpers

  scenario "home page displays unvoted movies" do
    table = create_group_table(
      "    | Fer |",
      " Up | u   | Fer "
    )
    sign_in(table[:users][:fer])

    visit home_path

    unvoted_titles = find_all("[data-watchable-title]").map(&:text)
    expect(unvoted_titles).to include("Up (2009)")
  end

  scenario "home page displays unvoted TV shows" do
    table = create_tv_show_group_table(
      "          | Fer |",
      " The Wire | u   | Fer "
    )
    sign_in(table[:users][:fer])

    visit home_path

    unvoted_titles = find_all("[data-watchable-title]").map(&:text)
    expect(unvoted_titles).to include("The Wire (2002)")
  end

  scenario "voting movies", js: true do
    table = create_group_table(
      "    | Fer |",
      " Up | u   | Fer "
    )
    sign_in(table[:users][:fer])

    visit home_path

    within_watchable(title: "Up (2009)") do
      current_user_vote(:no).click
      wait_for_ajax
    end

    visit movies_path

    within_watchable(title: "Up (2009)") do
      expect(page).to have_current_user_vote(:no)
    end
  end

  scenario "voting TV shows", js: true do
    table = create_tv_show_group_table(
      "          | Fer |",
      " The Wire | u   | Fer "
    )
    sign_in(table[:users][:fer])

    visit home_path

    within_watchable(title: "The Wire (2002)") do
      current_user_vote(:yes).click
      wait_for_ajax
    end

    visit tv_shows_path

    within_watchable(title: "The Wire (2002)") do
      expect(page).to have_current_user_vote(:yes)
    end
  end
end
