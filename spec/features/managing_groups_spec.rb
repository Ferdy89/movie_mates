require "rails_helper"

RSpec.describe "Managing groups" do
  let(:group) { FactoryGirl.create(:group, name: "My Group") }
  let(:fer)   { FactoryGirl.create(:user, :fer,  group:) }
  let(:fido)  { FactoryGirl.create(:user, :fido, group:) }

  scenario "a user edits the name of its group" do
    sign_in(fer)

    visit groups_path

    within_table("Groups") do
      click_link "My Group"
    end

    within("form#group") do
      expect(find_field("Group name").value).to eql("My Group")

      fill_in "Group name", with: "New Name"

      click_button "Update Group"
    end

    within("form#group") do
      expect(find_field("Group name").value).to eql("New Name")
    end
  end

  scenario "inviting a new MovieMates user to the group" do
    sign_in(fer)

    visit my_group_path

    within("form#invitation") do
      fill_in "Invitation emails", with: "friend@example.com, person@example.com"

      click_button "Send invitations"
    end

    expect(notice).to match("Invitations were sent to friend@example.com, person@example.com")

    expect(ActionMailer::Base.deliveries.count).to be(2)

    # New user takes over, starting with accepting the email invitation
    sign_out(fer)

    first_email = ActionMailer::Base.deliveries.first

    expect(first_email.body).to match(/Fer has invited you to their group on MovieMates/)

    accept_invitation_url = first_email.body.match(/localhost:3000([\/?=\-\w]+)"/)[1]
    page.driver.get(accept_invitation_url)

    expect { sign_in(fido) }.to change { group.users.count }.by(1)
  end

  scenario "being along on a group, a user can implode the group" do
    sign_in(fer)

    visit my_group_path

    click_button "Implode group and delete MovieMates account"

    expect(page.current_path).to eql(root_path)
    expect(User.find_by(id: fer.id)).to be_nil
    expect(Group.find_by(id: group.id)).to be_nil
  end

  scenario "a group becomes independent into a new group" do
    fido # Another MovieMate in the group
    sign_in(fer)

    visit my_group_path

    click_button "Abandon group and become my own one"

    expect(page.current_path).to eql(my_group_path)
    expect(notice).to match("You are now you own group!")
    expect(fer.reload.group).to_not eql(group)
    expect(group.users).to match_array([fido])
  end

  private

  def notice
    find(".alert").text
  end
end
