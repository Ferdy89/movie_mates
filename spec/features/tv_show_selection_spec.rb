require "rails_helper"
require_relative "watchable_table_helpers"

RSpec.feature "TV show selection" do
  include WatchableTableHelpers

  before { sign_in(table[:users][:fer]) }

  let(:table) do
    table = create_tv_show_group_table(
      "          | Fer | Ainho | Fido |",
      " The Wire | y   | a     | a    | Fer ",
      " Sherlock | y   | y     | n    | Fer ",
      " Lost     | y   | y     | u    | Fer ",
      " Seinfeld | y   | y     | y    | Fer ",
      " Friends  | y   | n     | n    | Fer ",
      " Homeland | u   | y     | n    | Fer ",
      " Dark     | y   | y     | n    | Fer " # This one is fully watched, though
    )
    dark = table[:tv_shows][:dark]
    FactoryGirl.create(
      :season,
      tmdb_tv_show_profile: dark.tmdb_profile,
      season_number: 1,
      episode_count: 10
    )
    dark_nomination = dark.nominations.first
    dark_nomination.update!(
      last_season_watched: 1,
      last_episode_watched: 10
    )
    table
  end

  scenario "user selects a few of her friends and the system returns the TV shows they can watch" do
    visit home_path

    within("#pick-a-tv-show") do
      check        "Fer"
      check        "Ainho"
      click_button "Pick a TV show"
    end

    within("#catalog") do
      all_titles = find_all("[data-watchable-title]").map(&:text)
      expect(all_titles).to match(["Sherlock (2010)", "The Wire (2002)"])

      within_watchable(title: "Sherlock (2010)") do
        expect(page).to have_current_user_vote(:yes)
      end

      within_watchable(title: "The Wire (2002)") do
        expect(page).to have_current_user_vote(:yes)
      end
    end
  end

  scenario "user can filter the results by provider", js: true do
    netflix = FactoryGirl.create(:provider, :netflix)
    amazon_prime = FactoryGirl.create(:provider, :amazon_prime)
    FactoryGirl.create(:offer, watchable: table[:tv_shows][:the_wire], provider: netflix, country: "en-US")
    FactoryGirl.create(:offer, watchable: table[:tv_shows][:sherlock], provider: amazon_prime, country: "en-US")

    visit home_path

    find("#pick-a-tv-show-heading > h4").click
    # TODO(https://gitlab.com/ferdynton/movie_mates/-/issues/220) Investigate why
    # clicking on Fer first causes the test to be flaky (that's why we're
    # clicking on Ainho first here)
    find("label", text: "Ainho").click
    find("label", text: "Fer").click
    find(".bootstrap-select > button").click
    # All providers are checked by default, so I'm de-selecting Amazon Prime, leaving Netflix
    find("li > a[role='option']", text: "Amazon Prime").click
    click_button "Pick a TV show"

    find(".bootstrap-select").click
    selected_providers = find(".dropdown-menu.open").find_all("li.selected").map(&:text)
    expect(selected_providers).to eql(["Netflix"])

    within("#catalog") do
      all_titles = find_all("[data-watchable-title]").map(&:text)

      expect(all_titles).to eql(["The Wire (2002)"])
    end
  end

  scenario "user does not select any friends and the system shows an error message" do
    visit home_path
    click_button "Pick a TV show"

    expect(current_path).to eq(home_path)
    expect(page).to have_content("Please select one or more users in order to pick something to watch")
  end
end
