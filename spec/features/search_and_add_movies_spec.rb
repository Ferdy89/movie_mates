require "rails_helper"

RSpec.feature "Search and add movies" do
  scenario "searching shows results from the local library and from TMDb", vcr: {cassette_name: "search/snatch"} do
    table = create_group_table(
      "        | Fer |",
      " Snatch | y   | Fer "
    )
    sign_in(table[:users][:fer])
    visit home_path

    within("nav") do
      fill_in "Search", with: "snatch"
      click_button "Search"
    end

    expect(page).to have_field("Search query", with: "snatch")

    within("section#library-results") do
      expect(titles_in_local_library).to eql(["Snatch (2000)"])
    end

    within("section#external-results") do
      expect(titles_to_be_added).to include("Snatched (2017)")
    end
  end

  scenario "external search results can be added to the library a movie", vcr: {cassette_name: "features/movie_lifecycle/add_movie"} do
    group = FactoryGirl.create(:group)
    fer = FactoryGirl.create(:user, :fer, group:)
    sign_in(fer)
    visit home_path

    within("section#search") do
      fill_in "Search", with: "die hard"
      click_button "Search"
    end

    within("section#external-results") do
      the_original = find_all("[data-watchable-result]").find do |result|
        result.text =~ /Die Hard \(1988\)/
      end

      expect { the_original.find("[data-watchable-button]").click }.to change { Movie.count }.by(1)
    end
    expect(page.text).to include("Die Hard was added to your group's library")
  end

  scenario "start off in the search page and search by year and locale", vcr: {cassette_name: "search/house"} do
    group = FactoryGirl.create(:group)
    fer = FactoryGirl.create(:user, :fer, group:)
    sign_in(fer)
    visit search_path

    within("section#search-params") do
      fill_in "Search query", with: "house"
      fill_in "Release year", with: 1977
      click_button "Search"
    end

    within("section#external-results") do
      expect(titles_to_be_added).to include("ハウス (1977)")
    end
  end

  private

  def titles_in_local_library
    page.find_all("[data-watchable-title]").map(&:text)
  end

  def titles_to_be_added
    page.find_all("[data-watchable-result] .panel-heading").map(&:text)
  end
end
