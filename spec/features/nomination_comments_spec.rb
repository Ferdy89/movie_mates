require "rails_helper"

RSpec.feature "Commenting on nominations" do
  scenario "a user can comment on a nomination for its group to see" do
    group = create_group_table(
      "           | Fer | Diego |",
      " Heat      | y   | a     | Fer "
    )
    another_group = create_group_table(
      "           | Ainho |",
      " Heat      | n     | Ainho "
    )
    heat = group[:movies][:heat]
    fer = group[:users][:fer]
    diego = group[:users][:diego]
    ainho = another_group[:users][:ainho]

    sign_in(fer)

    visit(movie_path(heat))

    fill_in "comment", with: "This is my first comment!"
    click_button "Add comment"

    comment = all_comments.first
    expect(comment.body).to eql("This is my first comment!")
    expect(comment.user).to eql("Fer")
    expect(comment.timestamp).to be_within(10.seconds).of(Time.current)

    sign_in(diego)

    fill_in "comment", with: "This is another user's comment!"
    click_button "Add comment"

    comment = all_comments.last
    expect(comment.body).to eql("This is another user's comment!")
    expect(comment.user).to eql("Diego")

    sign_in(ainho)

    visit current_path
    expect(all_comments).to be_empty
  end

  scenario "a user can comment on TV shows too" do
    group = create_tv_show_group_table(
      "          | Fer |",
      " The Wire | y   | Fer "
    )
    the_wire = group[:tv_shows][:the_wire]
    fer = group[:users][:fer]
    nomination = the_wire.nominations.first

    sign_in(fer)

    visit(tv_show_path(slug: the_wire))

    fill_in "comment", with: "This is my first comment!"
    click_button "Add comment"

    comment = all_comments.first
    expect(comment.body).to eql("This is my first comment!")
    expect(comment.user).to eql("Fer")
    expect(comment.timestamp).to be_within(10.seconds).of(Time.current)
    expect(nomination.comments.count).to be(1)
  end

  private

  let(:comment_klass) do
    Class.new do
      attr_reader :element

      def initialize(element)
        @element = element
      end

      def body
        element.find(".body").text
      end

      def user
        element.find(".author").text
      end

      def timestamp
        Time.zone.parse(element.find(".timestamp")["title"])
      end
    end
  end

  def all_comments
    find_all(".comment").map do |element|
      comment_klass.new(element)
    end
  end
end
