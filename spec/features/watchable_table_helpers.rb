module WatchableTableHelpers
  def have_current_user_vote(vote)
    have_css("[data-current-user-opinion=#{vote}][checked]")
  end

  def current_user_vote(vote)
    find("[data-current-user-opinion=#{vote}] ~ span")
  end

  def be_yes
    have_css("[data-opinion=yes]")
  end

  def be_no
    have_css("[data-opinion=no]")
  end

  def be_abstention
    have_css("[data-opinion=abstention]")
  end

  def be_unknown
    have_css("span.unknown")
  end

  def watched_checkbox_for(user)
    page.find("#watched_#{user.id}")
  end

  def within_watchable(title:, &block)
    watchable = find_all(".panel").find do |panel|
      panel.find("[data-watchable-title]").text == title if panel.has_css?("[data-watchable-title]")
    end

    within(watchable, &block)
  end
end
