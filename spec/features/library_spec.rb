require "rails_helper"

RSpec.feature "Library" do
  scenario "by default, the library shows the group's movies in alphabetical order" do
    table = create_group_table(
      "        | Fer |",
      " Up     | y   | Fer ",
      " Snatch | y   | Fer ",
      " Amelie | y   | Fer "
    )
    create_group_table(
      "           | Diego |",
      " Gladiator | y     | Diego "
    )
    sign_in(table[:users][:fer])
    visit home_path

    click_link "Movie Library"

    expect(all_titles).to eql(["Amélie (2001)", "Snatch (2000)", "Up (2009)"])
  end

  scenario "TV shows also have their own library" do
    table = create_tv_show_group_table(
      "          | Fer |",
      " The Wire | y   | Fer ",
      " Sherlock | y   | Fer "
    )
    create_tv_show_group_table(
      "      | Diego |",
      " Lost | y     | Diego "
    )
    sign_in(table[:users][:fer])
    visit home_path

    click_link "TV Show Library"

    expect(all_titles).to eql(["Sherlock (2010)", "The Wire (2002)"])
  end

  scenario "movies can be sorted by rating and length too" do
    table = create_group_table(
      "        | Fer |",
      " Up     | y   | Fer ",
      " Snatch | y   | Fer ",
      " Amelie | y   | Fer "
    )
    table[:movies][:up].tmdb_profile.update!(rating: 7.8, minutes: 96)
    table[:movies][:snatch].tmdb_profile.update!(rating: 7.7, minutes: 103)
    table[:movies][:amelie].tmdb_profile.update!(rating: 7.9, minutes: 122)
    sign_in(table[:users][:fer])
    visit movies_path

    within("section#library") do
      click_link "Sort by rating (High > Low)"
    end

    expect(all_titles).to eql(["Amélie (2001)", "Up (2009)", "Snatch (2000)"])

    within("section#library") do
      click_link "Sort by duration (Low > High)"
    end

    expect(all_titles).to eql(["Up (2009)", "Snatch (2000)", "Amélie (2001)"])
  end

  scenario "movies are paginated to have great performance in both the server and the client" do
    group = FactoryGirl.create(:group)
    user = FactoryGirl.create(:user, group:)
    24.times do |index|
      movie = FactoryGirl.create(:movie)
      FactoryGirl.create(:tmdb_profile, movie:, title: "Movie #{index.to_s.rjust(2, "0")}", release_date: Date.new(2018))
      FactoryGirl.create(:movie_nomination, movie:, contributor: user, group:)
    end
    sign_in(user)
    visit movies_path

    # Applying a non-default sorting to ensure the sorting param is sticky across pages
    within("section#library") do
      click_link "Sort alphabetically (Z > A)"
    end

    within("section#library") do
      expect(all_titles).to_not include("Movie 11 (2018)")
      expect(all_titles).to include("Movie 12 (2018)")
      expect(all_titles).to include("Movie 23 (2018)")

      click_link "Next page"
    end

    within("section#library") do
      expect(all_titles).to include("Movie 00 (2018)")
      expect(all_titles).to include("Movie 11 (2018)")
      expect(all_titles).to_not include("Movie 12 (2018)")
    end
  end

  private

  def all_titles
    find_all("[data-watchable-title]").map(&:text)
  end
end
