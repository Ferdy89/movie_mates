require "rails_helper"

RSpec.describe MovieMates::Library do
  describe "#as_json" do
    it "retrieves the library from a user's group ordered alphabetically" do
      table = create_group_table(
        "      | Fer |",
        " Up   | y   | Fer",
        " Heat | y   | Fer"
      )

      subject = described_class.new(user: table[:users][:fer], sort_by: "alphabetically_high")
      titles = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(titles).to eql(["Heat", "Up"])

      subject = described_class.new(user: table[:users][:fer], sort_by: "alphabetically_low")
      titles = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(titles).to eql(["Up", "Heat"])
    end

    it "retrieves the library from a user's group ordered by rating" do
      table = create_group_table(
        "        | Fer |",
        " Up     | y   | Fer",
        " Heat   | y   | Fer",
        " Amelie | y   | Fer"
      )
      table[:movies][:up].tmdb_profile.update!(rating: 7.9)
      table[:movies][:heat].tmdb_profile.update!(rating: 7.8)
      table[:movies][:amelie].tmdb_profile.update!(rating: 7.8)

      subject = described_class.new(user: table[:users][:fer], sort_by: "rating_high")
      titles = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(titles).to eql(["Up", "Amélie", "Heat"])

      subject = described_class.new(user: table[:users][:fer], sort_by: "rating_low")
      titles = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(titles).to eql(["Amélie", "Heat", "Up"])
    end

    it "retrieves the library from a user's group ordered by duration" do
      table = create_group_table(
        "      | Fer |",
        " Up   | y   | Fer",
        " Heat | y   | Fer"
      )
      table[:movies][:up].tmdb_profile.update!(minutes: 96)
      table[:movies][:heat].tmdb_profile.update!(minutes: 170)

      subject = described_class.new(user: table[:users][:fer], sort_by: "duration_high")
      titles = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(titles).to eql(["Heat", "Up"])

      subject = described_class.new(user: table[:users][:fer], sort_by: "duration_low")
      titles = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(titles).to eql(["Up", "Heat"])
    end

    it "upon an unknown sort direction, it defaults to the classic alphabetical order" do
      table = create_group_table(
        "      | Fer |",
        " Up   | y   | Fer",
        " Heat | y   | Fer"
      )

      subject = described_class.new(user: table[:users][:fer], sort_by: "unknown_order")
      titles = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(titles).to eql(["Heat", "Up"])

      subject = described_class.new(user: table[:users][:fer])
      titles = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(titles).to eql(["Heat", "Up"])
    end

    it "limits the results returned to 12 for each page" do
      group = FactoryGirl.create(:group)
      user = FactoryGirl.create(:user, group:)
      20.times do |index|
        movie = FactoryGirl.create(:movie)
        FactoryGirl.create(:tmdb_profile, movie:, title: "Movie #{index.to_s.rjust(2, "0")}")
        FactoryGirl.create(:movie_nomination, movie:, contributor: user, group:)
      end

      result = described_class.new(user:).as_json
      titles = result.fetch(:watchables).map { |movie| movie[:title] }

      expect(result.fetch(:total_pages)).to be(2)
      expect(result.fetch(:current_page)).to be(1)
      expect(titles.count).to be(12)
      expect(titles).to include("Movie 00")
      expect(titles).to include("Movie 11")
      expect(titles).to_not include("Movie 12")

      result = described_class.new(user:, page: 2).as_json
      titles = result.fetch(:watchables).map { |movie| movie[:title] }

      expect(result.fetch(:total_pages)).to be(2)
      expect(result.fetch(:current_page)).to be(2)
      expect(titles.count).to be(8)
      expect(titles).to_not include("Movie 11")
      expect(titles).to include("Movie 12")
      expect(titles).to include("Movie 19")
    end

    it "sanitizes any weird input that might be passed to the page argument" do
      table = create_group_table(
        "      | Fer |",
        " Up   | y   | Fer"
      )

      subject = described_class.new(user: table[:users][:fer], page: "no_page")

      expect(subject.as_json.fetch(:current_page)).to be(1)

      subject = described_class.new(user: table[:users][:fer], page: -1)

      expect(subject.as_json.fetch(:current_page)).to be(1)

      subject = described_class.new(user: table[:users][:fer], page: 1000)

      expect(subject.as_json.fetch(:current_page)).to be(1)
    end
  end
end
