require "rails_helper"

RSpec.describe MovieMates::TvShowLibrary do
  describe "#as_json" do
    it "retrieves the library from a user's group ordered alphabetically" do
      table = create_tv_show_group_table(
        "          | Fer |",
        " The Wire | y   | Fer",
        " Sherlock | y   | Fer"
      )

      subject = described_class.new(user: table[:users][:fer], sort_by: "alphabetically_high")
      titles = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(titles).to eql(["Sherlock", "The Wire"])

      subject = described_class.new(user: table[:users][:fer], sort_by: "alphabetically_low")
      titles = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(titles).to eql(["The Wire", "Sherlock"])
    end

    it "retrieves the library from a user's group ordered by rating" do
      table = create_tv_show_group_table(
        "          | Fer |",
        " The Wire | y   | Fer",
        " Sherlock | y   | Fer",
        " Lost     | y   | Fer"
      )
      table[:tv_shows][:the_wire].tmdb_profile.update!(rating: 7.9)
      table[:tv_shows][:sherlock].tmdb_profile.update!(rating: 7.8)
      table[:tv_shows][:lost].tmdb_profile.update!(rating: 7.8)

      subject = described_class.new(user: table[:users][:fer], sort_by: "rating_high")
      titles = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(titles).to eql(["The Wire", "Lost", "Sherlock"])

      subject = described_class.new(user: table[:users][:fer], sort_by: "rating_low")
      titles = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(titles).to eql(["Lost", "Sherlock", "The Wire"])
    end

    it "retrieves the library from a user's group ordered by duration" do
      table = create_tv_show_group_table(
        "          | Fer |",
        " The Wire | y   | Fer",
        " Sherlock | y   | Fer"
      )
      table[:tv_shows][:the_wire].tmdb_profile.update!(minutes: 96)
      table[:tv_shows][:sherlock].tmdb_profile.update!(minutes: 170)

      subject = described_class.new(user: table[:users][:fer], sort_by: "duration_high")
      titles = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(titles).to eql(["Sherlock", "The Wire"])

      subject = described_class.new(user: table[:users][:fer], sort_by: "duration_low")
      titles = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(titles).to eql(["The Wire", "Sherlock"])
    end

    it "upon an unknown sort direction, it defaults to the classic alphabetical order" do
      table = create_tv_show_group_table(
        "          | Fer |",
        " The Wire | y   | Fer",
        " Sherlock | y   | Fer"
      )

      subject = described_class.new(user: table[:users][:fer], sort_by: "unknown_order")
      titles = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(titles).to eql(["Sherlock", "The Wire"])

      subject = described_class.new(user: table[:users][:fer])
      titles = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(titles).to eql(["Sherlock", "The Wire"])
    end

    it "limits the results returned to 12 for each page" do
      group = FactoryGirl.create(:group)
      user = FactoryGirl.create(:user, group:)
      20.times do |index|
        tv_show = FactoryGirl.create(:tv_show)
        FactoryGirl.create(:tmdb_tv_show_profile, tv_show:, title: "TV show #{index.to_s.rjust(2, "0")}")
        FactoryGirl.create(:tv_show_nomination, tv_show:, contributor: user, group:)
      end

      result = described_class.new(user:).as_json
      titles = result.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(result.fetch(:total_pages)).to be(2)
      expect(result.fetch(:current_page)).to be(1)
      expect(titles.count).to be(12)
      expect(titles).to include("TV show 00")
      expect(titles).to include("TV show 11")
      expect(titles).to_not include("TV show 12")

      result = described_class.new(user:, page: 2).as_json
      titles = result.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(result.fetch(:total_pages)).to be(2)
      expect(result.fetch(:current_page)).to be(2)
      expect(titles.count).to be(8)
      expect(titles).to_not include("TV show 11")
      expect(titles).to include("TV show 12")
      expect(titles).to include("TV show 19")
    end

    it "sanitizes any weird input that might be passed to the page argument" do
      table = create_tv_show_group_table(
        "          | Fer |",
        " The Wire | y   | Fer"
      )

      subject = described_class.new(user: table[:users][:fer], page: "no_page")

      expect(subject.as_json.fetch(:current_page)).to be(1)

      subject = described_class.new(user: table[:users][:fer], page: -1)

      expect(subject.as_json.fetch(:current_page)).to be(1)

      subject = described_class.new(user: table[:users][:fer], page: 1000)

      expect(subject.as_json.fetch(:current_page)).to be(1)
    end
  end
end
