require "rails_helper"

RSpec.describe MovieMates::Import::TmdbTvShowWrapper do
  describe ".by_id", vcr: {cassette_name: "import/tv_show/the_wire"} do
    it "wraps a TMDb TV show from its ID" do
      subject = described_class.by_id(1438)

      expect(subject.tmdb_id).to be(1438)
      expect(subject.title).to eql("The Wire")
      expect(subject.plot).to eql(
        "Told from the points of view of both the Baltimore homicide and "\
        "narcotics detectives and their targets, the series captures a "\
        "universe in which the national war on drugs has become a permanent, "\
        "self-sustaining bureaucracy, and distinctions between good and evil "\
        "are routinely obliterated."
      )
      expect(subject.release_date).to eql(Date.new(2002, 6, 2))
      expect(subject.poster_url).to eql("https://image.tmdb.org/t/p/w500/4lbclFySvugI51fwsyxBTOm4DqK.jpg")
      expect(subject.rating).to eql(8.4)
      expect(subject.minutes).to eql(60)
      expect(subject.genres).to match_array(["Drama", "Crime"])
    end

    it "raises a specific error when the TV show couldn't be found", vcr: {cassette_name: "import/tv_show/404"} do
      expect { described_class.by_id(-1) }.
        to raise_error(described_class::TvShowNotFound)
    end
  end

  describe "#hash_with" do
    it "creates a hash with the attributes passed", vcr: {cassette_name: "import/tv_show/the_wire"} do
      subject = described_class.by_id(1438)

      expect(subject.hash_with(:tmdb_id, :title, :rating, :minutes)).to eql(
        tmdb_id: 1438,
        title: "The Wire",
        rating: 8.4,
        minutes: 60
      )
    end
  end

  describe "#release_date" do
    it "does not contain a release_date when the movie does not have it", vcr: {cassette_name: "import/tv_show/matrix"} do
      # Weird Italian news show
      subject = described_class.by_id(31_636)

      expect(subject.release_date).to be(nil)
    end
  end

  describe "#poster_url" do
    it "retrieves the URL for the poster of a TV show", vcr: {cassette_name: "import/tv_show/the_wire"} do
      subject = described_class.by_id(1438)

      expect(subject.poster_url).to eql("https://image.tmdb.org/t/p/w500/4lbclFySvugI51fwsyxBTOm4DqK.jpg")
    end
  end

  describe "#url" do
    it "builds the URL of the TV show in TMDb", vcr: {cassette_name: "import/tv_show/the_wire"} do
      subject = described_class.by_id(1438)

      expect(subject.url).to eql("https://www.themoviedb.org/tv/1438")
    end
  end

  describe "#seasons" do
    it "creates a nice wrapper around seasons too", vcr: {cassette_name: "import/tv_show/the_wire"} do
      subject = described_class.by_id(1438)

      expect(subject.seasons.count).to be(6)
      expect(subject.seasons[1].name).to eql("Season 1")
      expect(subject.seasons[1].episode_count).to be(13)
      expect(subject.seasons[1].season_number).to be(1)
      expect(subject.seasons[1].air_date).to eql(Date.new(2002, 6, 2))
      expect(subject.seasons[1].poster_url).to eql("https://image.tmdb.org/t/p/w500/blgnd2APWtxsxGyctmZyaMPO4Ym.jpg")
    end

    it "doesn't parse an air date if there isn't one", vcr: {cassette_name: "import/tv_show/no_air_date"} do
      subject = described_class.by_id(21_421) # The Staircase

      expect(subject.seasons.first.air_date).to be_nil
    end
  end
end
