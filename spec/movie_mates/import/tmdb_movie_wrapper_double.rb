# Quacks like a TmdbMovieWrapper with a simple constructor and default values
class TmdbMovieWrapperDouble
  def initialize(attrs = {})
    attrs.reverse_merge(defaults).each do |name, value|
      if MovieMates::Import::TmdbMovieWrapper.instance_methods.include?(name)
        define_singleton_method(name) { value }
      else
        raise "TmdbMovieWrapper doesn't implement #{name}"
      end
    end
  end

  def hash_with(*fields)
    fields.index_with { |field| public_send(field) }
  end

  private

  def defaults
    {
      tmdb_id: Time.now.to_i,
      imdb_id: Time.now.to_i.to_s,
      title: SecureRandom.uuid,
      poster_url: SecureRandom.uuid,
      release_date: Date.current,
      plot: SecureRandom.uuid,
      rating: 5.0,
      minutes: 90,
      genres: ["Action"],
      trailer_youtube_id: nil,
    }
  end
end
