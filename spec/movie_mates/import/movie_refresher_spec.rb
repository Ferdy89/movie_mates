require "rails_helper"
require_relative "tmdb_movie_wrapper_double"

RSpec.describe MovieMates::Import::MovieRefresher do
  describe "#refresh" do
    it "updates a movie with the latest data from TMDb" do
      movie = FactoryGirl.create(:movie)
      tmdb_profile = FactoryGirl.create(:tmdb_profile, movie:, tmdb_id: 123)
      allow(MovieMates::Import::TmdbMovieWrapper).to receive(:by_id) do
        TmdbMovieWrapperDouble.new(tmdb_id: 123, title: "Die Hard", release_date: Date.new(1988))
      end

      subject = described_class.new(movie:)

      expect { subject.refresh }.to change { tmdb_profile.reload.title }.to("Die Hard")
    end
  end
end
