# Quacks like a TmdbTvShowWrapper with a simple constructor and default values
class TmdbTvShowWrapperDouble
  def initialize(attrs = {})
    attrs.reverse_merge(defaults).each do |name, value|
      if MovieMates::Import::TmdbTvShowWrapper.instance_methods.include?(name)
        define_singleton_method(name) { value }
      else
        raise "TmdbTvShowWrapper doesn't implement #{name}"
      end
    end
  end

  def hash_with(*fields)
    fields.index_with { |field| public_send(field) }
  end

  private

  def defaults
    {
      tmdb_id: Time.now.to_i,
      title: SecureRandom.uuid,
      poster_url: SecureRandom.uuid,
      release_date: Date.current,
      plot: SecureRandom.uuid,
      rating: 5.0,
      minutes: 90,
      genres: ["Action"],
      in_production: false,
      next_episode_to_air: nil,
      seasons: [],
    }
  end
end

class SeasonDouble
  def initialize(attrs = {})
    attrs.reverse_merge(defaults).each do |name, value|
      if MovieMates::Import::TmdbTvShowWrapper::Season.
          instance_methods.include?(name)
        define_singleton_method(name) { value }
      else
        raise "TmdbTvShowWrapper::Season doesn't implement #{name}"
      end
    end
  end

  private

  def defaults
    {
      name: "A Season",
      air_date: Time.zone.today,
      episode_count: 10,
      season_number: 1,
      poster_url: "http://themoviedb.org/poster.png",
    }
  end
end
