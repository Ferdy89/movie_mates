require "rails_helper"

RSpec.describe MovieMates::Import::OffersRefresher do
  describe "#refresh for movie" do
    it "fetches all the offers and providers for a movie", vcr: {cassette_name: "import/offers/fetch_offers"} do
      movie = FactoryGirl.create(:movie, :up)

      subject = described_class.for(watchable: movie, country: "en-US")

      expect { subject.refresh }.
        to change { movie.offers.reload.size }.from(0).
        and change { Provider.count }.from(0)
      disney_plus = movie.offers.joins(:provider).
        find_by(providers: {clear_name: "Disney Plus"}, monetization_type: "flatrate")
      expect(disney_plus.price).to eql(Money.new(0, "USD"))
    end

    it "logs when it can't find offers for a movie", vcr: {cassette_name: "import/offers/no_offers"} do
      pending "Finding another movie that isn't in Just Watch"
      # https://moviemates.party/movies/voldemort:-origins-of-the-heir-2018

      movie = FactoryGirl.create(:movie)
      FactoryGirl.create(:tmdb_profile, movie:, title: "Down with Cats", tmdb_id: 236_698)
      allow(Rails.logger).to receive(:info)

      described_class.for(watchable: movie, country: "en-US").refresh

      expect(Rails.logger).to have_received(:info)
    end

    it "refreshes all the offers for a movie, preserving existing providers", vcr: {cassette_name: "import/offers/refresh_offers"} do
      movie = FactoryGirl.create(:movie, :up)
      # This is the real ID from a real offer in the cassette. It's important to test the code entirely
      microsoft = Provider.create!(clear_name: "Microsoft Store", provider_id: 68)
      old_offer = FactoryGirl.create(:offer, provider: microsoft, watchable: movie, country: "en-US")

      described_class.for(watchable: movie, country: "en-US").refresh

      expect(Offer.find_by(id: old_offer.id)).to be_nil
      expect(movie.offers.reload.size).to be > 0
      expect(microsoft.reload).to_not be(nil)
    end

    it "doesn't import offers for unknown providers", vcr: {cassette_name: "import/offers/no_provider"} do
      pending "Finding another movie without a provider"

      movie = FactoryGirl.create(:movie)
      FactoryGirl.create(:tmdb_profile, tmdb_id: 287_947, movie:, title: "Shazam!")

      described_class.for(watchable: movie, country: "en-US").refresh

      # The payload actually comes with 13 providers but 51 offers. Two of
      # those offers don't have a recognizable provider_id, so we don't import
      # them. A quick look at prod data reveals these are outdated providers and
      # shouldn't be used anyway.
      expect(Provider.count).to be(13)
      expect(Offer.count).to be(49)
    end

    it "fetches offers for the given country", vcr: {cassette_name: "import/offers/countries"} do
      movie = FactoryGirl.create(:movie, :up)

      subject = described_class.for(watchable: movie, country: "es-ES")

      expect { subject.refresh }.
        to change { movie.offers.reload.size }.from(0).
        and change { Provider.count }.from(0)
      google_play_rental = movie.offers.joins(:provider).
        find_by(providers: {clear_name: "Google Play Movies"}, monetization_type: "buy", presentation_type: "sd")
      expect(google_play_rental.price).to eql(Money.new(1_199, "EUR"))
    end

    it "only refreshes the offers for the given country", vcr: {cassette_name: "import/offers/countries"} do
      movie = FactoryGirl.create(:movie, :up)
      disney_plus = FactoryGirl.create(:provider, provider_id: 337, clear_name: "Disney Plus")
      us_offer = FactoryGirl.create(:offer, watchable: movie, provider: disney_plus, country: "en-US")

      described_class.for(watchable: movie, country: "es-ES").refresh

      expect(us_offer.reload).to be_present
    end
  end

  describe "#refresh for TV show" do
    it "fetches all the offers and providers for a TV show", vcr: {cassette_name: "import/offers/fetch_tv_show_offers"} do
      tv_show = FactoryGirl.create(:tv_show, :the_wire)

      subject = described_class.for(watchable: tv_show, country: "en-US")

      expect { subject.refresh }.
        to change { tv_show.offers.reload.size }.from(0).
        and change { Provider.count }.from(0)
      apple = tv_show.offers.joins(:provider).
        find_by(providers: {clear_name: "Apple TV"}, monetization_type: "buy", presentation_type: "sd")
      expect(apple.price).to eql(Money.new(98_95, "USD"))
    end

    it "logs when it can't find offers for a TV show", vcr: {cassette_name: "import/offers/no_tv_show_offers"} do
      pending "Finding another show that isn't in Just Watch"
      # https://moviemates.party/tv_shows/venga-juan-2021

      movie = FactoryGirl.create(:movie)
      FactoryGirl.create(:tmdb_profile, movie:, title: "Disenchantment", tmdb_id: 73_021)
      allow(Rails.logger).to receive(:info)

      described_class.for(watchable: movie, country: "en-US").refresh

      expect(Rails.logger).to have_received(:info)
    end

    it "refreshes all the offers for a TV show, preserving existing providers", vcr: {cassette_name: "import/offers/refresh_tv_show_offers"} do
      tv_show = FactoryGirl.create(:tv_show, :the_wire)
      # This is the real ID from a real offer in the cassette. It's important to test the code entirely
      microsoft = Provider.create!(clear_name: "Microsoft Store", provider_id: 68)
      old_offer = FactoryGirl.create(:offer, provider: microsoft, watchable: tv_show, country: "en-US")

      described_class.for(watchable: tv_show, country: "en-US").refresh

      expect(Offer.find_by(id: old_offer.id)).to be_nil
      expect(tv_show.offers.reload.size).to be > 0
      expect(microsoft.reload).to_not be(nil)
    end

    it "fetches offers for the given country", vcr: {cassette_name: "import/offers/tv_show_countries"} do
      tv_show = FactoryGirl.create(:tv_show, :the_wire)

      subject = described_class.for(watchable: tv_show, country: "es-ES")

      expect { subject.refresh }.
        to change { tv_show.offers.reload.size }.from(0).
        and change { Provider.count }.from(0)
      hbo = tv_show.offers.joins(:provider).
        find_by(providers: {clear_name: "Max"}, monetization_type: "flatrate", presentation_type: "hd")
      expect(hbo.price).to eql(Money.new(0, "EUR"))
    end

    it "only refreshes the offers for the given country", vcr: {cassette_name: "import/offers/tv_show_countries"} do
      tv_show = FactoryGirl.create(:tv_show, :the_wire)
      disney_plus = FactoryGirl.create(:provider, provider_id: 337, clear_name: "Disney Plus")
      us_offer = FactoryGirl.create(:offer, watchable: tv_show, provider: disney_plus, country: "en-US")

      described_class.for(watchable: tv_show, country: "es-ES").refresh

      expect(us_offer.reload).to be_present
    end
  end
end
