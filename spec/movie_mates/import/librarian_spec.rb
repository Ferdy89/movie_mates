require "rails_helper"
require_relative "tmdb_movie_wrapper_double"
require_relative "tmdb_tv_show_wrapper_double"

RSpec.describe MovieMates::Import::Librarian do
  let(:group) { FactoryGirl.create(:group) }
  let(:user)  { FactoryGirl.create(:user, group:) }

  describe "#nominate_movie" do
    it "creates a Nomination for the movie with the given TMDb ID" do
      subject = described_class.new(tmdb_id: 123, user:)
      allow(MovieMates::Import::TmdbMovieWrapper).to receive(:by_id) do
        TmdbMovieWrapperDouble.new(tmdb_id: 123, title: "Die Hard", release_date: Date.new(1988))
      end

      nomination = subject.nominate_movie

      expect(nomination.contributor).to eql(user)
      expect(nomination.movie.slug).to eql("die-hard-1988")
      expect(nomination.movie.tmdb_profile.title).to eql("Die Hard")
    end

    it "updates an existing profile" do
      subject = described_class.new(tmdb_id: 123, user:)
      movie = FactoryGirl.create(:movie)
      profile = FactoryGirl.create(:tmdb_profile, tmdb_id: 123, title: "Die Softly", movie:)
      allow(MovieMates::Import::TmdbMovieWrapper).to receive(:by_id) do
        TmdbMovieWrapperDouble.new(tmdb_id: 123, title: "Die Hard")
      end

      expect { subject.nominate_movie }.to change { profile.reload.title }.to("Die Hard")
    end

    it "votes yes to a movie for a given user" do
      subject = described_class.new(tmdb_id: 123, user:)
      allow(MovieMates::Import::TmdbMovieWrapper).to receive(:by_id) do
        TmdbMovieWrapperDouble.new(tmdb_id: 123)
      end

      nomination = subject.nominate_movie

      expect(nomination.votes.first.opinion).to eql("yes")
    end

    it "creates the slug without a year when there is no release date" do
      subject = described_class.new(tmdb_id: 123, user:)
      allow(MovieMates::Import::TmdbMovieWrapper).to receive(:by_id) do
        TmdbMovieWrapperDouble.new(tmdb_id: 123, title: "Die Hard", release_date: nil)
      end

      nomination = subject.nominate_movie

      expect(nomination.movie.slug).to eql("die-hard")
    end

    it "also refreshes the offers for a new movie" do
      subject = described_class.new(tmdb_id: 123, user:)
      allow(MovieMates::Import::TmdbMovieWrapper).to receive(:by_id) do
        TmdbMovieWrapperDouble.new(tmdb_id: 123, title: "Die Hard", release_date: Date.new(1988))
      end

      nomination = subject.nominate_movie

      expect(OffersAllCountriesRefresher.jobs).to match_array([
        a_hash_including(
          "class" => "OffersAllCountriesRefresher",
          "args" => ["Movie", nomination.movie.id]
        ),
      ])
    end
  end

  describe "#nominate_tv_show" do
    it "creates a Nomination for the TV show with the given TMDb ID" do
      subject = described_class.new(tmdb_id: 123, user:)
      allow(MovieMates::Import::TmdbTvShowWrapper).to receive(:by_id) do
        TmdbTvShowWrapperDouble.new(tmdb_id: 123, title: "The Wire", release_date: Date.new(2002))
      end

      nomination = subject.nominate_tv_show

      expect(nomination.contributor).to eql(user)
      expect(nomination.tv_show.slug).to eql("the-wire-2002")
      expect(nomination.tv_show.tmdb_profile.title).to eql("The Wire")
    end

    it "updates an existing profile" do
      subject = described_class.new(tmdb_id: 123, user:)
      tv_show = FactoryGirl.create(:tv_show)
      profile = FactoryGirl.create(:tmdb_tv_show_profile, tmdb_id: 123, title: "The Guayer", tv_show:)
      allow(MovieMates::Import::TmdbTvShowWrapper).to receive(:by_id) do
        TmdbTvShowWrapperDouble.new(tmdb_id: 123, title: "The Wire")
      end

      expect { subject.nominate_tv_show }.to change { profile.reload.title }.to("The Wire")
    end

    it "votes yes to a TV show for a given user" do
      subject = described_class.new(tmdb_id: 123, user:)
      allow(MovieMates::Import::TmdbTvShowWrapper).to receive(:by_id) do
        TmdbTvShowWrapperDouble.new(tmdb_id: 123)
      end

      nomination = subject.nominate_tv_show

      expect(nomination.votes.first.opinion).to eql("yes")
    end

    it "imports the seasons" do
      subject = described_class.new(tmdb_id: 123, user:)
      allow(MovieMates::Import::TmdbTvShowWrapper).to receive(:by_id) do
        TmdbTvShowWrapperDouble.new(tmdb_id: 123, title: "The Wire", seasons: [
          SeasonDouble.new(
            name: "Season 1",
            air_date: Date.new(2002, 5, 11),
            episode_count: 10,
            season_number: 1,
            poster_url: "http://themoviedb.org/the_wire.png"
          ),
        ])
      end

      nomination = subject.nominate_tv_show

      seasons = nomination.tv_show.tmdb_profile.seasons
      expect(seasons.count).to be(1)
      expect(seasons.first.name).to eql("Season 1")
      expect(seasons.first.air_date).to eql(Date.new(2002, 5, 11))
      expect(seasons.first.episode_count).to eql(10)
      expect(seasons.first.season_number).to eql(1)
      expect(seasons.first.poster_url).to eql("http://themoviedb.org/the_wire.png")
    end

    it "creates the slug without a year when there is no release date" do
      subject = described_class.new(tmdb_id: 123, user:)
      allow(MovieMates::Import::TmdbTvShowWrapper).to receive(:by_id) do
        TmdbTvShowWrapperDouble.new(tmdb_id: 123, title: "The Wire", release_date: nil)
      end

      nomination = subject.nominate_tv_show

      expect(nomination.tv_show.slug).to eql("the-wire")
    end

    it "eliminates from the slug any characters that would make bad URLs" do
      subject = described_class.new(tmdb_id: 123, user:)
      allow(MovieMates::Import::TmdbTvShowWrapper).to receive(:by_id) do
        TmdbTvShowWrapperDouble.new(tmdb_id: 123, title: "Don't f#@& with cats!", release_date: Date.new(2018))
      end

      nomination = subject.nominate_tv_show

      expect(nomination.tv_show.slug).to eql("don-t-f-with-cats-2018")
    end

    it "also refreshes the offers for a new TV show" do
      subject = described_class.new(tmdb_id: 123, user:)
      allow(MovieMates::Import::TmdbTvShowWrapper).to receive(:by_id) do
        TmdbTvShowWrapperDouble.new(tmdb_id: 123, title: "Die Hard", release_date: Date.new(1988))
      end

      nomination = subject.nominate_tv_show

      expect(OffersAllCountriesRefresher.jobs).to match_array([
        a_hash_including(
          "class" => "OffersAllCountriesRefresher",
          "args" => ["TvShow", nomination.tv_show.id]
        ),
      ])
    end
  end
end
