require "rails_helper"
require_relative "tmdb_tv_show_wrapper_double"

RSpec.describe MovieMates::Import::TvShowRefresher do
  describe "#refresh" do
    it "updates a TV show with the latest data from TMDb" do
      tv_show = FactoryGirl.create(:tv_show)
      tmdb_profile = FactoryGirl.create(:tmdb_tv_show_profile, tv_show:, tmdb_id: 123)
      allow(MovieMates::Import::TmdbTvShowWrapper).to receive(:by_id) do
        TmdbTvShowWrapperDouble.new(tmdb_id: 123, title: "The Wire", release_date: Date.new(2002))
      end

      subject = described_class.new(tv_show:)

      expect { subject.refresh }.to change { tmdb_profile.reload.title }.to("The Wire")
    end

    it "updates the TV show seasons as well" do
      tv_show = FactoryGirl.create(:tv_show)
      tmdb_profile = FactoryGirl.create(:tmdb_tv_show_profile, tv_show:, tmdb_id: 123)
      FactoryGirl.create(:season, tmdb_tv_show_profile: tmdb_profile, season_number: 1, episode_count: 8)
      allow(MovieMates::Import::TmdbTvShowWrapper).to receive(:by_id) do
        TmdbTvShowWrapperDouble.new(
          tmdb_id: 123,
          title: "The Wire",
          release_date: Date.new(2002),
          seasons: [
            SeasonDouble.new(
              name: "Season 1",
              air_date: Date.new(2002, 5, 11),
              episode_count: 10,
              season_number: 1,
              poster_url: "http://themoviedb.org/the_wire1.png"
            ),
            SeasonDouble.new(
              name: "Season 2",
              air_date: Date.new(2003, 5, 11),
              episode_count: 10,
              season_number: 2,
              poster_url: "http://themoviedb.org/the_wire2.png"
            ),
          ]
        )
      end

      subject = described_class.new(tv_show:)

      expect { subject.refresh }.to change { tmdb_profile.reload.seasons.count }.to(2)
      first_season = tmdb_profile.seasons.order(:season_number).first
      second_season = tmdb_profile.seasons.order(:season_number).last
      expect(first_season.episode_count).to be(10)
      expect(second_season.episode_count).to be(10)
    end
  end
end
