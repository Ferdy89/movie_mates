require "rails_helper"

RSpec.describe MovieMates::Import::TmdbMovieWrapper do
  describe ".by_id", vcr: {cassette_name: "import/movie/die_hard_562"} do
    it "wraps a TMDb movie from its ID" do
      subject = described_class.by_id(562)

      expect(subject.tmdb_id).to be(562)
      expect(subject.title).to eql("Die Hard")
      expect(subject.plot).to eql(
        "NYPD cop, John McClane's plan to reconcile with his estranged wife "\
        "is thrown for a serious loop when minutes after he arrives at her "\
        "office, the entire building is overtaken by a group of terrorists. "\
        "With little help from the LAPD, wisecracking McClane sets out to "\
        "single-handedly rescue the hostages and bring the bad guys down."
      )
      expect(subject.release_date).to eql(Date.new(1988, 7, 15))
      expect(subject.poster_url).to eql("https://image.tmdb.org/t/p/w500/mc7MubOLcIw3MDvnuQFrO9psfCa.jpg")
      expect(subject.rating).to eql(7.5)
      expect(subject.imdb_id).to eql("tt0095016")
      expect(subject.minutes).to eql(131)
      expect(subject.genres).to eql(["Action", "Thriller"])
    end

    it "raises a specific error when the movie couldn't be found", vcr: {cassette_name: "import/movie/404"} do
      # This used to be the movie "Don't f**k with cats", but TMDb changed the
      # ID one day and refreshes stopped working. They seem to do this
      # sometimes.
      # While the proper fix would be to re-find the movie again, the low-cost
      # solution for now is to raise an error so we can manually change the ID.
      expect { described_class.by_id(657_733) }.
        to raise_error(described_class::MovieNotFound)
    end
  end

  describe "#hash_with" do
    it "creates a hash with the attributes passed", vcr: {cassette_name: "import/movie/die_hard_562"} do
      subject = described_class.by_id(562)

      expect(subject.hash_with(:tmdb_id, :title, :rating, :minutes)).to eql(
        tmdb_id: 562,
        title: "Die Hard",
        rating: 7.5,
        minutes: 131
      )
    end
  end

  describe "#release_date" do
    it "retrieves the release date of a movie", vcr: {cassette_name: "import/movie/die_hard_562"} do
      subject = described_class.by_id(562)

      expect(subject.release_date).to eql(Date.new(1988, 7, 15))
    end

    it "does not contain a release_date when the movie does not have it", vcr: {cassette_name: "import/movie/southside"} do
      # Strange case of a "movie" without a release date
      subject = described_class.by_id(310_927)

      expect(subject.release_date).to be(nil)
    end
  end

  describe "#poster_url" do
    it "retrieves the URL for the poster of a movie", vcr: {cassette_name: "import/movie/die_hard_562"} do
      subject = described_class.by_id(562)

      expect(subject.poster_url).to eql("https://image.tmdb.org/t/p/w500/mc7MubOLcIw3MDvnuQFrO9psfCa.jpg")
    end
  end

  describe "#imdb_id" do
    it "retrieves the IMDb ID of a movie", vcr: {cassette_name: "import/movie/die_hard_562"} do
      subject = described_class.by_id(562)

      expect(subject.imdb_id).to eql("tt0095016")
    end
  end

  describe "#minutes" do
    it "retrieves the runtime of the movie in minutes", vcr: {cassette_name: "import/movie/die_hard_562"} do
      subject = described_class.by_id(562)

      expect(subject.minutes).to eql(131)
    end
  end

  describe "#url" do
    it "builds the URL of the movie in TMDb", vcr: {cassette_name: "import/movie/die_hard_562"} do
      subject = described_class.by_id(562)

      expect(subject.url).to eql("https://www.themoviedb.org/movie/562")
    end
  end

  describe "#trailer_youtube_id", vcr: {cassette_name: "import/movie/die_hard_562_with_trailer"} do
    it "retrieves the YouTube Video ID for the trailer of the movie" do
      subject = described_class.by_id(562)

      expect(subject.trailer_youtube_id).to eql("2TQ-pOvI6Xo")
    end
  end
end
