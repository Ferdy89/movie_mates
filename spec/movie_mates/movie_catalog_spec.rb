require "rails_helper"

RSpec.describe MovieMates::MovieCatalog do
  subject { described_class.new(current_user:, movie_ids: [up.id, heat.id]) }

  describe "#as_json" do
    let!(:table) do
      create_group_table(
        "      | Fer | Ainho | Fido |",
        " Up   | y   | a     | n    | Fer",
        " Heat | y   | n     | y    | Fido"
      )
    end
    let(:current_user) { table[:users][:fer] }
    let(:up)           { table[:movies][:up] }
    let(:heat)         { table[:movies][:heat] }

    it "renders a hash with organized information about the users and the movies passed" do
      up_nomination = MovieNomination.find_by(group: table[:group], movie: up)
      FactoryGirl.create(:comment, nomination: up_nomination, author: current_user)
      up.tmdb_profile.update!(release_date: nil)

      expect(subject.as_json.fetch(:watchables)).to match(
        [
          {
            slug: up.slug,
            tmdb_id: up.tmdb_profile.tmdb_id,
            title: "Up",
            year: nil,
            minutes: 96,
            rating: a_value_between(0, 10),
            poster: a_string_matching(/\Ahttp.+\.jpg\z/),
            comment_count: 1,
            contributor: "Fer",
            votes: a_collection_containing_exactly(
              {user_name: "Fer", opinion: "yes"},
              {user_name: "Ainho", opinion: "abstention"},
              user_name: "Fido", opinion: "no"
            ),
            current_user_vote: "yes",
          },
          {
            slug: heat.slug,
            tmdb_id: heat.tmdb_profile.tmdb_id,
            title: "Heat",
            year: 1995,
            minutes: 170,
            rating: a_value_between(0, 10),
            poster: a_string_matching(/\Ahttp.+\.jpg\z/),
            comment_count: 0,
            contributor: "Fido",
            votes: a_collection_containing_exactly(
              {user_name: "Fer", opinion: "yes"},
              {user_name: "Ainho", opinion: "no"},
              user_name: "Fido", opinion: "yes"
            ),
            current_user_vote: "yes",
          },
        ]
      )
    end

    it "does not incur in N+1 queries and queries models efficiently" do
      query_tracker = Support::QueryTracker.new

      query_tracker.during { subject.as_json }

      expect(query_tracker.models_queried).to eql(
        Comment: 1,
        Movie: 1,
        TmdbProfile: 1,
        MovieNomination: 1,
        User: 2,
        Vote: 1
      )
    end
  end
end
