require "rails_helper"

RSpec.describe MovieMates::TvShowCatalog do
  subject { described_class.new(current_user:, tv_show_ids: [the_wire.id, sherlock.id]) }

  describe "#as_json" do
    let!(:table) do
      create_tv_show_group_table(
        "          | Fer | Ainho | Fido |",
        " The Wire | y   | a     | n    | Fer",
        " Sherlock | y   | n     | y    | Fido"
      )
    end
    let(:current_user) { table[:users][:fer] }
    let(:the_wire)     { table[:tv_shows][:the_wire] }
    let(:sherlock)     { table[:tv_shows][:sherlock] }

    it "renders a hash with organized information about the users and the TV shows passed" do
      up_nomination = TvShowNomination.find_by(group: table[:group], tv_show: the_wire)
      FactoryGirl.create(:comment, nomination: up_nomination, author: current_user)
      the_wire.tmdb_profile.update!(release_date: nil)

      expect(subject.as_json.fetch(:watchables)).to match(
        [
          {
            slug: the_wire.slug,
            tmdb_id: the_wire.tmdb_profile.tmdb_id,
            title: "The Wire",
            year: nil,
            minutes: 60,
            rating: a_value_between(0, 10),
            poster: a_string_matching(/\Ahttp.+\.jpg\z/),
            comment_count: 1,
            contributor: "Fer",
            votes: a_collection_containing_exactly(
              {user_name: "Fer", opinion: "yes"},
              {user_name: "Ainho", opinion: "abstention"},
              user_name: "Fido", opinion: "no"
            ),
            current_user_vote: "yes",
          },
          {
            slug: sherlock.slug,
            tmdb_id: sherlock.tmdb_profile.tmdb_id,
            title: "Sherlock",
            year: 2010,
            minutes: 90,
            rating: a_value_between(0, 10),
            poster: a_string_matching(/\Ahttp.+\.jpg\z/),
            comment_count: 0,
            contributor: "Fido",
            votes: a_collection_containing_exactly(
              {user_name: "Fer", opinion: "yes"},
              {user_name: "Ainho", opinion: "no"},
              user_name: "Fido", opinion: "yes"
            ),
            current_user_vote: "yes",
          },
        ]
      )
    end

    it "does not incur in N+1 queries and queries models efficiently" do
      query_tracker = Support::QueryTracker.new

      query_tracker.during { subject.as_json }

      expect(query_tracker.models_queried).to eql(
        Comment: 1,
        TvShow: 1,
        TmdbTvShowProfile: 1,
        TvShowNomination: 1,
        User: 2,
        Vote: 1
      )
    end
  end
end
