require "rails_helper"

RSpec.describe MovieMates::Registration::UserCredentials do
  describe "#register" do
    subject { described_class.for(invitation_token: nil, omniauth_hash: google_login) }

    let(:google_login) do
      {
        "provider" => "google_oauth2",
        "uid" => "123545",
        "info" => {
          "email" => "user@moviemates.party",
          "first_name" => "MovieMate",
          "image" => "https://googleusercontent.com/photo.jpg",
        },
      }
    end

    it "creates a new group and a new user from the information coming from a Google login" do
      user = subject.register

      expect(user).to be_persisted
      expect(user).to have_attributes(
        provider: "google_oauth2",
        uid: "123545",
        email: "user@moviemates.party",
        google_pic: /photo.jpg/,
        name: "MovieMate"
      )
    end

    it "updates an existing user but preserves the name" do
      existing_group = FactoryGirl.create(:group)
      existing_user  = FactoryGirl.create(
        :user,
        group: existing_group,
        provider: "google_oauth2",
        uid: "123545",
        email: "old@email.com",
        google_pic: "picture.png",
        name: "Existing Name"
      )

      user = subject.register

      expect(user).to eql(existing_user)
      expect(user.group).to eql(existing_group)
      expect(user).to have_attributes(email: "user@moviemates.party", name: "Existing Name", google_pic: /photo.jpg/)
    end
  end
end
