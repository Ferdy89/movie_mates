require "rails_helper"

RSpec.describe MovieMates::Registration::GroupSplit do
  describe "#split" do
    it "separates the current user from their group, creating a new one with nominations" do
      initial_table = create_group_table(
        "    | Fer | Ainho |",
        " Up | y   | w     | Ainho"
      )
      initial_movie_nomination = initial_table[:movies][:up].nominations.first
      fer = initial_table[:users][:fer]

      described_class.split_into_new_group(user: fer)

      # Creates a new group
      new_group = fer.group.reload
      expect(new_group).to_not eql(initial_table[:group])

      # Move user to the new group
      expect(new_group.users).to match_array([fer])
      expect(initial_table[:group].reload.users).to match_array([initial_table[:users][:ainho]])

      # Duplicates nominations in the new group
      new_movie_nomination = new_group.movie_nominations.first
      expect(new_movie_nomination).to_not eql(initial_movie_nomination)
      expect(new_movie_nomination.movie).to eql(initial_table[:movies][:up])

      # Move all votes
      expect(new_movie_nomination.votes.count).to be(1)
      expect(new_movie_nomination.votes.first.opinion).to eql("yes")
      expect(new_movie_nomination.votes.first.voter).to eql(fer)
      expect(initial_movie_nomination.votes.count).to be(1)
      expect(initial_movie_nomination.votes.first.opinion).to eql("watched")
      expect(initial_movie_nomination.votes.first.voter).to eql(initial_table[:users][:ainho])
    end
  end
end
