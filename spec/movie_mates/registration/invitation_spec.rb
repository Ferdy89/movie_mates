require "rails_helper"

RSpec.describe MovieMates::Registration::Invitation do
  describe "#accept" do
    it "moves the user to the new group" do
      first_group = FactoryGirl.create(:group)
      second_table = create_group_table(
        "    | Ainho |",
        " Up | y     | Ainho"
      )
      ainho = second_table[:users][:ainho]
      token = FactoryGirl.create(:group_invitation, group: first_group).token

      invitation = described_class.from(token:, user: ainho)

      expect { invitation.accept }.to change { first_group.users.count }.by(1)
    end

    it "moves the nominations to the new group" do
      first_group = FactoryGirl.create(:group)
      second_table = create_group_table(
        "    | Ainho |",
        " Up | y     | Ainho"
      )
      second_group = second_table[:group]

      up_movie = second_table[:movies][:up]
      ainho = second_table[:users][:ainho]
      token = FactoryGirl.create(:group_invitation, group: first_group).token

      invitation = described_class.from(token:, user: ainho)
      invitation.accept

      new_nomination = MovieNomination.find_by(group: first_group, movie: up_movie)
      expect(new_nomination.votes.count).to be(1)
      expect(new_nomination.votes.first.voter).to eql(ainho)
      expect(new_nomination.votes.first.opinion).to eql("yes")

      old_nomination = MovieNomination.find_by(group: second_group, movie: up_movie)
      expect(old_nomination).to be_nil
    end

    it "destroys the used invitation" do
      first_group = FactoryGirl.create(:group)
      second_table = create_group_table(
        "    | Ainho |",
        " Up | y     | Ainho"
      )
      ainho = second_table[:users][:ainho]
      token = FactoryGirl.create(:group_invitation, group: first_group).token

      invitation = described_class.from(token:, user: ainho)

      expect { invitation.accept }.to change { first_group.group_invitations.count }.by(-1)
    end

    it "moves the nominations unvoted when the user had not voted them on the old group either" do
      first_group = FactoryGirl.create(:group)
      second_table = create_group_table(
        "    | Ainho |",
        " Up | u     | Ainho"
      )

      up_movie = second_table[:movies][:up]
      ainho = second_table[:users][:ainho]
      token = FactoryGirl.create(:group_invitation, group: first_group).token

      invitation = described_class.from(token:, user: ainho)
      invitation.accept

      new_nomination = MovieNomination.find_by(group: first_group, movie: up_movie)
      expect(new_nomination.votes.count).to be_zero
    end

    it "destroys the old group when the user moves was the last one" do
      first_group  = FactoryGirl.create(:group)
      second_group = FactoryGirl.create(:group)
      ainho        = FactoryGirl.create(:user, group: second_group)
      token        = FactoryGirl.create(:group_invitation, group: first_group).token

      invitation = described_class.from(token:, user: ainho)

      expect { invitation.accept }.to change { Group.find_by(id: second_group.id) }.from(be_present).to(nil)
    end

    it "merges the votes of existing nominations" do
      first_table = create_group_table(
        "         | Fer |",
        " Up      | y   | Fer"
      )
      first_group = first_table[:group]
      second_table = create_group_table(
        "    | Ainho |",
        " Up | y     | Ainho"
      )

      up_movie = first_table[:movies][:up]
      ainho = second_table[:users][:ainho]
      token = FactoryGirl.create(:group_invitation, group: first_group).token

      invitation = described_class.from(token:, user: ainho)

      expect { invitation.accept }.to change { MovieNomination.find_by(group: first_group, movie: up_movie).votes.count }.by(1)
    end
  end
end
