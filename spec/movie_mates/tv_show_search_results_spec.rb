require "rails_helper"

RSpec.describe MovieMates::TvShowSearchResults do
  describe "#as_json" do
    it "can also search TV shows", vcr: {cassette_name: "search/search_wire"} do
      table = create_tv_show_group_table(
        "          | Fer |",
        " The Wire | y   | Fer "
      )
      the_wire = table[:tv_shows][:the_wire]
      # TMDb's search tolerates small typos
      subject = described_class.from("the wir", user: table[:users][:fer])

      expect(subject.as_json.fetch(:library).fetch(:watchables)).
        to include(a_hash_including(tmdb_id: the_wire.tmdb_profile.tmdb_id))
      expect(subject.as_json.fetch(:external)).
        to_not include(a_hash_including(tmdb_id: the_wire.tmdb_profile.tmdb_id))
      expect(subject.as_json.fetch(:term)).to eq("the wir")
    end

    it "doesn't search unless there's a term to search" do
      group = FactoryGirl.create(:group)
      fer = FactoryGirl.create(:user, :fer, group:)
      subject = described_class.from(nil, user: fer)

      expect(subject.as_json.fetch(:library).fetch(:watchables)).to be_empty
      expect(subject.as_json.fetch(:external)).to be_empty
    end
  end
end
