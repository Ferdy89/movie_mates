require "rails_helper"

RSpec.describe MovieMates::SearchResults do
  describe "#as_json" do
    it "looks for any matches in the library from external results and separates them accordingly", vcr: {cassette_name: "search/up"} do
      table = create_group_table(
        "    | Fer |",
        " Up | y   | Fer "
      )
      up = table[:movies][:up]
      # TMDb's search tolerates small typos
      subject = described_class.from("upp", user: table[:users][:fer])

      expect(subject.as_json.fetch(:library).fetch(:watchables)).to include(a_hash_including(tmdb_id: up.tmdb_profile.tmdb_id))
      expect(subject.as_json.fetch(:external)).to_not include(a_hash_including(tmdb_id: up.tmdb_profile.tmdb_id))
      expect(subject.as_json.fetch(:term)).to eq("upp")
    end

    it "doesn't search unless there's a term to search" do
      group = FactoryGirl.create(:group)
      fer = FactoryGirl.create(:user, :fer, group:)
      subject = described_class.from(nil, user: fer)

      expect(subject.as_json.fetch(:library).fetch(:watchables)).to be_empty
      expect(subject.as_json.fetch(:external)).to be_empty
    end

    it "optionally allows for a year to search", vcr: {cassette_name: "search/house"} do
      group = FactoryGirl.create(:group)
      fer = FactoryGirl.create(:user, :fer, group:)
      subject = described_class.from("house", user: fer, year: "1977")

      expect(subject.as_json.fetch(:external)).to include(a_hash_including(title: "ハウス", release_date: Date.new(1977, 8, 26)))
      expect(subject.as_json.fetch(:year)).to eq("1977")
    end
  end
end
