class Support::QueryTracker
  attr_reader :models_queried

  def initialize
    @models_queried = Hash.new(0)
  end

  def during
    subscription = enable_subscription

    yield
  ensure
    ActiveSupport::Notifications.unsubscribe(subscription)
  end

  private

  def enable_subscription
    ActiveSupport::Notifications.
      subscribe("sql.active_record") do |_, _, _, _, data|
      query_type = data[:name]

      if query_type
        match = query_type.match(/\A(\w+) Load\z/)

        models_queried[match[1].to_sym] += 1 if match
      end
    end
  end
end
