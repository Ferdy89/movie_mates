module Support::CreateGroupTable
  def create_group_table(user_line, *movie_lines)
    group  = FactoryGirl.create(:group)
    users  = create_users(user_line, group)
    movies = UserGroup.new(group, users).create_movies(movie_lines)

    {group:, users:, movies:}
  end

  def create_tv_show_group_table(user_line, *tv_show_lines)
    group = FactoryGirl.create(:group)
    users = create_users(user_line, group)
    tv_shows = UserGroup.new(group, users).create_tv_shows(tv_show_lines)

    {group:, users:, tv_shows:}
  end

  private

  def create_users(user_line, group)
    user_names   = user_line.split("|").reject(&:blank?).map(&:strip)
    user_symbols = user_names.map(&:downcase).map(&:to_sym)

    user_symbols.index_with do |user_name|
      FactoryGirl.create(:user, user_name, group:)
    end
  end

  class UserGroup < Struct.new(:group, :users)
    def create_movies(movie_lines)
      movie_lines.each_with_object({}) do |movie_line, mem|
        movie_name, *votes, contributor =
          movie_line.split("|").map(&:strip).map(&:downcase).map(&:to_sym)

        movie = create_movie(movie_name, contributor, votes)

        mem[movie_name] = movie
      end
    end

    def create_tv_shows(tv_show_lines)
      tv_show_lines.each_with_object({}) do |tv_show_line, mem|
        tv_show_name, *votes, contributor =
          tv_show_line.
            split("|").
            map(&:strip).
            map(&:downcase).
            map { |title| title.tr(" ", "_") }.
            map(&:to_sym)

        tv_show = create_tv_show(tv_show_name, contributor, votes)

        mem[tv_show_name] = tv_show
      end
    end

    private

    def create_movie(movie_name, contributor, votes)
      movie      = find_or_create_movie(movie_name)
      nomination = FactoryGirl.create(:movie_nomination,
                                      movie:,
                                      contributor: users[contributor],
                                      group:)

      create_votes(votes, nomination)

      movie
    end

    def create_tv_show(tv_show_name, contributor, votes)
      tv_show = find_or_create_tv_show(tv_show_name)
      nomination = FactoryGirl.create(:tv_show_nomination,
                                      tv_show:,
                                      contributor: users[contributor],
                                      group:)

      create_votes(votes, nomination)

      tv_show
    end

    def create_votes(votes, nomination)
      votes.zip(users.values).each do |(vote_key, user)|
        next if vote_key == :u

        FactoryGirl.create(:vote,
                           opinion: opinions_enum[vote_key],
                           nomination:,
                           voter: user)
      end
    end

    def find_or_create_movie(movie_name)
      existing_movie =
        Movie.joins(:tmdb_profile).
          find_by(tmdb_profiles: {title: movie_name.to_s.capitalize})
      existing_movie || FactoryGirl.create(:movie, movie_name)
    end

    def find_or_create_tv_show(tv_show_slug)
      tv_show_name = tv_show_slug.to_s.split("_").map(&:capitalize).join(" ")
      existing_tv_show =
        TvShow.joins(:tmdb_tv_show_profile).
          find_by(tmdb_tv_show_profiles: {title: tv_show_name})
      existing_tv_show || FactoryGirl.create(:tv_show, tv_show_slug)
    end

    def opinions_enum
      @opinions_enum ||= {
        y: "yes",
        n: "no",
        a: "abstention",
        w: "watched",
      }
    end
  end
end
