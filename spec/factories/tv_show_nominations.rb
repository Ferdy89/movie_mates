FactoryGirl.define do
  factory :tv_show_nomination do
    trait :the_wire do
      tv_show     { build(:tv_show, :the_wire) }
      contributor { build(:user, :fer) }
    end

    trait :sherlock do
      tv_show     { build(:tv_show, :sherlock) }
      contributor { build(:user, :fer) }
    end

    trait :lost do
      tv_show     { build(:tv_show, :lost) }
      contributor { build(:user, :fer) }
    end

    trait :seinfeld do
      tv_show     { build(:tv_show, :seinfeld) }
      contributor { build(:user, :fer) }
    end

    trait :friends do
      tv_show     { build(:tv_show, :friends) }
      contributor { build(:user, :fer) }
    end

    trait :homeland do
      tv_show     { build(:tv_show, :homeland) }
      contributor { build(:user, :fer) }
    end

    trait :dark do
      tv_show     { build(:tv_show, :dark) }
      contributor { build(:user, :fer) }
    end
  end
end
