FactoryGirl.define do
  factory :provider do
    sequence(:provider_id)

    trait :netflix do
      provider_id 1 # Not the real one from JustWatch
      clear_name "Netflix"
    end

    trait :amazon_prime do
      provider_id 2 # Not the real one from JustWatch
      clear_name "Amazon Prime"
    end

    trait :youtube do
      provider_id 5 # Not the real one from JustWatch
      clear_name "YouTube"
    end
  end
end
