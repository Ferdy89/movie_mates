FactoryGirl.define do
  factory :tv_show do
    sequence(:slug) { |n| "tv-show-#{n}" }

    trait :the_wire do
      tmdb_tv_show_profile { build(:tmdb_tv_show_profile, :the_wire) }
    end

    trait :sherlock do
      tmdb_tv_show_profile { build(:tmdb_tv_show_profile, :sherlock) }
    end

    trait :lost do
      tmdb_tv_show_profile { build(:tmdb_tv_show_profile, :lost) }
    end

    trait :seinfeld do
      tmdb_tv_show_profile { build(:tmdb_tv_show_profile, :seinfeld) }
    end

    trait :friends do
      tmdb_tv_show_profile { build(:tmdb_tv_show_profile, :friends) }
    end

    trait :homeland do
      tmdb_tv_show_profile { build(:tmdb_tv_show_profile, :homeland) }
    end

    trait :dark do
      tmdb_tv_show_profile { build(:tmdb_tv_show_profile, :dark) }
    end
  end
end
