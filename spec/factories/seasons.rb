FactoryGirl.define do
  factory :season do
    sequence(:name) { |n| "Season #{n}" }
    sequence(:season_number)
    episode_count 10
    air_date { Time.zone.today }
    sequence(:poster_url) { |n| "http://themoviedb.org/#{n}.png" }
  end
end
