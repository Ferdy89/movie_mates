FactoryGirl.define do
  factory :tmdb_profile do
    sequence(:title) { |n| "Movie #{n}" }
    poster_url { "https://images-na.ssl-images-amazon.com/images/M/#{title}.jpg" } # Fake image, valid URL
    tmdb_id { Kernel.rand(1_000_000) }
    imdb_id { "tt#{tmdb_id}" }

    trait :alien do
      tmdb_id 123
      imdb_id "tt78748"
      title   "Alien"
      release_date Date.new(1979)
    end

    trait :up do
      tmdb_id 14_160
      imdb_id  "tt1049413"
      title    "Up"
      release_date Date.new(2009)
      minutes  96
      rating   8.3
      poster_url "https://images-na.ssl-images-amazon.com/images/M/up.jpg"
      trailer_youtube_id "ORFWdXl_zJ4"
    end

    trait :heat do
      tmdb_id 789
      imdb_id "tt113277"
      title   "Heat"
      release_date Date.new(1995)
      minutes 170
      rating  8.2
      poster_url "https://images-na.ssl-images-amazon.com/images/M/heat.jpg"
    end

    trait :snatch do
      tmdb_id 107
      imdb_id "tt208092"
      title   "Snatch"
      release_date Date.new(2000)
    end

    trait :gladiator do
      tmdb_id 345
      imdb_id "tt172495"
      title   "Gladiator"
      release_date Date.new(2000)
    end

    trait :amelie do
      tmdb_id 678
      imdb_id "tt211915"
      title   "Amélie"
      release_date Date.new(2001)
    end
  end
end
