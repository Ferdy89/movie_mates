FactoryGirl.define do
  factory :movie_nomination do
    trait :up do
      movie       { build(:movie, :up) }
      contributor { build(:user, :fer) }
    end

    trait :heat do
      movie       { build(:movie, :heat) }
      contributor { build(:user, :fido) }
    end

    trait :alien do
      movie       { build(:movie, :alien) }
      contributor { build(:user, :ainho) }
    end

    trait :snatch do
      movie       { build(:movie, :snatch) }
      contributor { build(:user, :fer) }
    end
  end
end
