FactoryGirl.define do
  factory :movie do
    sequence(:slug) { |n| "movie-#{n}" }

    trait :up do
      tmdb_profile { build(:tmdb_profile, :up) }
    end

    trait :heat do
      tmdb_profile { build(:tmdb_profile, :heat) }
    end

    trait :alien do
      tmdb_profile { build(:tmdb_profile, :alien) }
    end

    trait :snatch do
      tmdb_profile { build(:tmdb_profile, :snatch) }
    end

    trait :gladiator do
      tmdb_profile { build(:tmdb_profile, :gladiator) }
    end

    trait :amelie do
      tmdb_profile { build(:tmdb_profile, :amelie) }
    end
  end
end
