FactoryGirl.define do
  factory :offer do
    monetization_type "rent"
    presentation_type "HD"
    price_cents 399
    price_currency "USD"
  end
end
