FactoryGirl.define do
  factory :comment do
    body { "A random comment from a test factory!" }
  end
end
