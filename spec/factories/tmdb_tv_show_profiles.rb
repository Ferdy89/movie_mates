FactoryGirl.define do
  factory :tmdb_tv_show_profile do
    sequence(:title) { |n| "Movie #{n}" }
    poster_url { "https://images-na.ssl-images-amazon.com/images/M/#{title}.jpg" } # Fake image, valid URL
    tmdb_id { Kernel.rand(1_000_000) }
    in_production true

    trait :the_wire do
      tmdb_id 1438
      title   "The Wire"
      release_date Date.new(2002)
      minutes 60
      rating  8.4
    end

    trait :sherlock do
      tmdb_id 19_885
      title   "Sherlock"
      release_date Date.new(2010)
      minutes 90
      rating  8.3
    end

    trait :lost do
      tmdb_id 4_607
      title   "Lost"
      release_date Date.new(2004)
      minutes 45
      rating  7.9
    end

    trait :seinfeld do
      tmdb_id 1_400
      title   "Seinfeld"
      release_date Date.new(1989)
      minutes 22
      rating  8.3
    end

    trait :friends do
      tmdb_id 1_668
      title   "Friends"
      release_date Date.new(1994)
      minutes 25
      rating  8.3
    end

    trait :homeland do
      tmdb_id 1_407
      title   "Homeland"
      release_date Date.new(2011)
      minutes 55
      rating  7.5
    end

    trait :dark do
      tmdb_id 70_523
      title   "Dark"
      release_date Date.new(2017)
      minutes 53
      rating  8.4
    end
  end
end
