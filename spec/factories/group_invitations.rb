FactoryGirl.define do
  factory :group_invitation do
    sequence(:email) { |n| "email_#{n}@example.com" }
  end
end
