FactoryGirl.define do
  factory :user do
    name  "User"
    sequence(:email) { |n| "email_#{n}@example.com" }
    sequence(:google_pic) { |n| "https://googleusercontent.com/#{n}/photo.jpg" }

    trait :fer do
      name  "Fer"
      email "fer@example.com"
    end

    trait :fido do
      name  "Fido"
      email "fido@example.com"
    end

    trait :ainho do
      name  "Ainho"
      email "ainho@example.com"
    end

    trait :diego do
      name  "Diego"
      email "diego@example.com"
    end
  end
end
