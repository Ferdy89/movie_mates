FactoryGirl.define do
  factory :vote do
    trait :yes do
      opinion { "yes" }
    end

    trait :no do
      opinion { "no" }
    end

    trait :abstention do
      opinion { "abstention" }
    end
  end
end
