require "rails_helper"

RSpec.describe Picking::TvShowSession do
  describe "#as_json" do
    it "follows the Code of the MovieMaters to pick a list of TV shows to watch" do
      # Code of the MovieMaters:
      # * You shall not watch a TV show that any movie mate hasn't voted
      # * You shall watch a TV show that all present movie mates are willing to watch
      # * You shall watch a TV show that no absent movie mate wants to watch
      # * You shall try to watch the TV show that present movie mates want to watch the most
      table = create_tv_show_group_table(
        "          | Fer | Ainho | Fido |",
        " The Wire | y   | a     | a    | Fer ",
        " Sherlock | y   | y     | n    | Fer ",
        " Lost     | y   | y     | u    | Fer ",
        " Seinfeld | y   | y     | y    | Fer ",
        " Friends  | y   | n     | n    | Fer ",
        " Homeland | u   | y     | n    | Fer ",
        " Dark     | y   | y     | n    | Fer " # This one is fully watched, though
      )
      fer   = table[:users][:fer]
      ainho = table[:users][:ainho]
      dark  = table[:tv_shows][:dark]
      FactoryGirl.create(
        :season,
        tmdb_tv_show_profile: dark.tmdb_profile,
        season_number: 1,
        episode_count: 10
      )
      dark_nomination = dark.nominations.first
      dark_nomination.update!(
        last_season_watched: 1,
        last_episode_watched: 10
      )
      subject = described_class.new(
        current_user: fer,
        user_ids: [fer.id, ainho.id],
        providers: Picking::ProviderFilter.all,
        country: "en-US"
      )

      tv_shows = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(tv_shows).to eql(["Sherlock", "The Wire"])
    end

    it "isolates groups from each other, even when groups have the same amount of users" do
      create_tv_show_group_table(
        "          | Fer |",
        " The Wire | y   | Fer"
      )
      other_group = FactoryGirl.create(:group)
      other_user  = FactoryGirl.create(:user, group: other_group)
      subject = described_class.new(
        current_user: other_user,
        user_ids: [other_user.id],
        providers: Picking::ProviderFilter.all,
        country: "en-US"
      )

      expect(subject.as_json.fetch(:watchables)).to be_empty
    end

    it "prioritizes a TV show with more yes votes from the people present over the rest" do
      table = create_tv_show_group_table(
        "          | Fer | Ainho | Fido |",
        " The Wire | y   | a     | a    | Fer",
        " Sherlock | y   | y     | a    | Fer",
        " Lost     | y   | y     | y    | Fer"
      )
      fer   = table[:users][:fer]
      ainho = table[:users][:ainho]
      fido  = table[:users][:fido]
      subject = described_class.new(
        current_user: fer,
        user_ids: [fer.id, ainho.id, fido.id],
        providers: Picking::ProviderFilter.all,
        country: "en-US"
      )

      tv_shows = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(tv_shows).to eql(["Lost", "Sherlock", "The Wire"])
    end

    it "prioritizes a TV show with more no and watched votes from the people missing over the rest" do
      table = create_tv_show_group_table(
        "          | Fer | Ainho | Fido | Diego |",
        " The Wire | a   | w     | a    | a     | Fer",
        " Sherlock | a   | n     | n    | a     | Fer",
        " Lost     | a   | w     | w    | w     | Fer"
      )
      fer     = table[:users][:fer]
      subject = described_class.new(
        current_user: fer,
        user_ids: [fer.id],
        providers: Picking::ProviderFilter.all,
        country: "en-US"
      )

      tv_shows = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(tv_shows).to eql(["Lost", "Sherlock", "The Wire"])
    end

    it "on equal priority from votes, it orders TV shows with better ratings first" do
      table = create_tv_show_group_table(
        "          | Fer |",
        " The Wire | y   | Fer",
        " Sherlock | y   | Fer",
        " Lost     | y   | Fer"
      )
      table[:tv_shows][:the_wire].tmdb_profile.update!(rating: 5.0)
      table[:tv_shows][:sherlock].tmdb_profile.update!(rating: 7.0)
      table[:tv_shows][:lost].tmdb_profile.update!(rating: 9.0)
      subject = described_class.new(
        current_user: table[:users][:fer],
        user_ids: [table[:users][:fer].id],
        providers: Picking::ProviderFilter.all,
        country: "en-US"
      )

      tv_shows = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(tv_shows).to eql(["Lost", "Sherlock", "The Wire"])
    end

    it "filters the TV shows by provider and country" do
      table = create_tv_show_group_table(
        "          | Fer |",
        " The Wire | y   | Fer",
        " Lost     | y   | Fer",
        " Sherlock | y   | Fer"
      )
      netflix = FactoryGirl.create(:provider, :netflix)
      youtube = FactoryGirl.create(:provider, :youtube)
      FactoryGirl.create(:offer, watchable: table[:tv_shows][:lost], provider: netflix, country: "en-US")
      FactoryGirl.create(:offer, watchable: table[:tv_shows][:the_wire], provider: youtube, country: "en-US")
      FactoryGirl.create(:offer, watchable: table[:tv_shows][:sherlock], provider: netflix, country: "es-ES")
      subject = described_class.new(
        current_user: table[:users][:fer],
        user_ids: [table[:users][:fer].id],
        providers: Picking::ProviderFilter.some(ids: [netflix.id]),
        country: "en-US"
      )

      tv_shows = subject.as_json.fetch(:watchables).map { |tv_show| tv_show[:title] }

      expect(tv_shows).to eql(["Lost"])
    end
  end
end
