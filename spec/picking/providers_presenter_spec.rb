require "rails_helper"

RSpec.describe Picking::ProvidersPresenter do
  describe ".as_json" do
    it "renders all the available providers for the user's country" do
      netflix = FactoryGirl.build_stubbed(
        :provider,
        id: 1,
        icon_url: "http://justwatch.com/netflix.jpg",
        clear_name: "Netflix"
      )
      youtube = FactoryGirl.build_stubbed(
        :provider,
        id: 3,
        icon_url: "http://justwatch.com/youtube.jpg",
        clear_name: "YouTube"
      )

      expect(described_class.as_json(providers: [netflix, youtube])).to eq([
        {
          id: 1,
          icon_url: "http://justwatch.com/netflix.jpg",
          name: "Netflix",
        },
        {
          id: 3,
          icon_url: "http://justwatch.com/youtube.jpg",
          name: "YouTube",
        },
      ])
    end
  end
end
