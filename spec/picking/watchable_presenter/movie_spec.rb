require "rails_helper"

RSpec.describe Picking::WatchablePresenter::Movie do
  subject { described_class.new(slug: up.slug, current_user: fer, country: "en-US") }

  let!(:table) do
    create_group_table(
      "      | Fer | Ainho |",
      " Up   | y   | w     | Ainho "
    )
  end

  let(:ainho) { table[:users][:ainho] }
  let(:fer)   { table[:users][:fer] }
  let(:up)    { table[:movies][:up] }
  let(:group) { table[:group] }

  before do
    up.tmdb_profile.update!(genres: ["Family"])
    nomination = MovieNomination.find_by(group:, movie: up)
    FactoryGirl.create(:comment, nomination:, author: fer)
    FactoryGirl.create(:comment, nomination:, author: ainho)
    netflix = FactoryGirl.create(:provider, :netflix, icon_url: "/netflix.png")
    youtube = FactoryGirl.create(:provider, :youtube, icon_url: "/youtube.png")
    FactoryGirl.create(:offer, watchable: up, provider: netflix, country: "en-US",
                       monetization_type: "flatrate", presentation_type: "hd", price: Money.zero, standard_web_url: "https://netflix.com/up")
    # The country on the following two is different
    FactoryGirl.create(:offer, watchable: up, provider: youtube, country: "en-US",
                       monetization_type: "rent", presentation_type: "4k", price: Money.new(399, "USD"), standard_web_url: "https://youtube.com/up")
    FactoryGirl.create(:offer, watchable: up, provider: youtube, country: "es-ES",
                       monetization_type: "rent", presentation_type: "4k", price: Money.new(399, "USD"), standard_web_url: "https://youtube.com/up")
  end

  describe "#as_json" do
    it "returns a hash with info about the movie, its offers for the given country, the votes and the comments on this group" do
      expect(subject.as_json).to match(
        slug: up.slug,
        contributor: "Ainho",
        currently_in_library: true,
        providers: a_collection_containing_exactly(
          {
            name: "Netflix",
            icon_url: "https://images.justwatch.com/netflix.png",
            offers: {
              flatrate: [{
                presentation_type: "HD",
                price: Money.zero,
                url: "https://netflix.com/up",
              }],
              rent: [],
              buy: [],
              ads: [],
              cinema: [],
              free: [],
            },
          },
          name: "YouTube",
          icon_url: "https://images.justwatch.com/youtube.png",
          offers: {
            flatrate: [],
            rent: [
              presentation_type: "4K",
              price: Money.new(399, "USD"),
              url: "https://youtube.com/up",
            ],
            buy: [],
            ads: [],
            cinema: [],
            free: [],
          }
        ),
        minutes: 96,
        plot: nil,
        genres: ["Family"],
        trailer_youtube_id: String,
        poster: "https://images-na.ssl-images-amazon.com/images/M/up.jpg",
        rating: a_value_between(0, 10),
        title: "Up",
        url_imdb: "http://www.imdb.com/title/tt1049413",
        url_tmdb: "http://www.themoviedb.org/movie/14160",
        votes: a_collection_containing_exactly(
          {user_id: fer.id, user_name: "Fer", user_pic: String, opinion: "yes"},
          user_id: ainho.id, user_name: "Ainho", user_pic: String, opinion: "watched"
        ),
        current_user_vote: "yes",
        comments: a_collection_containing_exactly(
          {author_name: "Fer", author_pic: String, timestamp: Time, body: String},
          author_name: "Ainho", author_pic: String, timestamp: Time, body: String
        ),
        year: 2009
      )
    end

    it "might not include a `year` key if the movies does not have a release_date" do
      up.tmdb_profile.update!(release_date: nil)

      expect(subject.as_json).to_not have_key(:year)
    end

    it "does not incur in N+1 queries and queries models efficiently" do
      query_tracker = Support::QueryTracker.new

      query_tracker.during { subject.as_json }

      expect(query_tracker.models_queried).to eql(
        Comment: 1,
        Movie: 1,
        TmdbProfile: 1,
        MovieNomination: 1,
        User: 3,
        Vote: 1
      )
    end

    it "does not contain info about the users' votes when the movie is not in the group's library" do
      other_group = FactoryGirl.create(:group)
      other_user = FactoryGirl.create(:user, group: other_group)
      subject = described_class.new(slug: up.slug, current_user: other_user, country: "en-US")

      expect(subject.as_json.keys).to_not include(:votes, :comments)
    end

    it "does not contain info about other groups' comments" do
      other_table = create_group_table(
        "      | Diego |",
        " Up   | y     |  Diego"
      )
      subject = described_class.new(slug: other_table[:movies][:up].slug, current_user: other_table[:users][:diego], country: "en-US")

      expect(subject.as_json.fetch(:comments)).to be_empty
    end
  end
end
