require "rails_helper"

RSpec.describe Picking::WatchablePresenter::TvShow do
  subject { described_class.new(slug: the_wire.slug, current_user: fer, country: "en-US") }

  let!(:table) do
    create_tv_show_group_table(
      "          | Fer | Ainho |",
      " The Wire | y   | w     | Ainho "
    )
  end

  let(:ainho)    { table[:users][:ainho] }
  let(:fer)      { table[:users][:fer] }
  let(:the_wire) { table[:tv_shows][:the_wire] }
  let(:group)    { table[:group] }

  before do
    the_wire.tmdb_profile.update!(genres: ["Thriller", "Drama"])
    nomination = TvShowNomination.find_by(group:, tv_show: the_wire)
    nomination.update!(
      last_season_watched: 1,
      last_episode_watched: 10
    )
    FactoryGirl.create(:comment, nomination:, author: fer)
    FactoryGirl.create(:comment, nomination:, author: ainho)
    netflix = FactoryGirl.create(:provider, :netflix, icon_url: "/netflix.png")
    youtube = FactoryGirl.create(:provider, :youtube, icon_url: "/youtube.png")
    FactoryGirl.create(:offer, watchable: the_wire, provider: netflix, country: "en-US",
                       monetization_type: "flatrate", presentation_type: "hd", price: Money.zero, standard_web_url: "https://netflix.com/the_wire")
    FactoryGirl.create(:offer, watchable: the_wire, provider: youtube, country: "en-US",
                       monetization_type: "rent", presentation_type: "4k", price: Money.new(399, "USD"), standard_web_url: "https://youtube.com/the_wire")
    FactoryGirl.create(:offer, watchable: the_wire, provider: youtube, country: "es-ES",
                       monetization_type: "rent", presentation_type: "4k", price: Money.new(399, "USD"), standard_web_url: "https://youtube.com/the_wire")
    FactoryGirl.create(
      :season,
      tmdb_tv_show_profile: the_wire.tmdb_profile,
      name: "Season 2",
      season_number: 2,
      episode_count: 13,
      air_date: Date.new(2003, 5, 11),
      poster_url: "http://themoviedb.org/the_wire2.png"
    )
    FactoryGirl.create(
      :season,
      tmdb_tv_show_profile: the_wire.tmdb_profile,
      name: "Season 1",
      season_number: 1,
      episode_count: 10,
      air_date: Date.new(2002, 5, 11),
      poster_url: "http://themoviedb.org/the_wire.png"
    )
  end

  describe "#as_json" do
    it "returns a hash with info about the TV show, its offers for the given country, the votes and the comments on this group" do
      expect(subject.as_json).to match(
        slug: the_wire.slug,
        contributor: "Ainho",
        currently_in_library: true,
        last_season_watched: 1,
        last_episode_watched: 10,
        seasons: [
          {
            name: "Season 1",
            number: 1,
            episode_count: 10,
            air_date: Date.new(2002, 5, 11),
            poster_url: "http://themoviedb.org/the_wire.png",
          },
          {
            name: "Season 2",
            number: 2,
            episode_count: 13,
            air_date: Date.new(2003, 5, 11),
            poster_url: "http://themoviedb.org/the_wire2.png",
          },
        ],
        providers: a_collection_containing_exactly(
          {
            name: "Netflix",
            icon_url: "https://images.justwatch.com/netflix.png",
            offers: {
              flatrate: [{
                presentation_type: "HD",
                price: Money.zero,
                url: "https://netflix.com/the_wire",
              }],
              rent: [],
              buy: [],
              ads: [],
              cinema: [],
              free: [],
            },
          },
          name: "YouTube",
          icon_url: "https://images.justwatch.com/youtube.png",
          offers: {
            flatrate: [],
            rent: [
              presentation_type: "4K",
              price: Money.new(399, "USD"),
              url: "https://youtube.com/the_wire",
            ],
            buy: [],
            ads: [],
            cinema: [],
            free: [],
          }
        ),
        minutes: 60,
        plot: nil,
        genres: ["Thriller", "Drama"],
        poster: "https://images-na.ssl-images-amazon.com/images/M/The Wire.jpg",
        rating: a_value_between(0, 10),
        title: "The Wire",
        url_tmdb: "http://www.themoviedb.org/tv/1438",
        votes: a_collection_containing_exactly(
          {user_id: fer.id, user_name: "Fer", user_pic: String, opinion: "yes"},
          user_id: ainho.id, user_name: "Ainho", user_pic: String, opinion: "watched"
        ),
        current_user_vote: "yes",
        comments: a_collection_containing_exactly(
          {author_name: "Fer", author_pic: String, timestamp: Time, body: String},
          author_name: "Ainho", author_pic: String, timestamp: Time, body: String
        ),
        year: 2002
      )
    end

    it "might not include a `year` key if the TV shows does not have a release_date" do
      the_wire.tmdb_profile.update!(release_date: nil)

      expect(subject.as_json).to_not have_key(:year)
    end

    it "does not incur in N+1 queries and queries models efficiently" do
      query_tracker = Support::QueryTracker.new

      query_tracker.during { subject.as_json }

      expect(query_tracker.models_queried).to eql(
        Comment: 1,
        Season: 1,
        TvShow: 1,
        TmdbTvShowProfile: 1,
        TvShowNomination: 1,
        User: 3,
        Vote: 1
      )
    end

    it "does not contain info about the users' votes when the TV show is not in the group's library" do
      other_group = FactoryGirl.create(:group)
      other_user = FactoryGirl.create(:user, group: other_group)
      subject = described_class.new(slug: the_wire.slug, current_user: other_user, country: "en-US")

      expect(subject.as_json.keys).to_not include(:votes, :comments)
    end

    it "does not contain info about other groups' comments" do
      other_table = create_tv_show_group_table(
        "          | Diego |",
        " The Wire | y     |  Diego"
      )
      subject = described_class.new(slug: other_table[:tv_shows][:the_wire].slug, current_user: other_table[:users][:diego], country: "en-US")

      expect(subject.as_json.fetch(:comments)).to be_empty
    end
  end
end
