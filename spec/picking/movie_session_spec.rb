require "rails_helper"

RSpec.describe Picking::MovieSession do
  describe "#as_json" do
    it "follows the Code of the MovieMaters to pick a list of movies to watch" do
      # Code of the MovieMaters:
      # * You shall not watch a movie that any movie mate hasn't voted
      # * You shall watch a movie that all present movie mates are willing to watch
      # * You shall watch a movie that no absent movie mate wants to watch
      # * You shall try to watch the movie that present movie mates want to watch the most
      table = create_group_table(
        "           | Fer | Ainho | Fido |",
        " Heat      | y   | a     | a    | Fer",
        " Snatch    | y   | y     | w    | Fer",
        " Up        | y   | y     | u    | Fer",
        " Gladiator | y   | y     | y    | Fer",
        " Amelie    | y   | w     | n    | Fer",
        " Alien     | u   | y     | n    | Fer"
      )
      fer     = table[:users][:fer]
      ainho   = table[:users][:ainho]
      subject = described_class.new(
        current_user: fer,
        user_ids: [fer.id, ainho.id],
        providers: Picking::ProviderFilter.all,
        country: "en-US"
      )

      movies = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(movies).to eql(["Snatch", "Heat"])
    end

    it "isolates groups from each other, even when groups have the same amount of users" do
      create_group_table(
        "      | Fer |",
        " Heat | y   | Fer"
      )
      other_group = FactoryGirl.create(:group)
      other_user  = FactoryGirl.create(:user, group: other_group)
      subject = described_class.new(
        current_user: other_user,
        user_ids: [other_user.id],
        providers: Picking::ProviderFilter.all,
        country: "en-US"
      )

      expect(subject.as_json.fetch(:watchables)).to be_empty
    end

    it "prioritizes a movie with more yes votes from the people present over the rest" do
      table = create_group_table(
        "        | Fer | Ainho | Fido |",
        " Heat   | y   | a     | a    | Fer",
        " Snatch | y   | y     | a    | Fer",
        " Up     | y   | y     | y    | Fer"
      )
      fer     = table[:users][:fer]
      ainho   = table[:users][:ainho]
      fido    = table[:users][:fido]
      subject = described_class.new(
        current_user: fer,
        user_ids: [fer.id, ainho.id, fido.id],
        providers: Picking::ProviderFilter.all,
        country: "en-US"
      )

      movies = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(movies).to eql(["Up", "Snatch", "Heat"])
    end

    it "prioritizes a movie with more no and watched votes from the people missing over the rest" do
      table = create_group_table(
        "           | Fer | Ainho | Fido | Diego |",
        " Heat      | a   | w     | a    | a     | Fer",
        " Snatch    | a   | n     | n    | a     | Fer",
        " Up        | a   | w     | w    | w     | Fer"
      )
      fer     = table[:users][:fer]
      subject = described_class.new(
        current_user: fer,
        user_ids: [fer.id],
        providers: Picking::ProviderFilter.all,
        country: "en-US"
      )

      movies = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(movies).to eql(["Up", "Snatch", "Heat"])
    end

    it "on equal priority from votes, it orders movies with better ratings first" do
      table = create_group_table(
        "        | Fer |",
        " Heat   | y   | Fer",
        " Snatch | y   | Fer",
        " Up     | y   | Fer"
      )
      table[:movies][:heat].tmdb_profile.update!(rating: 5.0)
      table[:movies][:snatch].tmdb_profile.update!(rating: 7.0)
      table[:movies][:up].tmdb_profile.update!(rating: 9.0)
      subject = described_class.new(
        current_user: table[:users][:fer],
        user_ids: [table[:users][:fer].id],
        providers: Picking::ProviderFilter.all,
        country: "en-US"
      )

      movies = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(movies).to eql(["Up", "Snatch", "Heat"])
    end

    it "filters the movies by provider and country" do
      table = create_group_table(
        "        | Fer |",
        " Heat   | y   | Fer",
        " Up     | y   | Fer",
        " Snatch | y   | Fer"
      )
      netflix = FactoryGirl.create(:provider, :netflix)
      youtube = FactoryGirl.create(:provider, :youtube)
      FactoryGirl.create(:offer, watchable: table[:movies][:up], provider: netflix, country: "en-US")
      FactoryGirl.create(:offer, watchable: table[:movies][:heat], provider: youtube, country: "en-US")
      FactoryGirl.create(:offer, watchable: table[:movies][:snatch], provider: netflix, country: "es-ES")
      subject = described_class.new(
        current_user: table[:users][:fer],
        user_ids: [table[:users][:fer].id],
        providers: Picking::ProviderFilter.some(ids: [netflix.id]),
        country: "en-US"
      )

      movies = subject.as_json.fetch(:watchables).map { |movie| movie[:title] }

      expect(movies).to eql(["Up"])
    end
  end
end
