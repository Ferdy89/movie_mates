require "rails_helper"

RSpec.describe Picking::UpdateLastWatched do
  describe ".update_last_watched" do
    it "updates the last season and episode watched for a given show" do
      table = create_tv_show_group_table(
        "          | Fer | ",
        " The Wire | y   |  Fer "
      )
      the_wire = table[:tv_shows][:the_wire]
      nomination = the_wire.nominations.first

      described_class.update_last_watched(
        group: table[:users][:fer].group,
        slug: the_wire.slug,
        last_season_watched: 5,
        last_episode_watched: 13
      )

      nomination.reload
      expect(nomination.last_season_watched).to be(5)
      expect(nomination.last_episode_watched).to be(13)
    end
  end
end
