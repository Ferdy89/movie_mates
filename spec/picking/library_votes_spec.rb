require "rails_helper"

RSpec.describe Picking::LibraryVotes do
  describe ".mark_watched" do
    it "adds watched votes to the specified users when the movie was not voted" do
      table = create_group_table(
        "    | Fer | Ainho |",
        " Up | u   | u     | Fer"
      )
      subject = described_class.new(group: table[:group])
      fer = table[:users][:fer]
      ainho = table[:users][:ainho]

      subject.mark_watched(slug: table[:movies][:up].slug, watched_user_ids: [fer.id, ainho.id])

      expect(fer.votes).to contain_exactly(an_object_having_attributes(opinion: "watched"))
      expect(ainho.votes).to contain_exactly(an_object_having_attributes(opinion: "watched"))
    end

    it "updates users' votes when the movie was voted already" do
      table = create_group_table(
        "    | Fer | Ainho |",
        " Up | y   | a     | Fer"
      )
      subject = described_class.new(group: table[:group])
      fer = table[:users][:fer]
      ainho = table[:users][:ainho]

      subject.mark_watched(slug: table[:movies][:up].slug, watched_user_ids: [fer.id])

      expect(fer.votes).to contain_exactly(an_object_having_attributes(opinion: "watched"))
      expect(ainho.votes).to contain_exactly(an_object_having_attributes(opinion: "abstention"))
    end

    it "deletes watched votes when a user had already watched the movie" do
      table = create_group_table(
        "    | Fer | Ainho |",
        " Up | w   | y     | Fer"
      )
      subject = described_class.new(group: table[:group])
      fer = table[:users][:fer]
      ainho = table[:users][:ainho]

      subject.mark_watched(slug: table[:movies][:up].slug, watched_user_ids: [])

      expect(fer.votes.exists?).to be(false)
      expect(ainho.votes).to contain_exactly(an_object_having_attributes(opinion: "yes"))
    end

    it "ignores user IDs that don't belong to the group" do
      table = create_group_table(
        "    | Fer |",
        " Up | y   | Fer"
      )
      subject = described_class.new(group: table[:group])
      another_group = FactoryGirl.create(:group)
      ainho = FactoryGirl.create(:user, :ainho, group: another_group)

      subject.mark_watched(slug: table[:movies][:up].slug, watched_user_ids: [ainho.id])

      expect(ainho.votes.exists?).to be(false)
    end
  end
end
