require "rails_helper"

RSpec.describe MovieRefresher do
  it "refreshes the information of a single movie" do
    movie = FactoryGirl.create(:movie)
    refresher = instance_double(MovieMates::Import::MovieRefresher)
    allow(MovieMates::Import::MovieRefresher).to receive(:new).with(movie:).and_return(refresher)
    allow(refresher).to receive(:refresh)

    described_class.perform_async(movie.id)
    described_class.drain

    expect(refresher).to have_received(:refresh)
  end
end
