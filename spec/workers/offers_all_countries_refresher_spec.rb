require "rails_helper"

RSpec.describe OffersAllCountriesRefresher do
  it "enqueues one OffersRefresher per country" do
    described_class.perform_async("Movie", 1)
    described_class.drain

    expected_jobs = [
      a_hash_including("class" => "OffersRefresher", "args" => ["Movie", 1, "en-US"]),
      a_hash_including("class" => "OffersRefresher", "args" => ["Movie", 1, "es-ES"]),
    ]
    expect(OffersRefresher.jobs).to match_array(expected_jobs)
  end
end
