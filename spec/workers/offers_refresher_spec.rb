require "rails_helper"

RSpec.describe OffersRefresher do
  it "refreshes the information of a single movie" do
    movie = FactoryGirl.create(:movie)
    country = "en-US"
    refresher = instance_double(MovieMates::Import::OffersRefresher)
    allow(MovieMates::Import::OffersRefresher).
      to receive(:for).with(watchable: movie, country:).and_return(refresher)
    allow(refresher).to receive(:refresh)

    described_class.perform_async("Movie", movie.id, country)
    described_class.drain

    expect(refresher).to have_received(:refresh)
  end

  it "refreshes the information of a single TV show" do
    tv_show = FactoryGirl.create(:tv_show)
    country = "en-US"
    refresher = instance_double(MovieMates::Import::OffersRefresher)
    allow(MovieMates::Import::OffersRefresher).
      to receive(:for).with(watchable: tv_show, country:).and_return(refresher)
    allow(refresher).to receive(:refresh)

    described_class.perform_async("TvShow", tv_show.id, country)
    described_class.drain

    expect(refresher).to have_received(:refresh)
  end
end
