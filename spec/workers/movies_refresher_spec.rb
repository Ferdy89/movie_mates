require "rails_helper"

RSpec.describe MoviesRefresher do
  it "enqueues all movies to be refreshed" do
    movies = FactoryGirl.create_list(:movie, 3)

    described_class.perform_async
    described_class.drain

    expected_jobs = movies.map do |movie|
      a_hash_including("class" => "MovieRefresher", "args" => [movie.id])
    end
    expect(MovieRefresher.jobs).to match_array(expected_jobs)

    expected_jobs = movies.map do |movie|
      a_hash_including(
        "class" => "OffersAllCountriesRefresher",
        "args" => ["Movie", movie.id]
      )
    end
    expect(OffersAllCountriesRefresher.jobs).to match_array(expected_jobs)
  end
end
