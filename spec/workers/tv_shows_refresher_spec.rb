require "rails_helper"

RSpec.describe TvShowsRefresher do
  it "enqueues all TV shows to be refreshed" do
    tv_shows = FactoryGirl.create_list(:tv_show, 3)

    described_class.perform_async
    described_class.drain

    expected_jobs = tv_shows.map do |tv_show|
      a_hash_including("class" => "TvShowRefresher", "args" => [tv_show.id])
    end
    expect(TvShowRefresher.jobs).to match_array(expected_jobs)

    expected_jobs = tv_shows.map do |tv_show|
      a_hash_including(
        "class" => "OffersAllCountriesRefresher",
        "args" => ["TvShow", tv_show.id]
      )
    end
    expect(OffersAllCountriesRefresher.jobs).to match_array(expected_jobs)
  end
end
