require "rails_helper"

RSpec.describe MoviePurger do
  it "removes a movie and all its associated models" do
    table = create_group_table(
      "    | Fer | Ainho |",
      " Up | u   | u     | Fer"
    )
    movie = table[:movies][:up]
    tmdb_profile = movie.tmdb_profile
    nomination = movie.nominations.first
    votes = nomination.votes
    comment = FactoryGirl.create(:comment, nomination:, author: table[:users][:fer])

    described_class.perform_async(movie.id)
    described_class.drain

    expect(Movie.find_by(id: movie.id)).to be(nil)
    expect(TmdbProfile.find_by(id: tmdb_profile.id)).to be(nil)
    expect(MovieNomination.find_by(id: nomination.id)).to be(nil)
    votes.each { |vote| expect(Votes.find_by(id: vote.id)).to be(nil) }
    expect(Comment.find_by(id: comment.id)).to be(nil)
  end
end
