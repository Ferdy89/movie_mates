require "rails_helper"

RSpec.describe TvShowRefresher do
  it "refreshes the information of a single TV show" do
    tv_show = FactoryGirl.create(:tv_show)
    refresher = instance_double(MovieMates::Import::TvShowRefresher)
    allow(MovieMates::Import::TvShowRefresher).
      to receive(:new).with(tv_show:).and_return(refresher)
    allow(refresher).to receive(:refresh)

    described_class.perform_async(tv_show.id)
    described_class.drain

    expect(refresher).to have_received(:refresh)
  end
end
