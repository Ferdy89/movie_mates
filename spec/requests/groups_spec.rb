require "rails_helper"

RSpec.describe "/groups" do
  describe "sending invitations" do
    it "does not send invitations for things that are not emails" do
      group = FactoryGirl.create(:group)
      fer   = FactoryGirl.create(:user, group:)

      sign_in(fer)

      expect { post my_group_invitations_path(emails: "foo@bar@example.com") }.to_not(change { group.group_invitations })
    end
  end

  describe "inviting an existing MovieMate to a group" do
    it "moves the user, their nominations and votes to the new group" do
      first_table = create_group_table(
        "         | Fer |",
        " Up      | y   | Fer"
      )
      first_group = first_table[:group]
      up_movie = first_table[:movies][:up]

      second_table = create_group_table(
        "    | Ainho |",
        " Up | y     | Ainho"
      )

      fer   = first_table[:users][:fer]
      ainho = second_table[:users][:ainho]

      sign_in(fer)

      post my_group_invitations_path(emails: "ainho@gmail.com")
      invitation_token = first_group.group_invitations.last.token

      sign_out(fer)
      sign_in(ainho)

      get commit_my_group_invitations_path(token: invitation_token)

      expect(ainho.group).to eql(first_group)
      new_nomination = MovieNomination.find_by(group: first_group, movie: up_movie)
      expect(new_nomination.votes.count).to be(2)
    end

    it "accepts the invitation after signing in when the user is not signed in" do
      first_group  = FactoryGirl.create(:group)
      fer          = FactoryGirl.create(:user, group: first_group)
      second_group = FactoryGirl.create(:group)
      ainho        = FactoryGirl.create(:user, group: second_group, provider: "google_oauth2", uid: "123545")

      sign_in(fer)

      post my_group_invitations_path(emails: "ainho@gmail.com")
      invitation_token = first_group.group_invitations.last.token

      sign_out(fer)

      get commit_my_group_invitations_path(token: invitation_token)
      post user_google_oauth2_omniauth_authorize_path
      follow_redirect!

      expect(ainho.reload.group).to eql(first_group)
    end
  end
end
