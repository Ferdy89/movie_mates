require "rails_helper"

RSpec.describe Group do
  describe "#implode" do
    it "destroys all the associated nominations and users, then destroys itself" do
      table = create_group_table(
        "    | Fer | ",
        " Up | y   | Fer"
      )
      group = table[:group]

      expect { group.implode }.
        to change { MovieNomination.count }.by(-1).
        and change { User.count }.by(-1).
        and change { Group.count }.by(-1)
    end
  end
end
