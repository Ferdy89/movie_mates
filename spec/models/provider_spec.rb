require "rails_helper"

RSpec.describe Provider do
  describe ".with_offers" do
    it "loads only providers with offers for a particular movie and country, sorted by provider id" do
      up = FactoryGirl.create(:movie, :up)
      alien = FactoryGirl.create(:movie, :alien)
      netflix = FactoryGirl.create(:provider, :netflix, provider_id: 1)
      amazon_prime = FactoryGirl.create(:provider, :amazon_prime)
      youtube = FactoryGirl.create(:provider, :youtube, provider_id: 3)
      FactoryGirl.create(:offer, watchable: up, provider: netflix, country: "en-US")
      FactoryGirl.create(:offer, watchable: up, provider: youtube, country: "en-US")
      FactoryGirl.create(:offer, watchable: up, provider: amazon_prime, country: "es-ES")
      FactoryGirl.create(:offer, watchable: alien, provider: amazon_prime, country: "en-US")

      providers = described_class.with_offers(watchable: up, country: "en-US")

      expect(providers).to eq([netflix, youtube])
    end

    it "only loads the offers for the movie" do
      up = FactoryGirl.create(:movie, :up)
      alien = FactoryGirl.create(:movie, :alien)
      youtube = FactoryGirl.create(:provider, :youtube)
      up_offer = FactoryGirl.create(:offer, watchable: up, provider: youtube, country: "en-US")
      FactoryGirl.create(:offer, watchable: alien, provider: youtube, country: "en-US")

      provider = described_class.with_offers(watchable: up, country: "en-US").first # YouTube

      expect(provider.offers).to eq([up_offer])
    end
  end

  describe ".for_country" do
    it "loads providers that have offers for the given country, sorted by provider id" do
      up = FactoryGirl.create(:movie, :up)
      alien = FactoryGirl.create(:movie, :alien)
      heat = FactoryGirl.create(:movie, :heat)
      netflix = FactoryGirl.create(:provider, :netflix, provider_id: 1)
      amazon_prime = FactoryGirl.create(:provider, :amazon_prime)
      youtube = FactoryGirl.create(:provider, :youtube, provider_id: 3)
      FactoryGirl.create(:offer, watchable: up, provider: netflix, country: "es-ES")
      FactoryGirl.create(:offer, watchable: alien, provider: netflix, country: "es-ES")
      FactoryGirl.create(:offer, watchable: heat, provider: youtube, country: "es-ES")
      FactoryGirl.create(:offer, watchable: up, provider: amazon_prime, country: "en-US")

      providers = described_class.for_country("es-ES")

      expect(providers).to eq([netflix, youtube])
    end
  end
end
