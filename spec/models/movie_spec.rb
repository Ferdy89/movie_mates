require "rails_helper"

RSpec.describe Movie do
  describe ".not_voted_by" do
    let!(:table) do
      create_group_table(
        "       | Fer | Ainho |",
        " Alien | y   | u     | Fer",
        " Up    | u   | y     | Fer"
      )
    end

    it "finds movies not voted by the user" do
      expect(described_class.not_voted_by(table[:users][:fer])).to match_array([table[:movies][:up]])
    end
  end

  describe ".for_tmdb_id" do
    it "finds the movie for a given TMDb movie ID" do
      movie = FactoryGirl.create(:movie)
      FactoryGirl.create(:tmdb_profile, tmdb_id: 123, movie:)

      expect(described_class.for_tmdb_id(123)).to eql(movie)
    end
  end
end
