require "rails_helper"

RSpec.describe TvShow do
  describe ".not_voted_by" do
    let!(:table) do
      create_tv_show_group_table(
        "          | Fer | Ainho |",
        " The Wire | y   | u     | Fer",
        " Sherlock | u   | y     | Fer"
      )
    end

    it "finds TV shows not voted by the user" do
      expect(described_class.not_voted_by(table[:users][:fer])).to match_array([table[:tv_shows][:sherlock]])
    end
  end
end
