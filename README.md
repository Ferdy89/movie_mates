# MovieMates [![build status](https://gitlab.com/ferdynton/movie_mates/badges/master/pipeline.svg)](https://gitlab.com/ferdynton/movie_mates/commits/master)![coverage](https://gitlab.com/ferdynton/movie_mates/badges/master/coverage.svg)

[MovieMates](https://moviemates.party) is a website that helps you and your
friends keep track of which movies you want to watch, then helps you determine
which one to watch with any of your friends.

[MovieMates](https://moviemates.party) is free, open source and has no ads. We
seek no retribution other than having fun building a service that we enjoy
using. We offer login with social media for convenience but we make no other use
of your data.

## First time with the source code

MovieMates is built with Ruby on Rails, tested in GitLab CI and hosted in a
Raspberry Pi (check out the configuration files in the `/deploy` directory). We
pull movie information from [TMDb](https://themoviedb.org) and we send emails
using Mailgun's free tier.

To run MovieMates locally, first clone the repo:

```sh
git clone git@gitlab.com:ferdynton/movie_mates.git
cd movie_mates
```

Now you will need to have Ruby 3.3.0 installed, Bundler, a modern version of
NodeJS, a recent version of PostgreSQL, and Redis:

```sh
bin/bundle install
bin/rails db:setup
```

Now, in order to be able to use MovieMates with all its features, you will need
a `THE_MOVIE_DATABASE_API_KEY`. You can create an account with them and then get
it from [here](https://www.themoviedb.org/settings/api). Once you have it, save
it on a `.env.shared` file like:

```txt
THE_MOVIE_DATABASE_API_KEY=<your_key_here>
```

You're finally ready to run your local server or run the test suite:

```sh
bin/rails server # => Open the site on http://localhost:3000

bin/rspec # Run the test suite
```

### Test

Running the tests is as easy as running

```sh
bin/rspec
```

#### CI

In GitLab, this runs automatically as part of a pipeline before and after
accepting any Merge Request. This happens inside a Docker container.

The `.gitlab-ci.yml` file indicates the Docker image to run these tests in.
This image can be rebuilt and pushed using the `bin/push-test-runner` command
after making any desired modifications to the `Dockerfile`. Once pushed, you
should be able to see the image at
https://gitlab.com/ferdynton/movie_mates/container_registry.

### Deployment

I wrote how to deploy MovieMates to a Raspberry Pi in [a blog
post](https://ferdynton.github.io/post/heroku-to-raspbery-pi/).

### Ways to help

Whether you code or not, there are many ways to help with MovieMates:

* [Tell me](https://twitter.com/Ferdy89) what you think! Is there anything you
  would love to see in the site?
* [Report](https://gitlab.com/Ferdy89/movie_mates/issues/new) any bugs or
  unexpected behavior with the site.
* Code with us! Take a look at the
  [Board](https://gitlab.com/Ferdy89/movie_mates/boards) to get an idea of what
  we're focusing on. Ask on any task you find interesting and go for the win!

### Code of Conduct

Please note that this project is released with a Contributor Code of Conduct.
By participating in this project you agree to abide by [its
terms](CODE_OF_CONDUCT.md).

## License

MovieMates as an MIT license, which means go have fun with its source code!
