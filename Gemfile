# frozen_string_literal: true

ruby   "3.3.0"
source "https://rubygems.org"

# MVC web-application framework and its translations
gem "rails", "~> 7.1.4"
gem "rails-i18n"
# Boot large Ruby/Rails apps faster
gem "bootsnap", require: false

# Explicitly required on version 1.11 and above to be protected from
# CVE-2020-26247
gem "nokogiri", "~> 1.11", require: false
gem "pg"
# Use Puma as the app server
gem "puma", "~> 6"
# Use SCSS for stylesheets
gem "sass-rails", "~> 6"
# Use Uglifier as compressor for JavaScript assets
gem "uglifier", ">= 1.3.0"

# Haml is a templating engine for HTML. It's designed to make it both easier and
# more pleasant to write HTML documents, by eliminating redundancy, reflecting
# the underlying structure that the document represents, and providing an
# elegant syntax that's both powerful and easy to understand.
gem "haml", "~> 5.0"

# Use jquery as the JavaScript library. Version 4.3.4 fixes vulnerability
# CVE-2019-11358.
gem "jquery-rails", "~> 4", ">= 4.3.4"
# Turbolinks makes navigating your web application faster. Read more:
# https://github.com/turbolinks/turbolinks
gem "turbolinks", "~> 5"

# Flexible authentication solution for Rails. Version 4.7.1 is the first one to
# address vulnerability https://github.com/plataformatec/devise/issues/5071
gem "devise", "~> 4.7", ">= 4.7.1"
gem "devise-encryptable"
# Standardizes multi-provider authentication for web applications. Version 2 is
# the first one to address vulnerability CVE-2015-9284
gem "omniauth", "~> 2"
gem "omniauth-rails_csrf_protection", "~> 1"
# Strategy to authenticate with Google via OAuth2 in OmniAuth
gem "omniauth-google-oauth2"

# Wrapper for the The Movie Database API. Using my own fork until they merge
# https://github.com/ahmetabdi/themoviedb/pull/59, which might not really happen
# because the project doesn't look very active.
gem "themoviedb", git: "https://github.com/ferdynton/themoviedb", require: false

# Allows Rails to easily store Money types in the db.
gem "money-rails", "~> 1.13"

# Shows emoji-based flags for the language of the movie when searching.
gem "emoji_flag"

# Homemade API client for JustWatch, which allows MovieMates to fetch offers
# for movies.
gem "just_watch", path: "lib/just_watch", require: false

# Detects the user's language, as sent by the "Accept-Language" HTTP header.
gem "http_accept_language", "~> 2.1"

# Redis-based background job processor. Version 7 is the first one to allow
# using Rack 3
gem "sidekiq", "~> 7"
# A scheduling add-on for Sidekiq.
gem "sidekiq-cron", "~> 1.1"

group :production do
  # The official client and integration layer for the Sentry error reporting
  # API. Hooks up into Rails from early on, so it's required from the beginning
  # in production.
  gem "sentry-raven", "~> 2.7"

  # Provides with deep information about the performance of the application as
  # it runs in production and transmits it to newrelic.com to monitor in real
  # time. Hooks up into Rails from early on, so it's required from the beginning
  # in production.
  gem "newrelic_rpm"
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger
  # console
  gem "byebug", platform: :mri, require: false
  gem "pry", require: false

  # Loads environment variables from a .env file into ENV in development. Needs
  # to load before the application which is why it's required from the
  # beginning.
  gem "dotenv-rails"
end

group :development do
  gem "listen", "~> 3", require: false
end

group :test do
  # Provides with helpers for testing models, controllers and views
  gem "rspec-rails", "~> 6", require: false
  # Provides methods to traverse HTML documents and create tests for them.
  # Version 3 is the first one to work with Ruby 3
  gem "capybara", "~> 3", require: false
  # Captures a screenshot for each failure in the test suite
  gem "capybara-screenshot", require: false
  # Runs Selenium tests with automatic installation for all supported
  # webdrivers. This enables tests that use JavaScript on the client.
  gem "selenium-webdriver", "~> 4", require: false
  # Looks like it's required for Capybara tests to run
  gem "webrick"

  # Generates test coverage reports
  gem "simplecov", "~> 0.12", require: false
  # Dependency of simplecov. Version 2.3 is the first that patches vulnerability
  # CVE-2020-10663
  gem "json", "~> 2.3", require: false
  # Fixtures replacement with a straightforward definition syntax. Avoids
  # repetition in tests when persisting model instances.
  gem "factory_girl_rails", "~> 4.8", require: false

  # Record your test suite's HTTP interactions and replay them during future
  # test runs for fast, deterministic, accurate tests.
  gem "vcr", "~> 6", require: false
  # Library for stubbing and setting expectations on HTTP requests. 3.5.0 is the
  # first one supporting Ruby 2.6
  gem "webmock", "~> 3.5", require: false
end
