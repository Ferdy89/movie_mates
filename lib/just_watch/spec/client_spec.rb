# frozen_string_literal: true

RSpec.describe JustWatch::Client do
  describe "#search" do
    it "searches movies", vcr: {cassette_name: "search"} do
      movies = subject.search("matrix")
      matrix = movies.first

      expect(matrix.title).to eql("The Matrix")
      expect(matrix.imdb_id).to eql("tt0133093")
      expect(matrix.tmdb_id).to eql(603)
      expect(matrix.offers.map { |offer| offer.provider.id }).to include(192) # YouTube
      youtube_rent = matrix.offers.find do |offer|
        offer.provider.id == 192 &&
          offer.monetization_type == "rent" &&
          offer.presentation_type == "sd"
      end
      expect(youtube_rent.price).to eql(Money.new(3_99, "USD"))
      expect(youtube_rent.url).to eql("https://www.youtube.com/results?search_query=The+Matrix%2Bmovie")
      youtube = youtube_rent.provider
      expect(youtube.id).to eql(192)
      expect(youtube.clear_name).to eql("YouTube")
      expect(youtube.icon_url).to eql("/icon/59562423/{profile}/youtube.jpeg")
    end

    it "searches TV shows", vcr: {cassette_name: "search_tv_shows"} do
      shows = subject.search("the wire")
      the_wire = shows.first

      expect(the_wire.title).to eql("The Wire")
      expect(the_wire.imdb_id).to eql("tt0306414")
      expect(the_wire.tmdb_id).to eql(1438)
      expect(the_wire.offers.map { |offer| offer.provider.id }).to include(3) # Google Play
      gplay_buy = the_wire.offers.find do |offer|
        offer.provider.id == 3 &&
          offer.presentation_type == "sd"
      end
      expect(gplay_buy.monetization_type).to eql("buy")
      expect(gplay_buy.price).to eql(Money.new(99_96, "USD"))
      expect(gplay_buy.url).to eql(
        "https://play.google.com/store/tv/show?cdid=tvseason--kI75uoQY2g&gl=US&hl=en&id=xg6VT9Gc_sg"
      )
      gplay = gplay_buy.provider
      expect(gplay.id).to eql(3)
      expect(gplay.clear_name).to eql("Google Play Movies")
      expect(gplay.icon_url).to eql("/icon/169478387/{profile}/play.jpeg")
    end
  end
end
