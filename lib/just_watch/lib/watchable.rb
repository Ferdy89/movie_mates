# frozen_string_literal: true

require_relative "offers"

class JustWatch::Watchable
  def self.from_search_result(search_result)
    new(
      id: search_result.fetch("id"),
      title: search_result.fetch("content").fetch("title"),
      offers:
        JustWatch::Offers.from_api_results(search_result.fetch("offers", [])),
      imdb_id: search_result.fetch("content").fetch("externalIds").fetch("imdbId"),
      tmdb_id: search_result.fetch("content").fetch("externalIds").fetch("tmdbId").to_i
    )
  end

  attr_reader :id, :title, :offers, :imdb_id, :tmdb_id

  def initialize(id:, title:, offers:, imdb_id: nil, tmdb_id: nil)
    @id = id
    @title = title
    @offers = offers
    @imdb_id = imdb_id
    @tmdb_id = tmdb_id
  end
end
