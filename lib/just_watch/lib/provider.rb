# frozen_string_literal: true

class JustWatch::Provider
  def self.from_api_result(api_result)
    new(
      id: api_result.fetch("packageId"),
      clear_name: api_result.fetch("clearName"),
      icon_url: api_result.fetch("icon").gsub("{format}", "jpeg")
    )
  end

  attr_reader :id, :clear_name, :icon_url

  def initialize(id:, clear_name:, icon_url:)
    @id = id
    @clear_name = clear_name
    @icon_url = icon_url
  end
end
