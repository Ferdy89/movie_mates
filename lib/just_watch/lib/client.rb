# frozen_string_literal: true

require "faraday"
require "json"

require_relative "watchable"

# This uses the JustWatch GraphQL API just like their website does. Inspiration
# for this code comes from
# https://www.reddit.com/r/webscraping/comments/wacb8a/comment/ii3d9y1.
# Normally we'd use a GraphQL Schema to properly consume a GraphQL API, but this
# is an unofficial usage. Instead, we simply mimic the requests they use by looking at
# the Network tab in DevTools and copying/modifying the GraphQL requests we see
# there. While they have a lot of different requests and there's a lot of data
# we could scrape, here we only pull the data we need for MovieMates to operate.
class JustWatch::Client
  GRAPHQL_ENDPOINT = "https://apis.justwatch.com/graphql"
  RESULTS_PULLED = 100 # Arbitrary, most movies should work with a lot less

  def initialize(locale: "en_US")
    @locale = locale
  end

  def search(query)
    result = graph("GetSuggestedTitles", variables: {
      country:,
      language:,
      first: RESULTS_PULLED,
      filter: {
        searchQuery: query,
        includeTitlesWithoutUrl: true,
      },
    })

    result.fetch("popularTitles").fetch("edges").map do |search_result|
      JustWatch::Watchable.from_search_result(search_result.fetch("node"))
    end
  end

  private

  attr_reader :locale

  def graph(operation_name, variables: {})
    query = File.read(
      File.expand_path(
        "./queries/#{operation_name}.graphql",
        File.dirname(__FILE__)
      )
    )

    result = Faraday.post(
      GRAPHQL_ENDPOINT,
      JSON.dump({
        operationName: operation_name,
        variables:,
        query:,
      }),
      "Content-Type" => "application/json"
    )

    JSON.parse(result.body).fetch("data")
  end

  def country
    @country ||= locale.split("_").last
  end

  def language
    @language ||= locale.split("_").first
  end
end
