# frozen_string_literal: true

require "money"

require_relative "provider"

class JustWatch::Offer
  def self.from_api_result(api_result)
    new(
      provider: api_result.fetch("package"),
      monetization_type: api_result.fetch("monetizationType"),
      presentation_type: api_result.fetch("presentationType"),
      price: extract_price(api_result:),
      url: api_result.fetch("standardWebURL")
    )
  end

  def self.extract_price(api_result:)
    if api_result["retailPriceValue"]
      Money.new(
        api_result.fetch("retailPriceValue") * 100,
        api_result.fetch("currency")
      )
    end
  end
  private_class_method :extract_price

  attr_reader :provider, :monetization_type, :presentation_type, :price,
              :url

  def initialize(
    provider:,
    monetization_type:,
    presentation_type:,
    price:,
    url:
  )
    @provider = JustWatch::Provider.from_api_result(provider)
    @monetization_type = monetization_type.downcase
    @presentation_type = case presentation_type
                         when "_4K" then "4k"
                         else presentation_type.downcase
    end
    @price = price
    @url = url
  end
end
