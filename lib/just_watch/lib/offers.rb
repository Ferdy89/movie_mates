# frozen_string_literal: true

require_relative "offer"

class JustWatch::Offers
  include Enumerable

  def self.from_api_results(api_results)
    if api_results
      new(
        offers: api_results.map do |api_result|
          JustWatch::Offer.from_api_result(api_result)
        end
      )
    else
      []
    end
  end

  def initialize(offers:)
    @offers = offers
  end

  def each(&block)
    offers.each(&block)
  end

  private

  attr_reader :offers
end
