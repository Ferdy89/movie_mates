# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name        = "just_watch"
  spec.version     = "0.1.0"
  spec.authors     = ["Fernando Seror"]
  spec.email       = "ferdynton@gmail.com"
  spec.summary     = "Connects with the unofficial API of JustWatch"
  spec.description = "Inspired by the Python version at https://github.com/dawoudt/JustWatchAPI"
  spec.homepage    = "https://gitlab.com/ferdynton/movie_mates"
  spec.license     = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.require_paths = ["lib"]

  spec.add_dependency "faraday", ">= 0.4", "< 2"
  spec.add_dependency "money", ">= 3.0.1", "< 7"

  spec.add_development_dependency "rspec", "~> 3"
  spec.add_development_dependency "vcr", ">= 2", "< 7"
  spec.add_development_dependency "webmock", ">= 2", "< 4"
end
